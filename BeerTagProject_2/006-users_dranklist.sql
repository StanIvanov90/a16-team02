create table users_dranklist
(
	id int auto_increment
		primary key,
	beer_id int not null,
	user_id int not null,
	constraint users_dranklist_beers_beer_id_fk
		foreign key (id) references beers (beer_id),
	constraint users_dranklist_users_user_id_fk
		foreign key (id) references users (user_id)
);

