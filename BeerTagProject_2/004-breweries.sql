create table breweries
(
	brewery_id int auto_increment
		primary key,
	brewery_name varchar(255) not null,
	country_id int not null,
	constraint breweries_countries_country_id_fk
		foreign key (country_id) references countries (country_id)
);

