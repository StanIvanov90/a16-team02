create table beer_tags
(
	beer_tag_id int auto_increment
		primary key,
	beer_id int null,
	tag_id int not null,
	constraint beer_tags_beers_id_fk
		foreign key (beer_id) references beers (beer_id),
	constraint beer_tags_tags_id_fk
		foreign key (tag_id) references tags (tag_id)
);

