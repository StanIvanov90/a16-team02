create table users
(
	user_id int auto_increment
		primary key,
	user_name varchar(255) not null,
	user_email varchar(255) not null,
	user_picture blob null
);

