INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (1, 'Bulgaria');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (2, 'Mexico');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (3, 'Australia');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (4, 'Austria');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (5, 'Belgium');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (6, 'Canada');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (7, 'Croatia');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (8, 'Czech Republic');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (9, 'Denmark');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (10, 'Greece');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (11, 'Germany');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (12, 'England');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (13, 'France');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (14, 'Ireland');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (15, 'USA');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (16, 'Macedonia');
INSERT INTO beerTag.countries ( country_id , country_name ) VALUES (17, 'Turkey');

INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Kamenitza', 1);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Zagorka', 1);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Corona', 2);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Carlton & United Breweries', 3);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Ottakringer', 4);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Brewery Fohrenburg', 4);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Bosteels Brewery', 5);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Astika Brewery', 1);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Carlsberg Bulgaria', 1);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Moosehead Breweries Limited', 6);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Zmajska Pivovara', 7);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Staropramen Brewery', 8);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Starobrno Brewery', 8);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Carlsberg Breweries', 9);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Santorini Brewing', 10);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Becks Brewery', 11);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Marston''s Beers and Breweries', 12);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Kronenbourg Brewery', 13);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Arthur Guinness Brewery', 14);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Anheuser-Busch', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Bass Brewers', 12);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Great Lakes Brewing', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Lagunitas Brewing Company', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('The Alchemist', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Tree House Brewing Company', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Funky Buddha Brewery', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Perennial Artisan Ales', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Stone Brewing', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Trillium Brewing Company', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Bottle Logic Brewing', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Hoof Hearted Brewing', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Alpine Beer Company', 15);
INSERT INTO beerTag.breweries ( brewery_name , country_id ) VALUES ('Temov Craft Brewery', 16);

INSERT INTO beerTag .styles  ( style_name ) VALUES ('Stout');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Pale Lager');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Lager');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('European Style Export');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Hefeweizen');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Belgian Three-grain Tripel');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Smoked German Lager');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Pilsner');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Alcohol-Free');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Lagered Ale');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('English Bitter');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('English Pale Ale');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('German Märzen');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('American Imperial Stout');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('New England IPA');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('American Imperial Porter');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('English Sweet');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Russian Imperial Stout');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('American IPA');
INSERT INTO beerTag .styles  ( style_name ) VALUES ('Porter');



INSERT INTO beerTag.tags (tag_name) VALUES ('Dark');
INSERT INTO beerTag.tags (tag_name) VALUES ('Light');
INSERT INTO beerTag.tags (tag_name) VALUES ('Tallie');
INSERT INTO beerTag.tags (tag_name) VALUES ('High-sugar');
INSERT INTO beerTag.tags (tag_name) VALUES ('Gold');
INSERT INTO beerTag.tags (tag_name) VALUES ('Fizzy Fruity Flavour');
INSERT INTO beerTag.tags (tag_name) VALUES ('Strong');
INSERT INTO beerTag.tags (tag_name) VALUES ('Fresh');
INSERT INTO beerTag.tags (tag_name) VALUES ('Smokiness');
INSERT INTO beerTag.tags (tag_name) VALUES ('Serpent');
INSERT INTO beerTag.tags (tag_name) VALUES ('Czech');
INSERT INTO beerTag.tags (tag_name) VALUES ('Alcohol-Free');
INSERT INTO beerTag.tags (tag_name) VALUES ('Unfiltered');
INSERT INTO beerTag.tags (tag_name) VALUES ('Bitter');
INSERT INTO beerTag.tags (tag_name) VALUES ('Refreshing');
INSERT INTO beerTag.tags (tag_name) VALUES ('Sweet');
INSERT INTO beerTag.tags (tag_name) VALUES ('Oktoberfest');
INSERT INTO beerTag.tags (tag_name) VALUES ('Coffee');
INSERT INTO beerTag.tags (tag_name) VALUES ('Citrus');
INSERT INTO beerTag.tags (tag_name) VALUES ('Tropic fruit');
INSERT INTO beerTag.tags (tag_name) VALUES ('Bacon');
INSERT INTO beerTag.tags (tag_name) VALUES ('Peach');
INSERT INTO beerTag.tags (tag_name) VALUES ('Chocolate');
INSERT INTO beerTag.tags (tag_name) VALUES ('Peanut butter');
INSERT INTO beerTag.tags (tag_name) VALUES ('Vanilla');
INSERT INTO beerTag.tags (tag_name) VALUES ('Dragon');
<<<<<<< HEAD
=======

>>>>>>> 47c7bd2dd1f8baff32b4b1dd6161541bdf25fd5f

INSERT INTO beerTag.beers (beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES ('Zagorka', 'TestDescr', 5, 1, 1, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
 VALUES (2, 'Pirinsko', 'PirinskoDesc', 4.4, 1, 1, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (3, 'Stolichno', 'StolichnoDesc', 4.4, 1, 1, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (4, 'Corona', ' PlaceHolder', 4.4, 2, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (5, 'Victoria Bitter', 'PlaceHolder', 4.9, 4, 3, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (6, 'Abbotsford Invalid Stout', ' PlaceHolder', 5.2, 4, 1, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (7, 'Ottakringer Gold Fassl', ' PlaceHolder', 5.6, 5, 4, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (8, 'Fohrenburger Weizen', ' PlaceHolder', 5.2, 6, 5, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (9, 'Tripel Karmeliet', ' PlaceHolder', 8.4, 7, 6, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (10, 'Burgasko', ' PlaceHolder', 4.4, 8, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (11, 'Kamenitza', ' PlaceHolder', 4.4, 8, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (12, 'Moosehead Lager', ' PlaceHolder', 5, 10, 3, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (13, 'Small Batch Rauchbier Smoked Lager', ' PlaceHolder', 5.5, 10, 7, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (14, 'Zmajsko Pale Ale', ' PlaceHolder', 5.3, 11, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (15, 'Starobrno', ' PlaceHolder', 5, 12, 3, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (16, 'Staropramen', ' PlaceHolder', 4.7, 13, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (17, 'Tuborg', ' PlaceHolder', 4.6, 14, 8, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (18, 'Carlsberg Norden Gylden Bryyg', ' PlaceHolder', 0.5, 14, 9, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (19, 'Red Dragon', ' PlaceHolder', 5.6, 15, 10, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (20, 'Becks', ' PlaceHolder', 5, 16, 3, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (21, 'Marstons Smooth', ' PlaceHolder', 3.6, 17, 11, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (22, 'Kronenbourg 1664', ' PlaceHolder', 5, 18, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (23, 'Guiness', ' PlaceHolder', 4.4, 19, 1, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (24, 'Bud Light', ' PlaceHolder', 4.2, 20, 2, null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (25, 'Bass Pale Ale', ' PlaceHolder', 5.1,21 ,12 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (26, 'Oktoberfest', ' PlaceHolder', 6.5,22 ,13 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (27, 'Willettized Coffee Stout', ' PlaceHolder', 12.9 ,23 ,14 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (28, 'Heady Topper', ' PlaceHolder', 8 ,24 ,15 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (29, 'King Julius', ' PlaceHolder', 8.3 ,25 ,15 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (30, 'Morning Wood', ' PlaceHolder', 12 ,26 ,16 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (31, 'Doppelganger', ' PlaceHolder', 8.2 ,25 ,15 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (32, 'Very Hazy', ' PlaceHolder', 8.6 ,25 ,15 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (33, 'Moment Of Clarity', ' PlaceHolder', 7.7 ,25 ,17 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (34, 'Hold On To Sunshine', ' PlaceHolder', 7.6 ,25 ,17 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (35, 'Abraxas', ' PlaceHolder', 10 ,27 ,14 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (36, 'Fyodor', ' PlaceHolder', 13.1 ,28 ,18 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (37, 'Vicinity', ' PlaceHolder', 8 ,29 ,15 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (38, 'Fundamental Forces', ' PlaceHolder', 16.2 ,30 ,14 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (39, 'Dragonsaddle', ' PlaceHolder', 11.5 ,31 ,15 , null);

INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture_id)
VALUES (40, 'Nelson', ' PlaceHolder', 7 ,32 ,19 , null);

# INSERT INTO beerTag.beers (beer_id, beer_name, beer_description, abv, brewery_id, style_id, beer_picture, beer_exists,created_by)
# VALUES (41, 'doko', ' PlaceHolder', 7 ,32 ,19 , null, 1,1);
-- -----------back to life
# update beers
# set created_by = 2
# where created_by=3;
#
# delete countries
# from countries
# where country_id between 20 and 24;
# update countries
# set country_exists = true
# where country_name = 'Turkey';

# select beer_name
# from beers
# join breweries b on beers.brewery_id = b.brewery_id
# join countries c on b.country_id = c.country_id
# where b.country_id = 15;

# select beers.beer_name
# from beers
# join beer_tags bt on beers.beer_id = bt.beer_id
# join tags t on bt.tag_id = t.tag_id
# where t.tag_id=15;


INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (1, 8);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (2, 2);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (3, 2);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (4, 8);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (4, 2);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (5, 3);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (6, 4);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (7, 5);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (8, 6);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (9, 7);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (10, 8);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (11, 2);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (12, 5);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (13, 9);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (14, 10);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (15, 11);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (16, 11);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (17, 8);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (18, 12);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (19, 13);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (20, 8);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (21, 14);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (22, 5);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (23, 1);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (24, 15);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (25, 16);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (26, 17);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (27, 18);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (28, 19);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (29, 20);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (30, 21);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (31, 20);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (32, 22);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (33, 23);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (34, 24);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (35, 15);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (36, 18);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (37, 20);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (38, 25);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (39, 26);
INSERT INTO beerTag.beer_tags (beer_id, tag_id) VALUES (40, 19);





INSERT INTO beerTag.roles (role_id, name) VALUES (1, 'User');
INSERT INTO beerTag.roles (role_id, name) VALUES (2, 'Admin');

INSERT INTO beerTag.users (user_id, user_name, user_email) VALUES (1, 'pesho','praykov@abv.bg');
INSERT INTO beerTag.users (user_id, user_name, user_email) VALUES (2, 'nadya','nadya@abv.bg');
INSERT INTO beerTag.users (user_id, user_name, user_email) VALUES (3, 'toshko','to6ko@abv.bg');

INSERT INTO beertag.usersroles(user_id, role_id) VALUES (1, 1);
INSERT INTO beertag.usersroles(user_id, role_id) VALUES (2, 1);
INSERT INTO beertag.usersroles(user_id, role_id) VALUES (3, 1);
INSERT INTO beertag.usersroles(user_id, role_id) VALUES (3, 2);

INSERT INTO beertag.users_dranklist(id, beer_id, user_id) VALUES (1, 1, 1);
INSERT INTO beertag.users_dranklist(id, beer_id, user_id) VALUES (2, 2, 1);
INSERT INTO beertag.users_dranklist(id, beer_id, user_id) VALUES (3, 3, 1);
INSERT INTO beertag.users_dranklist(id, beer_id, user_id) VALUES (4, 4, 2);
INSERT INTO beertag.users_dranklist(id, beer_id, user_id) VALUES (5, 5, 2);

INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (1, 1, 1);
INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (2, 2, 1);
INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (3, 3, 1);
INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (4, 4, 2);
INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (5, 5, 2);
INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (6, 7, 13);
INSERT INTO beertag.users_wishlist(id, beer_id, user_id) VALUES (7, 4, 13);


INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (1, 'http://tuidagroup.com/438-thickbox_default/ZAGORKA-RETRO-KASA-0-5L20.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (2, 'http://tuidagroup.com/2997-thickbox_default/PIRINSKO-KASA-0-5L20.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (3, 'http://tuidagroup.com/1404-thickbox_default/STOLICHNO-VAJS-TREJ-0-4L9.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (4, 'https://sc01.alicdn.com/kf/UTB8HeV_eYnJXKJkSahGq6xhzFXaR/Corona-Extra-Beer-For-Export-worldwide.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (5, 'https://cdn-5c6c9973f911ca1b2cef5d81.closte.com/wp-content/uploads/2015/09/victoria-bitter-beer-online-1368418576.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (6, 'https://abbeycellars.com.au/wp-content/uploads/2017/01/abbotsford_invalid_stout_stubbie.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (7, 'https://i.ibb.co/VMkPqrR/ottakringer-goldfassl-watermark-62e401ad2e.png');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (8, 'https://i.ibb.co/yBrzBNV/fohrenburger-weizen-watermark-f20f3d3f0d.png');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (9, 'https://cdn.shopify.com/s/files/1/0397/3509/products/tripel_karmeliet_1200x.png?v=1568119204');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (10, 'http://tuidagroup.com/887-thickbox_default/BURGASKO-SVETLO-0-5L20-KASA.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (11, 'http://tuidagroup.com/886-thickbox_default/KAMENICA-0-5L2050-KASA.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (12, 'http://ladysliquors.us/wp-content/uploads/2017/04/MOOSEHEAD_LAGER_NR_BEER_12_OZ.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (13, 'https://anbl-2.azureedge.net/img/product/34399-B.jpg?fv=FA082A404B36ACFD154E1885B02B1332-15326');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (14, 'https://dublindostava.com/wp-content/uploads/2017/03/Zmajsko-Pale-Ale-033l-600x600.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (15, 'https://secure.ce-tescoassets.com/assets/CZ/965/0000085933965/ShotType1_540x540.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (16, 'http://tuidagroup.com/2940-thickbox_default/STAROPRAMEN-0-5L20-KASA.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (17, 'http://tuidagroup.com/1428-thickbox_default/TUBORG-0-5L20-KASA.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (18, 'https://drikkevarer.nu/wp-content/uploads/sites/6/2019/01/64733-1.png');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (19, 'https://s3-eu-west-1.amazonaws.com/thefoodmarket1/spree/products/9188/e1000/BRCDRED330.jpg?1530065208');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (20, 'http://tuidagroup.com/1419-thickbox_default/BEKS-0-5L20-KASA.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (21, 'https://images-na.ssl-images-amazon.com/images/I/41il4iZB9cL.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (22, 'https://digitalcontent.api.tesco.com/v1/media/ghs/snapshotimagehandler_366702772/cf05f2ff82eafe9a98b850fe28908b7a1993a180330a38a51e48b70875cc10da/05035766380610/snapshotimagehandler_366702772.jpeg?h=540&w=540');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (23, 'https://cdn2.bigcommerce.com/server5500/tpbc2s65/products/256/images/283/guinessdraught11p2oz__98782.1351005565.1280.1280.jpg?c=2');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (24, 'https://centralsupercenter.com/wp-content/uploads/1970/01/bud-light-beer-12oz-600x600.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (25, 'https://specsonline.com/wp-content/uploads/2019/12/008382007131.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (26, 'https://cdn3-www.mandatory.com/assets/uploads/gallery/american-oktoberfest-beers/octoberfest.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (27, 'https://www.binnys.com/media/catalog/product/cache/eab16ae251e4410504af434c6d9419db/7/9/79563.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (28, 'https://cdn.shopify.com/s/files/1/0227/0581/products/The-Alchemist-Heady-Topper-16OZ-CAN.JPG?v=1531726530');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (29, 'https://www.thebeersbeer.com/wp-content/uploads/2019/06/beer-king-julius-style-ipa.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (30, 'http://merchantsfinewine.com/wp-content/uploads/2014/09/local-option-morning-wood-coffee-amber.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (31, 'https://menduina.eu/98-large_default/maria-solina-cerveza-smoked-brown-lager-menduina.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (32, 'https://www.mybeercollectibles.com/uploads/Screenshot_2019-09-11VeryHazy-TreeHouseBrewingCompanyPhotos-Untappd1.png');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (33, 'https://beerbliotek.com/wp-content/uploads/2017/03/Beerbliotek-A-Moment-of-Clarity-Can-Packshot-Shadow-Low-1024x1024.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (34, 'https://i.ibb.co/LCN3HtK/treehouse-brewing-company-hold-1-b8272391d24de63b3f42c4b3dc097739.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (35, 'https://www.galaxybeershop.com/gbs/wp-content/uploads/2019/05/Perennial-Abraxas-Imperial-Stout-750ML-BTL_59e233fc-0dc3-4446-902b-f98c7aa85cba.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (36, 'https://cdn.shopify.com/s/files/1/0227/0581/products/Stone-Mikhail-2017-500ML-BTL_0d766b90-9b33-4941-8351-54a04043ef96.png?v=1531728196');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (37, 'https://i.ibb.co/ByTgw6X/trillium-brewing-company-vicinity-double-ipa-1508194079-copy.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (38, 'https://untappd.akamaized.net/photos/2019_11_28/0ba1a8fd9c3bbe6efe5e33d9acd3a205_640x640.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (39, 'https://i.ibb.co/qkNm7dv/beer-249289.jpg');
INSERT INTO beerTag.pictures (picture_id, picture_url) VALUES (40, 'https://i.ibb.co/kgtSgxz/the-full-nelson-1050593-s120-p.jpg')




select beers.beer_name
from beers
join users_dranklist ud on beers.beer_id = ud.id
join users u on ud.id = u.user_id
where user_name like 'nadya';

select beers.beer_name
from beers
join beer_tags bt on beers.beer_id = bt.beer_id
join tags t on bt.tag_id = t.tag_id
where t.tag_id = 4;


select *
from beers
order by beer_name;

select *
from beers
order by abv;


select beers.beer_name
from beers
join beer_tags bt on beers.beer_id = bt.beer_id
join tags t on bt.tag_id = t.tag_id
where t.tag_name = 'Fresh';

select *
from beers
join users_dranklist ud on beers.beer_id = ud.beer_id
join users u on ud.user_id = u.user_id
where user_name like 'tanqboeva' and ud.rating is not null
order by ud.rating desc
limit 3;

select *
from beers
join breweries b on beers.brewery_id = b.brewery_id
where b.brewery_id =5 and  beers.beer_exists=1 and brewery_exists=1;


select*
from beers as b
join users_details ud on created_by = user_id
where user_name like :name and beer_exists =1;


select *
from beers
join breweries b on beers.brewery_id = b.brewery_id
join countries c on b.country_id = c.country_id
where country_name like 'bul' and country_exists=1 and beer_exists=1;
