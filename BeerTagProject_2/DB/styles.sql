create table styles
(
	style_id int auto_increment
		primary key,
	style_name varchar(255) not null,
	style_exist tinyint(1) default 1 not null
);

