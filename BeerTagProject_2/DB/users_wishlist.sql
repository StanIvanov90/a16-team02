create table users_wishlist
(
	id int auto_increment
		primary key,
	beer_id int null,
	user_id int null,
	constraint users_wishlist_beers_beer_id_fk
		foreign key (id) references beers (beer_id),
	constraint users_wishlist_users_id_fk
		foreign key (id) references users (user_id)
);

