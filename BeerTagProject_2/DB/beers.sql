create table beers
(
	beer_id int auto_increment
		primary key,
	beer_name varchar(255) not null,
	beer_description text null,
	abv double not null,
	brewery_id int not null comment 'Test',
	style_id int not null,
	beer_picture blob null,
	beer_exist tinyint(1) default 1 not null,
	created_by int null,
	constraint beers_breweries_id_fk
		foreign key (brewery_id) references breweries (brewery_id),
	constraint beers_styles_id_fk
		foreign key (style_id) references styles (style_id),
	constraint beers_users_user_id_fk
		foreign key (created_by) references users (user_id)
);

