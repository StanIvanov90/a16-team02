create table countries
(
	country_id int auto_increment
		primary key,
	country_name varchar(255) not null,
	country_exists tinyint(1) default 1 null
);

