create table usersroles
(
	user_id int null,
	role_id int null,
	constraint usersroles_roles_role_id_fk
		foreign key (role_id) references roles (role_id),
	constraint usersroles_users_user_id_fk
		foreign key (user_id) references users (user_id)
);

