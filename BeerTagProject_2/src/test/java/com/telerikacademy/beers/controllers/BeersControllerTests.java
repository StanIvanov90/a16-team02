//package com.telerikacademy.beers.controllers;
//
//import com.telerikacademy.beers.Factory;
//import com.telerikacademy.beers.exceptions.EntityNotFoundException;
//import com.telerikacademy.beers.models.Beer;
//import com.telerikacademy.beers.services.BeersService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.Mockito;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//
//import static com.telerikacademy.beers.Factory.createBeer;
//import static org.mockito.ArgumentMatchers.anyInt;
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//@RunWith(SpringRunner.class)
//@SpringBootTest
//@AutoConfigureMockMvc
//public class BeersControllerTests {
//
//    @MockBean
//    BeersService mockService;
//
//    @Autowired
//    MockMvc mockMvc;
//
//    @Test
//    public void getById_Should_Return_StatusOK_When_BeerExist() throws Exception {
//        Beer expected = Factory.createBeer();
//
//        Mockito.when(mockService.getById(anyInt()))
//                .thenReturn(expected);
//
//        mockMvc.perform(get("/api/beers/{id}", 1))
//                .andExpect(status().isOk())
//                .andExpect(jsonPath("$.name").value(expected.getName()))
//                .andDo(print());
//    }
//
//    @Test
//    public void getById_Should_Return_Status4xx_When_BeerDoesNotExist() throws Exception {
//
//        Mockito.when(mockService.getById(anyInt()))
//                .thenThrow(new EntityNotFoundException("Beer", anyInt()));
//
//        mockMvc.perform(get("/api/beers/{id}", 1))
//                .andExpect(status().is4xxClientError())
////                .andExpect(jsonPath("$.name").value(expected.getName()))
//                .andDo(print());
//    }
//
//}