package com.telerikacademy.beers;

import com.telerikacademy.beers.models.*;

public class Factory {
    public static Beer createBeer(){
        return new Beer("Zagorka",1.2,"description",createStyle(),getBrewery());
    }
    public static Country getCountry(){
        return new Country( "Bulgaria");
    }
    public static Brewery getBrewery(){
        return new Brewery( "Peshtera");
    }
    public static Style createStyle(){
        return new Style("Special Ale");
    }

    public static UserDetails createUser(){
        return new UserDetails("Ivan", "ivan@abv.bg");
    }
    public static Tag createTag(){
        return new Tag("Sweet");
    }

}
