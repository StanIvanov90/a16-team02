package com.telerikacademy.beers.services;

import com.telerikacademy.beers.Factory;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Brewery;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.BreweriesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
@RunWith(MockitoJUnitRunner.class)
public class BreweriesServiceImplTests {

    @Mock
    BreweriesRepository repository;
    BeerRepository beerRepository;

    @InjectMocks
    BreweriesServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnBrewery_WhenBreweryExists() {
        //Arrange
        Brewery expectedBrewery = Factory.getBrewery();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedBrewery);

        //Act
        Brewery returnedBrewery = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedBrewery, returnedBrewery);

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Brewery expectedBrewery = Factory.getBrewery();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedBrewery);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository,
                times(1)).getById(anyInt());

    }

    @Test
    public void createShould_Throw_WhenBreweryAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkBreweryExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.getBrewery()));
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Return_ListOfBreweries() {
        //Arrange
        Brewery expectedBrewery = Factory.getBrewery();
        List<Brewery> test = new ArrayList<>();
        test.add(expectedBrewery);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<Brewery> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

//    @Test
//    public void DeleteShould_CallRepository() {
//        //Arrange
//
//        List<Beer> beers = new ArrayList<>();
//        Beer expectedBeer = Factory.createBeer();
//        beers.add(expectedBeer);
//        Brewery expectedBrewery = Factory.getBrewery();
//        Mockito.when(beerRepository.checkIfBreweryHasBeers(expectedBrewery.getId()))
//                .thenReturn(beers);
//        //Act
//        mockService.delete(1);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).delete(anyInt());
//    }

    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        Brewery expectedBrewery = Factory.getBrewery();
        //Act
        mockService.update(1, expectedBrewery);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(1, expectedBrewery);
    }


    @Test
    public void filterByName_Should_CallRepo() {
        //Arrange

        //Act
        mockService.filteredByName(anyString());
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getByName(anyString());
    }
    @Test
    public void filterByCountry_Should_CallRepo() {
        //Arrange

        //Act
        mockService.filteredByCountry(anyString());
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getByCountry(anyString());
    }
    @Test
    public void filterByNameAndCountry_Should_CallRepo() {
        //Arrange

        //Act
        mockService.filteredByNameAndCountry(anyString(),"test");
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getByName(anyString());
    }


}
