package com.telerikacademy.beers.services;

import com.telerikacademy.beers.Factory;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Style;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.StylesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)
public class StylesServicesImplTests {

    @Mock
    StylesRepository repository;
    BeerRepository beerRepository;

    @InjectMocks
    StylesServiceImpl mockService;

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Style expectedStyle = Factory.createStyle();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedStyle);

        //Act

        mockService.getById(1);

        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(anyInt());

    }

    @Test
    public void getByIdShould_ReturnStyle_WhenStyleExists() {
        //Arrange
        Style expectedStyle = Factory.createStyle();
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedStyle);
        //Act
        Style returnedStyle = mockService.getById(1);
        //Assert
        Assert.assertSame(expectedStyle, returnedStyle);
    }

    @Test
    public void createShould_Throw_WhenStyleAlreadyExist() {
        //Arrange

        Mockito.when(repository.checkStyleExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createStyle()));
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Return_ListOfStyles() {
        //Arrange
        Style expectedStyle = Factory.createStyle();
        List<Style> test = new ArrayList<>();
        test.add(expectedStyle);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<Style> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

//    @Test
//    public void DeleteShould_CallRepository() {
//        //Arrange
//
//        //Act
//        mockService.delete(1);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).delete(anyInt());
//    }

    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        Style expectedStyle = Factory.createStyle();
        //Act
        mockService.update(1, expectedStyle);
        //Assert
        Mockito.verify(repository,Mockito.times(1)).update(1,expectedStyle);
    }

    @Test
    public void FilterByName_ShouldCallRepository () {
        //Arrange

        //Act

        mockService.filteredByName(anyString());
        //Assert
        Mockito.verify(repository,Mockito.times(1)).getByName(anyString());
    }


//    @Test
//    public void getStyleBeers_ShouldCallRepository() {
//        //Arrange
//
//        //Act
//        mockService.getStyleBeers(anyInt());
//
//        //Assert
//        Mockito.verify(beerRepository,Mockito.times(1)).getByStyle(1);
//    }





}
