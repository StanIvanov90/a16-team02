package com.telerikacademy.beers.services;

import com.telerikacademy.beers.Factory;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Beer;

import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.RatingRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;


import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class BeersServiceImplTests {

    @Mock
    BeerRepository repository;
    RatingRepository ratingRepository;

    @InjectMocks
    BeersServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnBeer_WhenBeerExists() {
        //Arrange
        Beer expectedBeer = Factory.createBeer();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedBeer);

        //Act
        Beer returnedBeer = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedBeer, returnedBeer);

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Beer expectedBeer = Factory.createBeer();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedBeer);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository,
                times(1)).getById(anyInt());

    }

    @Test
    public void createShould_Throw_WhenBeerAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkBeerExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createBeer()));
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Return_ListOfBeers() {
        //Arrange
        Beer expectedBeer = Factory.createBeer();
        List<Beer> test = new ArrayList<>();
        test.add(expectedBeer);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<Beer> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

//    @Test
//    public void DeleteShould_CallRepository() {
//        //Arrange
//        UserDetails user = Factory.createUser();
//        //Act
//        mockService.delete(1,user);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).delete(anyInt());
//    }

    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        Beer expectedBeer = Factory.createBeer();
        //Act
        mockService.update( expectedBeer);
        //Assert
        Mockito.verify(repository,Mockito.times(1)).update(expectedBeer);
    }

    @Test
    public void sortBy_ShouldCalRepository(){
        //Arrange

        //Act
        mockService.sortBy(repository.getAll(),"test");
        //Assert
        Mockito.verify(repository,Mockito.times(1)).sortBy(repository.getAll(),"tet1");
    }

    @Test
    public void filterByTag_ShouldCallRepository(){
        //Arrange

        //Act
        mockService.filterByTag(repository.getAll(),"test");
        //Assert
        Mockito.verify(repository,Mockito.times(1)).filterByTag(repository.getAll(),"test1");
    }
    @Test
    public void filterByStyle_ShouldCallRepository(){
        //Arrange

        //Act
        mockService.filterByStyle(repository.getAll(),"test");
        //Assert
        Mockito.verify(repository,Mockito.times(1)).filterByTag(repository.getAll(),"test1");
    }
    @Test
    public void filterByCountry_ShouldCallRepository(){
        //Arrange

        //Act
        mockService.filterByCountry(repository.getAll(),"test");
        //Assert
        Mockito.verify(repository,Mockito.times(1)).filterByTag(repository.getAll(),"test1");
    }

//    @Test
//    public void rateBeer_ShouldCallRepository() throws RatingOutOfRangeException, NoAccessToRateBeerException {
//        //Arrange
//        UserDetails user = Factory.createUser();
//        Beer testbeer = Factory.createBeer();
//        List<Beer> beers = new ArrayList<>();
//        beers.add(testbeer);
//        Mockito.when(repository.checkBeerExists(anyString())).thenReturn(true);
//
//
//        //Act
//        mockService.rateBeer(testbeer.getName(),1,2.5);
//        //Assert
//        Mockito.verify(ratingRepository,Mockito.times(1)).rate(user,testbeer,3.5);
//    }




}


