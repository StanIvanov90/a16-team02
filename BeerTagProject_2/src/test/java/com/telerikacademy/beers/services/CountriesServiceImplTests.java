package com.telerikacademy.beers.services;

import com.telerikacademy.beers.Factory;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Country;
import com.telerikacademy.beers.repositories.CountriesRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class CountriesServiceImplTests {
    @Mock
    CountriesRepository repository;

    @InjectMocks
    CountriesServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnCountry_WhenCountryExists() {
        //Arrange
        Country expectedCountry = Factory.getCountry();

        Mockito.when(repository.getById(1))
                .thenReturn(expectedCountry);

        //Act
        Country returnedCountry = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedCountry.getId(), returnedCountry.getId());
        // Assert.assertSame(expectedCountry.getName(), returnedCountry.getName());

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Country expectedBeer = Factory.getCountry();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedBeer);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository,
                times(1)).getById(anyInt());

    }
    @Test
    public void createShould_Throw_WhenCountryAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkCountryExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.getCountry()));
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Return_ListOfCountries() {
        //Arrange
        Country expectedCountry = Factory.getCountry();
        List<Country> test = new ArrayList<>();
        test.add(expectedCountry);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<Country> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

//    @Test
//    public void DeleteShould_CallRepository() {
//        //Arrange
//
//        //Act
//        mockService.delete(1);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).delete(anyInt());
//    }

    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        Country expectedCountry = Factory.getCountry();
        //Act
        mockService.update(1, expectedCountry);
        //Assert
        Mockito.verify(repository,Mockito.times(1)).update(1,expectedCountry);
    }
}
