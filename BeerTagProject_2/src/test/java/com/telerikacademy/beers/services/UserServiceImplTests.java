package com.telerikacademy.beers.services;


import com.telerikacademy.beers.Factory;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.User;
import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.repositories.UserRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)

public class UserServiceImplTests {

    @Mock
    UserRepository repository;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnUser_WhenUserExists() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedUser);

        //Act
        UserDetails returnedUser = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedUser);
        //Act
        mockService.getById(1);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(anyInt());
    }

    @Test
    public void createShould_Throw_WhenUserAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkUserExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createUser()));
    }


//    @Test
//    public void DeleteShould_DeleteUser_WhenData_Is_Valid() {
//        //Arrange
//        User expectedUser = Factory.createUser();
//
//        Mockito.when(repository.getById(anyInt()))
//                .thenReturn(expectedUser);
//
//
//        //Act
//        mockService.delete(1);
//        //Assert
//        Assert.assertEquals(0, repository.getAll().size());
//    }

    @Test
    public void DeleteShould_CallRepository() {
        //Arrange

        //Act
        mockService.delete(1);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).delete(anyInt());
    }


    @Test
    public void getAll_Should_Return_ListOfUsers() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        List<UserDetails> test = new ArrayList<>();
        test.add(expectedUser);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<UserDetails> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        //Act
        mockService.update(expectedUser,1);
        //Assert
        Mockito.verify(repository,Mockito.times(1)).update(expectedUser,1);
    }


}

