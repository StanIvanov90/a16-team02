package com.telerikacademy.beers.services;

import com.telerikacademy.beers.Factory;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Tag;
import com.telerikacademy.beers.repositories.TagsRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class TagsServiceTests {

    @Mock
    TagsRepository repository;

    @InjectMocks
    TagsServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnTag_WhenTagExists() {
        //Arrange
        Tag expectedTag = Factory.createTag();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedTag);

        //Act
        Tag returnedTag = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedTag, returnedTag);

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        Tag expectedTag = Factory.createTag();

        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedTag);

        //Act
        mockService.getById(1);

        //Assert
        Mockito.verify(repository,
                times(1)).getById(anyInt());

    }

    @Test
    public void createShould_Throw_WhenTagAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkTagExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createTag()));
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void getAll_Should_Return_ListOfTag() {
        //Arrange
        Tag expectedTag = Factory.createTag();
        List<Tag> test = new ArrayList<>();
        test.add(expectedTag);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<Tag> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

    //    @Test
//    public void DeleteShould_CallRepository() {
//        //Arrange
//
//        //Act
//        mockService.delete(1);
//        //Assert
//        Mockito.verify(repository, Mockito.times(1)).delete(anyInt());
//    }
    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        Tag expectedTag = Factory.createTag();
        //Act
        mockService.update(1, expectedTag);
        //Assert
        Mockito.verify(repository,Mockito.times(1)).update(1,expectedTag);
    }

}
