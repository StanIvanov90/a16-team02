package com.telerikacademy.beers.utils;

import com.telerikacademy.beers.models.Beer;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BeersCollectionHelper {
    public static List<Beer> filterByName(List<Beer> beers, String name) {
        if (!name.isEmpty()) {
            beers = beers.stream()
                    .filter(b -> b.getName().toLowerCase().contains(name.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return beers;
    }
//    public static List<Beer> filterByCountry(List<Beer> beers, String country) {
//        if (!country.isEmpty()) {
//            beers = beers.stream()
//                    .filter(b -> b.getCountry().getName().toLowerCase().contains(country.toLowerCase()))
//                    .collect(Collectors.toList());
//        }
//        return beers;
//    }
//
//    public static List<Beer> filterByTags(List<Beer> beers, String tag) {
//        if (!tag.isEmpty()) {
//            beers = beers.stream()
//                    .filter(b -> b.getTag().getName().toLowerCase().contains(tag.toLowerCase()))
//                    .collect(Collectors.toList());
//        }
//        return beers;
//    }


    public static List<Beer> filterByStyle(List<Beer> beers, String style) {
        if (!style.isEmpty()) {
            beers = beers.stream()
                    .filter(b -> b.getStyle().getName().toLowerCase().contains(style.toLowerCase()))
                    .collect(Collectors.toList());
        }
        return beers;
    }

    public static List<Beer> sortBy(List<Beer> beers, String sortBy) {
        if (!sortBy.isEmpty()) {
            switch (sortBy.toLowerCase()) {
                case "name":
                    beers.sort(Comparator.comparing(Beer::getName));
                    break;
                case "abv":
                    beers.sort(Comparator.comparingDouble(Beer::getAbv));
                    break;
                case "rating":
                    beers.sort(Comparator.comparingDouble(Beer::getRating));
                    break;
            }
        }
        return beers;
    }
}
