package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.UserDetails;

public interface RatingRepository {

    void rate(UserDetails user, Beer beer, double rating);

    double getRating(Beer beer);
}
