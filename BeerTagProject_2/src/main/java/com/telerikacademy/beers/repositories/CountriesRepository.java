package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Country;

import java.util.List;

public interface CountriesRepository {
    void create(Country country);

    List<Country> getAll();

    Country getById(int id);

    boolean checkCountryExists(String name);

    boolean isDeleted(String name);

    Country deletedCountry(String country);

    void update(int id,  Country country);

    void delete(int id);

}
