package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.UserDetails;

import java.util.List;
import java.util.Set;

public interface UserRepository {
    void create(UserDetails user);

    List<UserDetails> getAll();

    UserDetails getById(int id);

    UserDetails getByUsername(String username);

    boolean checkUserExists(String name);

    boolean checkEmailExists(String email);

    boolean checkIfExists(int id);

    void update(UserDetails user, int id);

    void delete(int id);

    boolean isDeleted(String name);

    int getUserID(String name);

    void addBeerToDrankList(UserDetails user, Beer beer);

    List<Beer> getUserDrankList(String name);

    void addBeerToWishList(UserDetails user, Beer beer);

    Set<Beer> getUserWishList(String name);

    List<Beer> getTopThreeBeers(String name);

    List<Beer> createdBeers(String name);


}
