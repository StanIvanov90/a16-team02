package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.User;
import com.telerikacademy.beers.models.UserDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public class UserRepositoryImpl implements UserRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setExists(true);
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<UserDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from UserDetails where user_exists is not 0", UserDetails.class)
                    .list();
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails user = session.get(UserDetails.class, id);
            if (user == null) {
                throw new EntityNotFoundException("User", id);
            }
            return user;
        }
    }

    @Override
    public UserDetails getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where user_name = :username");
            query.setParameter("username", username);
            List<UserDetails> users = query.list();
            if (users.size() != 1) {
                throw new EntityNotFoundException(String.format("User with username %s does not exist", username));
            }
            return users.get(0);
        }

    }


    @Override
    public boolean checkUserExists(String name) {
//        if (isDeleted(name)) {
//            return true;
//        }
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where user_name like :name and user_exists is not 0", UserDetails.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public boolean checkEmailExists(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where user_email like :email and user_exists is not 0", UserDetails.class);
            query.setParameter("email", email);
            return !query.list().isEmpty();
        }
    }

    @Override
    public boolean checkIfExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(UserDetails.class, id) == null;
        }
    }

    @Override
    public void update(UserDetails user, int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setExists(true);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails user = session.get(UserDetails.class, id);
            if (checkIfExists(id)) {
                throw new EntityNotFoundException("User", id);
            } else {
                session.beginTransaction();
                user.setExists(false);
                session.update(user);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public boolean isDeleted(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where name like :name and user_exists is 0", UserDetails.class);
            query.setParameter("name", name);
            session.clear();
            return !query.list().isEmpty();
        }

    }

    @Override
    public int getUserID(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where name like :name");
            query.setParameter("name", name);
            return query.list().get(0).getId();
        }
    }

    @Override
    public void addBeerToDrankList(UserDetails user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.getDranklist().add(beer);
            session.update(user);
            session.getTransaction().commit();

        }
    }

    @Override
    public List<Beer> getUserDrankList(String name) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join users_dranklist ud on beers.beer_id = ud.beer_id\n" +
                    "join users_details u on ud.user_id = u.user_id\n" +
                    "where user_name like :name and user_exists = 1 and beer_exists = 1\n" +
                    "order by ud.rating desc\n", Beer.class);
            query.setParameter("name", "%" + name + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Dranklist %s not found", name));
            }
            return query.list();
        }
    }

    @Override
    public void addBeerToWishList(UserDetails user, Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.getWishlist().add(beer);
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public Set<Beer> getUserWishList(String name) {
        UserDetails user = getByUsername(name);
        if (user.getWishlist().isEmpty()) {
            throw new EntityNotFoundException(String.format("Wishlist by user %s not found", name));
        }
        return user.getWishlist();
    }

    @Override
    public List<Beer> getTopThreeBeers(String name) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join users_dranklist ud on beers.beer_id = ud.beer_id\n" +
                    "join users_details u on ud.user_id = u.user_id\n" +
                    "where user_name like :name and ud.rating is not null and user_exists = 1 and beer_exists = 1\n" +
                    "order by ud.rating desc\n" +
                    "limit 3;", Beer.class);
            query.setParameter("name", "%" + name + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Top 3 rated beers by user.html %s not found", name));
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> createdBeers(String name) {
        try(Session session = sessionFactory.openSession()){
            NativeQuery<Beer> query = session.createNativeQuery("select*\n" +
                    "from beers as b\n" +
                    "join users_details ud on created_by = user_id\n" +
                    "where user_name like :name and beer_exists =1;",Beer.class);
            query.setParameter("name","%"+name+"%");
            return query.list();
        }
    }
}
