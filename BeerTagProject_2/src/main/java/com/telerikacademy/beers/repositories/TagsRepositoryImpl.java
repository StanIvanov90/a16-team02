package com.telerikacademy.beers.repositories;


import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Country;
import com.telerikacademy.beers.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Repository
public class TagsRepositoryImpl implements TagsRepository {

    SessionFactory sessionFactory;

    @Autowired
    public TagsRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            tag.setExists(true);
            session.save(tag);
        }
    }

    @Override
    public List<Tag> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Tag where tag_exists is not 0", Tag.class)
                    .list();
        }
    }

    @Override
    public Tag getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            session.clear();
            if (tag == null) {
                throw new EntityNotFoundException("Tag", id);
            }
            return tag;
        }
    }

    @Override
    public boolean checkTagExists(String name) {
//        if (isDeleted(name)) {
//            return true;
//        }
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name and tag_exists is not 0", Tag.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }
    @Override
    public boolean checkIfExists(int id){
        try(Session session = sessionFactory.openSession()){
            return session.get(Tag.class,id) == null;
        }
    }

    @Override
    public void update(int id, Tag tag) {
        try (Session session = sessionFactory.openSession()) {
            if (checkIfExists(tag.getId())) {
                throw new EntityNotFoundException("Tag", id);
            } else {
                session.beginTransaction();
                tag.setExists(true);
                session.update(tag);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Tag tag = session.get(Tag.class, id);
            if (checkIfExists(id)) {
                throw new EntityNotFoundException("Tag", id);
            } else {
                session.beginTransaction();
                tag.setExists(false);
                session.update(tag);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public boolean isDeleted(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Tag> query = session.createQuery("from Tag where name like :name and tag_exists is  0", Tag.class);
            query.setParameter("name", name);
            session.clear();
            return !query.list().isEmpty();
        }

    }

    public int getTagID(String name){
        try(Session session = sessionFactory.openSession()){
            Query<Tag> query = session.createQuery("from Tag where name like :name");
            query.setParameter("name",name);
            return query.list().get(0).getId();
        }
    }
}
