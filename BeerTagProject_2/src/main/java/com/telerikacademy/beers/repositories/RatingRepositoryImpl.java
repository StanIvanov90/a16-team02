package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Rating;
import com.telerikacademy.beers.models.RatingID;
import com.telerikacademy.beers.models.UserDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class RatingRepositoryImpl implements RatingRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public RatingRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void rate(UserDetails user, Beer beer, double rating) {
        try(Session session = sessionFactory.getCurrentSession()){
            RatingID id = new RatingID();
            id.setUserId(user.getId());
            id.setBeerId(beer.getId());
            Rating newRating = new Rating();
            newRating.setId(id);
            newRating.setRating(rating);
            session.beginTransaction();
            session.update(newRating);
            session.getTransaction().commit();

        }

    }

    @Override
    public double getRating(Beer beer) {
        try(Session session = sessionFactory.openSession()){
            Query<Rating> query = session.createQuery("from Rating where beer_id = :id and rating is not null",Rating.class);
            query.setParameter("id",beer.getId());
            double rating = (query.list().stream().mapToDouble(Rating::getRating).sum()) / query.list().size();
            session.beginTransaction();
            beer.setRating(rating);
            session.update(beer);
            session.getTransaction().commit();
            return rating;
        }
    }
}
