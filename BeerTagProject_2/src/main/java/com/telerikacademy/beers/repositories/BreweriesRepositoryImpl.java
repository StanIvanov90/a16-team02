package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Brewery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Repository;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@Repository
public class BreweriesRepositoryImpl implements BreweriesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public BreweriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Brewery> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Brewery where brewery_exists not like 0", Brewery.class)
                    .list();
        }
    }

    @Override
    public Brewery getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (brewery == null) {
                throw new EntityNotFoundException("Brewery", id);
            }
            return brewery;
        }
    }

    @Override
    public void create(Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            brewery.setExists(true);
            session.save(brewery);
        }
    }

    @Override
    public void update(int id, Brewery brewery) {
        try (Session session = sessionFactory.openSession()) {
            if (checkIfExists(brewery.getId())) {
                throw new EntityNotFoundException("Brewery", id);
            } else {
                session.beginTransaction();
                brewery.setExists(true);
                session.update(brewery);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            Brewery brewery = session.get(Brewery.class, id);
            if (checkIfExists(id)) {
                throw new EntityNotFoundException("Tag", id);
            } else {
                session.beginTransaction();
                brewery.setExists(false);
                session.update(brewery);
                session.getTransaction().commit();
            }
        }
    }

    @Override
    public boolean checkBreweryExists(String name) {
        if (isDeleted(name)) {
            return true;
        }
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name like :name and brewery_exists is not 0", Brewery.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public boolean checkIfExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            return session.get(Brewery.class, id) == null;
        }
    }

    @Override
    public boolean isDeleted(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name like :name and brewery_exists is  0", Brewery.class);
            query.setParameter("name", name);
            session.clear();
            return !query.list().isEmpty();
        }

    }

    public int getBreweryID(String name){
        try(Session session = sessionFactory.openSession()){
            Query<Brewery> query = session.createQuery("from Brewery where name like :name");
            query.setParameter("name",name);
            return query.list().get(0).getId();
        }
    }

    @Override
    public List<Brewery> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where name like :name", Brewery.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public List<Brewery> getByCountry(String country) {
        try(Session session = sessionFactory.openSession()) {
            Query<Brewery> query = session.createQuery("from Brewery where country.name like :country", Brewery.class);
            query.setParameter("country", "%"+ country + "%");
            return query.list();
        }
    }
}
