package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Style;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StylesRepositoryImpl implements StylesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public StylesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Style style) {
        try (Session session = sessionFactory.openSession()) {
            style.setExists(true);
            session.save(style);
        }
    }

    @Override
    public List<Style> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Style where style_exists is not 0", Style.class)
                    .list();
        }
    }

    @Override
    public List<Style> getByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name like :name", Style.class);
            query.setParameter("name", "%" + name + "%");
            return query.list();
        }
    }

    @Override
    public Style getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (style == null) {
                throw new EntityNotFoundException("Style", id);
            }
            if (!style.getExists()) {
                throw new EntityNotFoundException("Style", id);
            }
            return style;
        }
    }

    @Override
    public boolean checkStyleExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name like :name and style_exists is not 0", Style.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void update(int id, Style style) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            style.setId(id);
            style.setExists(true);
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        getById(id);
        try (Session session = sessionFactory.openSession()) {
            Style style = session.get(Style.class, id);
            if (!style.getExists()) {
                throw new EntityNotFoundException("Style", id);
            }
            style.setExists(false);
            session.beginTransaction();
            session.update(style);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean isDeleted(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name like :name and style_exists is 0", Style.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public Style deletedStyle(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Style> query = session.createQuery("from Style where name like :name and style_exists is 0", Style.class);
            query.setParameter("name", name);
            return query.list().get(0);
        }
    }
}
