package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Style;

import java.util.List;

public interface StylesRepository {
    void create(Style style);

    List<Style> getAll();

    List<Style> getByName(String name);

    Style getById(int id);

    boolean checkStyleExists(String name);

    void update(int id, Style style);

    void delete(int id);

    boolean isDeleted(String name);

    Style deletedStyle(String style);

}
