package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Beer;

import java.util.List;

public interface BeerRepository {
    void create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    boolean checkBeerExists(String name);

    void update(Beer beer);


    void delete(int id);

    boolean isDeleted(String name);

    Beer deletedBeer(String beer);

    List<Beer> getByStyle(int styleId);

    List<Beer> getByCountry(int countryId);

    List<Beer> getByTag(int tagId);

    List<Beer> checkIfBreweryHasBeers(int breweryId);

    List<Beer> checkIfStyleHasBeer(int styleId);

    List<Beer> checkIfCountryHasBeers(int countryId);

    List<Beer> checkIfTagHasBeers(int tagId);

    List<Beer> getWishlist(String username);

    List<Beer> getDranklist(String username);

    List<Beer> sortBy(List<Beer> result, String sortBy);

    List<Beer> filterByTag(List<Beer> result, String tag);

    List<Beer> filterByStyle(List<Beer> result, String style);

    List<Beer> filterByCountry(List<Beer> result, String country);

    List<Beer> getByBeerName(String name);




}
