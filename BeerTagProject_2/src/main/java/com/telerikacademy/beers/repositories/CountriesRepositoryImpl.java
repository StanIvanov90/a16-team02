package com.telerikacademy.beers.repositories;


import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.Country;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CountriesRepositoryImpl implements CountriesRepository {

    private SessionFactory sessionFactory;

    @Autowired
    public CountriesRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void create(Country country) {
        try (Session session = sessionFactory.openSession()) {
            country.setExists(true);
            session.save(country);
        }
    }

    @Override
    public List<Country> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Country where country_exists is not 0", Country.class)
                    .list();
        }
    }

    @Override
    public Country getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);

            if (country == null) {
                throw new EntityNotFoundException("Country", id);
            }
            if (!country.getExists()) {
                throw new EntityNotFoundException("Country", id);
            }
            return country;
        }
    }

    @Override
    public boolean checkCountryExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name like :name and country_exists is not 0", Country.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void update(int id, Country country) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            country.setId(id);
            country.setExists(true);
            session.update(country);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        getById(id);
        try (Session session = sessionFactory.openSession()) {
            Country country = session.get(Country.class, id);
            if (!country.getExists()) {
                throw new EntityNotFoundException("Country", id);
            }
            country.setExists(false);
            session.beginTransaction();
            session.update(country);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean isDeleted(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name like :name and country_exists is  0", Country.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public Country deletedCountry(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Country> query = session.createQuery("from Country where name like :name and country_exists is  0", Country.class);
            query.setParameter("name", name);
            return query.list().get(0);
        }
    }
}