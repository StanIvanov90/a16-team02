package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Tag;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {

    private SessionFactory sessionFactory;
    private TagsRepository tagsRepository;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory, TagsRepository tagsRepository) {

        this.sessionFactory = sessionFactory;
        this.tagsRepository = tagsRepository;
    }

    @Override
    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            beer.setExists(true);
            session.save(beer);
            session.getTransaction().commit();
        }

    }

    @Override
    public List<Beer> getAll() {
        try (Session session = sessionFactory.openSession()) {
            return session
                    .createQuery("from Beer where beer_exists is not 0", Beer.class)
                    .list();
        }
    }

    @Override
    public Beer getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (beer == null) {
                throw new EntityNotFoundException("Beer", id);
            }
            if (!beer.getExists()) {
                throw new EntityNotFoundException("Beer", id);
            }
            return beer;
        }
    }

    @Override
    public boolean checkBeerExists(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name like :name and beer_exists is not 0", Beer.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public void update(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            beer.setExists(true);
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        getById(id);
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);
            if (!beer.getExists()) {
                throw new EntityNotFoundException("Beer", id);
            }
            beer.setExists(false);
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean isDeleted(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name like :name and beer_exists is 0", Beer.class);
            query.setParameter("name", name);
            return !query.list().isEmpty();
        }
    }

    @Override
    public Beer deletedBeer(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("from Beer where name like :name and beer_exists is 0", Beer.class);
            query.setParameter("name", name);
            return query.list().get(0);
        }
    }

    @Override
    public List<Beer> getByStyle(int styleId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *" +
                    " from beers join styles s on beers.style_id = s.style_id " +
                    "where beers.style_id = :styleId and beer_exists=1 and style_exists=1", Beer.class);
            query.setParameter("styleId", styleId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Beers with styleId %d not found", styleId));
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> getByCountry(int countryId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select * " +
                    "from beers join breweries b on beers.brewery_id = b.brewery_id " +
                    "join countries c on b.country_id = c.country_id " +
                    "where c.country_id = :countryId and beer_exists=1 and country_exists=1", Beer.class);
            query.setParameter("countryId", countryId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Beers with countryId %d not found", countryId));
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> getByTag(int tagId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join beer_tags bt on beers.beer_id = bt.beer_id\n" +
                    "join tags t on bt.tag_id = t.tag_id\n" +
                    "where t.tag_id = :tagId and beer_exists=1 and tag_exists=1", Beer.class);
            query.setParameter("tagId", tagId);
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Beers with tagId %d not found", tagId));
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> checkIfBreweryHasBeers(int breweryId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join breweries b on beers.brewery_id = b.brewery_id\n" +
                    "where b.brewery_id =:breweryId and beers.beer_exists=1 and brewery_exists=1;", Beer.class);
            query.setParameter("breweryId", breweryId);
            return query.list();
        }
    }

    @Override
    public List<Beer> checkIfStyleHasBeer(int styleId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *" +
                    " from beers join styles s on beers.style_id = s.style_id " +
                    "where beers.style_id = :styleId and beer_exists=1 and style_exists=1", Beer.class);
            query.setParameter("styleId", styleId);
            return query.list();
        }
    }

    @Override
    public List<Beer> checkIfCountryHasBeers(int countryId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select * " +
                    "from beers join breweries b on beers.brewery_id = b.brewery_id " +
                    "join countries c on b.country_id = c.country_id " +
                    "where c.country_id = :countryId and beer_exists=1 and country_exists=1", Beer.class);
            query.setParameter("countryId", countryId);
            return query.list();
        }
    }

    @Override
    public List<Beer> checkIfTagHasBeers(int tagId) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join beer_tags bt on beers.beer_id = bt.beer_id\n" +
                    "join tags t on bt.tag_id = t.tag_id\n" +
                    "where t.tag_id = :tagId and beer_exists=1 and tag_exists=1", Beer.class);
            query.setParameter("tagId", tagId);
            return query.list();
        }
    }


    @Override
    public List<Beer> getWishlist(String username) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join users_wishlist ud on beers.beer_id = ud.id\n" +
                    "join users u on ud.id = u.user_id\n" +
                    "where user_name like :username and beer_exists=1", Beer.class);
            query.setParameter("username", "%" + username + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Empty wishlist");
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> getDranklist(String username) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join users_dranklist ud on beers.beer_id = ud.id\n" +
                    "join users u on ud.id = u.user_id\n" +
                    "where user_name like :username and beer_exists=1", Beer.class);
            query.setParameter("username", "%" + username + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException("Empty dranklist");
            }
            return query.list();
        }
    }

    @Override
    public List<Beer> sortBy(List<Beer> result, String sortBy) {
        try (Session session = sessionFactory.openSession()) {
            switch (sortBy.toLowerCase()) {
                case "name":
                    NativeQuery<Beer> nameQuery = session.
                            createNativeQuery("select * from beers where beer_exists=1 order by beer_name", Beer.class);
                    if (nameQuery.list().isEmpty()) {
                        throw new EntityNotFoundException("Beers not found");
                    }
                    return nameQuery.list();
                case "abv":
                    NativeQuery<Beer> abvQuery = session.
                            createNativeQuery("select * from beers where beer_exists=1 order by abv", Beer.class);
                    if (abvQuery.list().isEmpty()) {
                        throw new EntityNotFoundException("Beers not found");
                    }
                    return abvQuery.list();
                case "rating":
                    NativeQuery<Beer> ratingQuery = session.
                            createNativeQuery("select * from beers where beer_exists=1 order by rating desc", Beer.class);
                    if (ratingQuery.list().isEmpty()) {
                        throw new EntityNotFoundException("Beers not found");
                    }
                    return ratingQuery.list();
            }
            return result;
        }
    }

    @Override
    public List<Beer> filterByTag(List<Beer> result, String tag) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join beer_tags bt on beers.beer_id = bt.beer_id\n" +
                    "join tags t on bt.tag_id = t.tag_id\n" +
                    "where t.tag_name like :tag and tag_exists=1  and beer_exists=1", Beer.class);
            query.setParameter("tag", "%" + tag + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Beers with tag %s not found", tag));
            }
            return query.list();
        }
    }


    @Override
    public List<Beer> filterByStyle(List<Beer> result, String style) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join styles s on beers.style_id = s.style_id " +
                    "where style_name like :style and style_exists=1 and beer_exists=1", Beer.class);
            query.setParameter("style", "%" + style + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Beers with style %s not found", style));
            }
            return query.list();

        }
    }

    @Override
    public List<Beer> filterByCountry(List<Beer> result, String country) {
        try (Session session = sessionFactory.openSession()) {
            NativeQuery<Beer> query = session.createNativeQuery("select *\n" +
                    "from beers\n" +
                    "join breweries b on beers.brewery_id = b.brewery_id " +
                    "join countries c on b.country_id = c.country_id " +
                    "where country_name like :country and country_exists=1 and beer_exists=1", Beer.class);
            query.setParameter("country", "%" + country + "%");
            if (query.list().isEmpty()) {
                throw new EntityNotFoundException(String.format("Beers with country %s not found", country));
            }
            return query.list();
        }
    }


    @Override
    public List<Beer> getByBeerName(String name) {
        Session session = sessionFactory.getCurrentSession();
        Query<Beer> query = session.createQuery("from Beer where name = :name and beer_exists is not 0", Beer.class);
        query.setParameter("name", name);
        return query.list();
    }
}
