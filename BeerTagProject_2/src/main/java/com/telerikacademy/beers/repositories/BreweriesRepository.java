package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Brewery;

import java.util.List;

public interface BreweriesRepository {
    List<Brewery> getAll();

    Brewery getById(int id);

    void create(Brewery brewery);

    void update(int id, Brewery brewery);

    void delete(int id);

    boolean checkBreweryExists(String name);

    boolean checkIfExists(int id);

    boolean isDeleted(String name);

    int getBreweryID(String name);

    List<Brewery> getByName(String name);

    List<Brewery> getByCountry(String country);

}
