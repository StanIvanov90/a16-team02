package com.telerikacademy.beers.repositories;

import com.telerikacademy.beers.models.Tag;

import java.util.List;

public interface TagsRepository {
    void create(Tag tag);

    List<Tag> getAll();

    Tag getById(int id);

    boolean checkTagExists(String name);

    boolean checkIfExists(int id);

    boolean isDeleted(String name);

    void update(int id, Tag tag);

    void delete(int id);

    int getTagID(String name);
}
