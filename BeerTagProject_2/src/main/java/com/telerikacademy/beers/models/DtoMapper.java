package com.telerikacademy.beers.models;

import com.telerikacademy.beers.repositories.*;
import com.telerikacademy.beers.services.BreweriesService;
import com.telerikacademy.beers.services.CountriesService;
import com.telerikacademy.beers.services.StylesService;
import com.telerikacademy.beers.services.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DtoMapper {
    private StylesService stylesService;
    private CountriesService countriesService;
    private BreweriesService breweriesService;
    private TagsService tagsService;
    private CountriesRepository countriesRepository;
    private BreweriesRepository breweriesRepository;
    private StylesRepository stylesRepository;
    private TagsRepository tagsRepository;
    private BeerRepository beerRepository;

    @Autowired
    public DtoMapper(StylesService stylesService,
                     CountriesService countriesService,
                     BreweriesService breweriesService,
                     TagsService tagsService,
                     CountriesRepository countriesRepository,
                     BreweriesRepository breweriesRepository,
                     StylesRepository stylesRepository,
                     TagsRepository tagsRepository,
                     BeerRepository beerRepository) {
        this.stylesService = stylesService;
        this.countriesService = countriesService;
        this.breweriesService = breweriesService;
        this.tagsService = tagsService;
        this.countriesRepository = countriesRepository;
        this.breweriesRepository = breweriesRepository;
        this.stylesRepository = stylesRepository;
        this.tagsRepository = tagsRepository;
        this.beerRepository=beerRepository;
    }

    public Beer fromDto(BeerDto beerDto) {
        Beer beer = new Beer(beerDto.getName(), beerDto.getAbv(), beerDto.getDescription(),
                stylesRepository.getById(beerDto.getStyleId()),
                breweriesRepository.getById(beerDto.getBreweryId())

        );
        Picture picture = new Picture();
        picture.setUrl(beerDto.getPicture());
        beer.setPicture(picture);
        Set<Tag> tags = new HashSet<>();
        if (!(beerDto.getTagId() == null)) {
            for (String i : beerDto.getTagId()) {
                if(Integer.parseInt(i)==0){
                    continue;
                }
                tags.add(tagsRepository.getById(Integer.parseInt(i)));
            }
            beer.setTags(tags);
        }
        return beer;
    }

    public Brewery fromDto(BreweryDto breweryDto) {
        Brewery newBrewery = new Brewery(breweryDto.getName());
        newBrewery.setCountry(countriesRepository.getById(breweryDto.getCountryId()));
        return newBrewery;
    }

    public Brewery updateBreweryFromDto(Brewery brewery, BreweryDto breweryDto) {
        brewery.setName(breweryDto.getName());
        brewery.setCountry(countriesRepository.getById(breweryDto.getCountryId()));
        return brewery;
    }

    public UserDetails userFromDto(UserDto userDto) {
        UserDetails user = new UserDetails(userDto.getName(), userDto.getEmail()
        );
        user.setFirstName(userDto.getFirstName());
        user.setLastname(userDto.getLastName());
        Picture picture = new Picture();
        picture.setUrl(userDto.getPicture());
        user.setPicture(picture);
        return user;
    }

    public Beer updateBeerFromDto(Beer beer, BeerDto beerDto) {
        beer.setName(beerDto.getName());
        beer.setDescription(beerDto.getDescription());
        beer.setAbv(beerDto.getAbv());
        beer.setBrewery(breweriesService.getById(beerDto.getBreweryId()));
        beer.setStyle(stylesService.getById(beerDto.getStyleId()));
        Picture picture = new Picture();
        picture.setUrl(beerDto.getPicture());
        beer.setPicture(picture);
        return beer;
    }

    public UserDetails updateUserFromDto(UserDetails userDetails,UserDto userDto){
        userDetails.setFirstName(userDto.getFirstName());
        userDetails.setLastname(userDto.getLastName());
        userDetails.setEmail(userDto.getEmail());
        Picture picture = new Picture();
        picture.setUrl(userDto.getPicture());
        userDetails.setPicture(picture);
        return userDetails;
    }
}
