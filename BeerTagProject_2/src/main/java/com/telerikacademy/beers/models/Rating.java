package com.telerikacademy.beers.models;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "users_dranklist")
public class Rating {

    @EmbeddedId
    private RatingID id;

    @Column(name = "rating")
    private double rating = 0;

    public Rating() {
    }

    public RatingID getId() {
        return id;
    }

    public void setId(RatingID id) {
        this.id = id;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
