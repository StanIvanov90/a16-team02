package com.telerikacademy.beers.models;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "beers")
public class Beer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "beer_id")
    private int id;

    @Column(name = "beer_name")
    private String name;

    @Column(name = "abv")
    private double abv;

    @Column(name = "beer_description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "brewery_id")
    private Brewery brewery;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "beer_tags",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id")
    )
    private Set<Tag> tags;

    @JsonIgnore
    @Column(name = "beer_exists")
    private Boolean exists;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "created_by")
    private UserDetails createdBy;


    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_dranklist",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<Beer> dranklist;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "users_wishlist",
            joinColumns = @JoinColumn(name = "beer_id"),
            inverseJoinColumns = @JoinColumn(name = "user_id")
    )
    private Set<Beer> wishlist;

    @Column(name = "rating")
    private double rating;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "beer_picture_id")
    private Picture picture;


    public Beer() {

    }

    public Beer(String name, double abv, String description, Style style, Brewery brewery) {
        this.name = name;
        this.abv = abv;
        this.description = description;
        this.style = style;
        this.brewery = brewery;
    }

    //    public User getUser() {
//        return user.html;
//    }
//
//    public void setUser(User user.html) {
//        this.user.html = user.html;
//    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public Brewery getBrewery() {
        return brewery;
    }

    public void setBrewery(Brewery brewery) {
        this.brewery = brewery;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Boolean getExists() {
        return exists;
    }

    public void setExists(Boolean exists) {
        this.exists = exists;
    }

    public UserDetails getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(UserDetails createdBy) {
        this.createdBy = createdBy;
    }


    public Set<Beer> getDranklist() {
        return dranklist;
    }

    public void setDranklist(Set<Beer> dranklist) {
        this.dranklist = dranklist;
    }

    public Set<Beer> getWishlist() {
        return wishlist;
    }

    public void setWishlist(Set<Beer> wishlist) {
        this.wishlist = wishlist;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return String.format("%s", name);
    }
}
