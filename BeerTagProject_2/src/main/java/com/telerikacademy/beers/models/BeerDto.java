package com.telerikacademy.beers.models;

import javax.validation.constraints.*;
import java.util.List;
import java.util.Set;

public class BeerDto {
    @NotBlank
    @NotNull
    @Size(min = 2, max = 25, message = "Name size should be between 2 and 25 symbols")
    private String name;

    @NotBlank
    @NotNull
    @Size(min = 2, max = 25, message = "Description size should be between 2 and 25 symbols")
    private String description;

    @Positive
    private double abv;

    private int id;

    @Positive
    private int breweryId;

    private int countryId;

    @Positive
    private int styleId;

    private Set<String> tagId;

    @NotBlank
    @NotNull
    @Pattern(regexp ="(http(s?):)([/|.|\\w|\\s|-])*\\.(?:jpg|gif|png)", message = "Invalid URL")
    private String picture;

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public Set<String> getTagId() {
        return tagId;
    }

    public void setTagId(Set<String> tagId) {
        this.tagId = tagId;
    }

    public int getBreweryId() {
        return breweryId;
    }

    public void setBreweryId(int breweryId) {
        this.breweryId = breweryId;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAbv() {
        return abv;
    }

    public void setAbv(double abv) {
        this.abv = abv;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStyleId() {
        return styleId;
    }

    public void setStyleId(int styleId) {
        this.styleId = styleId;
    }

}
