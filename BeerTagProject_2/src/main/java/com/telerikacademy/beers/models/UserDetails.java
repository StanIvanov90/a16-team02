package com.telerikacademy.beers.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.*;



import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "users_details")
public class UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name ="user_id")
    private int id;

    @NotBlank
    @NotNull
    @Pattern(regexp = "[a-zA-Z0-9_.]*")
    @Size(min = 2, max = 25, message = "Name size should be between 2 and 25 symbols")
    @Column(name = "user_name")
    private String name;

    @Size(min = 6, max = 25, message = "Email size should be between 2 and 25 symbols")
    @Pattern(regexp ="^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$", message = "Email size should be between 2 and 25 symbols")
    @Column(name = "user_email")
    private String email;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "usersroles",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id")
    )
    private Set<Role> roles;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_dranklist",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private List<Beer> dranklist;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "users_wishlist",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "beer_id")
    )
    private Set<Beer> wishlist;

    @JsonIgnore
    @Column(name = "user_exists")
    private Boolean exists;

    @JsonIgnore
    @Column(name = "user_first_name")
    private String firstName;

    @JsonIgnore
    @Column(name = "user_last_name")
    private String lastname;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "user_picture_id")
    private Picture picture;

    public UserDetails() {
    }

    public UserDetails(String name, String email) {
        this.email = email;
        this.name = name;
    }

    public Picture getPicture() {
        return picture;
    }

    public void setPicture(Picture picture) {
        this.picture = picture;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public List<Beer> getDranklist() {
        return dranklist;
    }

    public void setDranklist(List<Beer> dranklist) {
        this.dranklist = dranklist;
    }

    public Set<Beer> getWishlist() {
        return wishlist;
    }

    public void setWishlist(Set<Beer> wishlist) {
        this.wishlist = wishlist;
    }

    public Boolean getExists() {
        return exists;
    }

    public void setExists(Boolean exists) {
        this.exists = exists;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }
}
