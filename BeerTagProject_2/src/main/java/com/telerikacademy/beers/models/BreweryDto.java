package com.telerikacademy.beers.models;

import javax.validation.constraints.Size;

public class BreweryDto {

    @Size(min = 5, max = 20, message = "Brewery name must be between {min} and {max} characters long.")
    private String name;

    private int countryId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountryId() {
        return countryId;
    }

    public void setCountryId(int countryId) {
        this.countryId = countryId;
    }
}
