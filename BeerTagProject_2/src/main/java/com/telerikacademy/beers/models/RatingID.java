package com.telerikacademy.beers.models;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class RatingID implements Serializable {

    @Column(name = "beer_id")
    private int beerId;

    @Column(name = "user_id")
    private int userId;

    public RatingID() {
    }

    public RatingID(Integer beerId, Integer userId) {
        this.beerId = beerId;
        this.userId = userId;
    }

    public int getBeerId() {
        return beerId;
    }

    public void setBeerId(int beerId) {
        this.beerId = beerId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RatingID ratingID = (RatingID) o;
        return getBeerId() == ratingID.getBeerId() &&
                getUserId() == ratingID.getUserId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getBeerId(), getUserId());
    }
}
