package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.*;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.RatingRepository;
import com.telerikacademy.beers.repositories.UserRepository;
import com.telerikacademy.beers.utils.BeersCollectionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

@Service
public class BeersServiceImpl implements BeersService {
    private BeerRepository repository;
    private UserRepository userRepository;
    private RatingRepository ratingRepository;



    @Autowired
    public BeersServiceImpl(BeerRepository repository, UserRepository userRepository, RatingRepository ratingRepository) {
        this.repository = repository;
        this.userRepository = userRepository;
        this.ratingRepository = ratingRepository;
    }

    @Override
    public void create(Beer beer) {
        if (repository.isDeleted(beer.getName())) {
            beer.setId(repository.deletedBeer(beer.getName()).getId());
            repository.update(beer);
        } else if (repository.checkBeerExists(beer.getName())) {
            throw new DuplicateEntityException(
                    String.format("Beer with name %s already exists", beer.getName()));
        } else {
            repository.create(beer);
        }
    }

    @Override
    public List<Beer> getAll() {
        return repository.getAll();
    }

    @Override
    public Beer getById(int id) {
        return repository.getById(id);
    }

    @Override
    public void update(Beer beer) {
//        if (beer.getCreatedBy().getId() != user.getId()
//                && !user.getRoles().stream().anyMatch(role -> role.getName().equals("Admin"))) {
//            throw new InvalidOperationException(String.format("User %s can not modify beer %d", user.getName(), id));
//        }
        repository.update(beer);
    }

    @Override
    public void delete(int id, UserDetails user) {
        Beer beer = repository.getById(id);
//        if (beer.getCreatedBy().getId() != user.getId()
//                && !user.getRoles().stream().anyMatch(role -> role.getName().equals("Admin"))) {
//            throw new InvalidOperationException(String.format("User %s can not modify beer %d", user.getName(), id));
//        }
        repository.delete(id);
    }

    @Override
    public List<Beer> sortBy(List<Beer> result, String sortBy) {
        if (sortBy != null && !sortBy.isEmpty()) {
            return repository.sortBy(result, sortBy);
        }
        return result;
    }

    @Override
    public List<Beer> filterByTag(List<Beer> result, String tag) {
        if (tag != null && !tag.isEmpty()) {
            return repository.filterByTag(result, tag);
        }
        return result;
    }

    @Override
    public List<Beer> filterByStyle(List<Beer> result, String style) {
        if (style != null && !style.isEmpty()) {
            return repository.filterByStyle(result, style);
        }
        return result;
    }

    @Override
    public List<Beer> filterByCountry(List<Beer> result, String country) {
        if (country != null && !country.isEmpty()) {
            return repository.filterByCountry(result, country);
        }
        return result;
    }

    @Override
    public void rateBeer(String beerName, int userId, double rating)
            throws NoAccessToRateBeerException, RatingOutOfRangeException {
        List<Beer> beers = repository.getByBeerName(beerName);
        if (beers.isEmpty()) {
            throw new EntityNotFoundException(String.format("Beer with name %s does not exist.", beerName));
        }
        Beer beer = beers.get(0);
        UserDetails user = userRepository.getById(userId);
        if (user == null) {
            throw new EntityNotFoundException(String.format("User with %d does not exist.", userId));
        }
        if (rating < 1 || rating > 5) {
            throw new RatingOutOfRangeException("Beer must be rated between 1 and 5.");
        }
        if(user.getDranklist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))){
            ratingRepository.rate(user,beer,rating);
        } else {
            throw new NoAccessToRateBeerException("You can only rate beers that you've drunk");
        }
        ratingRepository.getRating(beer);
    }

    @Override
    public Page<Beer> beersPage(Pageable pageable,List<Beer> beers) {
//        List<Beer> beers = getAll();
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Beer> beerSubList;
        if (beers.size() < startItem) {
            beerSubList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, beers.size());
            beerSubList = beers.subList(startItem, toIndex);
        }
        return new PageImpl<Beer>(beerSubList, PageRequest.of(currentPage, pageSize), beers.size());
    }

    @Override
    public Page<Beer> createdBeersPage(Pageable pageable, UserDetails user) {
        List<Beer> beers = userRepository.createdBeers(user.getName());
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Beer> beerSubList;
        if (beers.size() < startItem) {
            beerSubList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, beers.size());
            beerSubList = beers.subList(startItem, toIndex);
        }
        return new PageImpl<Beer>(beerSubList, PageRequest.of(currentPage, pageSize), beers.size());
    }
}
