package com.telerikacademy.beers.services;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Style;

import java.util.List;

public interface StylesService {
    List<Style> getAll();

    Style getById(int id);

    void create(Style style);

    void update(int id, Style style);

    void delete(int id);

    List<Style> filteredByName(String name);

    List<Beer> getStyleBeers(int styleId);
}
