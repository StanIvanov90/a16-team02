package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class UsersServiceImpl implements UsersService {
    private UserRepository repository;
    private BeerRepository beerRepository;
    private BeersService beersService;

    @Autowired
    public UsersServiceImpl(UserRepository repository, BeerRepository beerRepository, BeersService beersService) {
        this.repository = repository;
        this.beerRepository = beerRepository;
        this.beersService = beersService;
    }

    @Override
    public void create(UserDetails user) {
        if (repository.isDeleted(user.getName())) {
            if (repository.checkEmailExists(user.getEmail())) {
                throw new DuplicateEntityException(
                        String.format("User with email %s already exists", user.getEmail()));
            }
            user.setId(repository.getUserID(user.getName()));
            repository.update(user, user.getId());

        } else if (repository.checkUserExists(user.getName())) {
            throw new DuplicateEntityException(
                    String.format("User with name %s already exists", user.getName()));
        } else if (repository.checkEmailExists(user.getEmail())) {
            throw new DuplicateEntityException(
                    String.format("User with email %s already exists", user.getEmail()));
        }
        else {
            repository.create(user);
        }
    }

    @Override
    public List<UserDetails> getAll() {
        return repository.getAll();
    }

    @Override
    public UserDetails getById(int id) {
        return repository.getById(id);
    }

    @Override
    public UserDetails getByUsername(String username) {
        return repository.getByUsername(username);
    }

    @Override
    public void update(UserDetails user, int id) {

        if (repository.checkIfExists(id)) {
            throw new EntityNotFoundException("User", id);
        }
        repository.update(user, id);
    }

    @Override
    public void delete(int id) {
        repository.delete(id);
    }

    @Override
    public Set<Beer> getWishlist(String username) {
        if (!repository.checkUserExists(username)) {
            throw new EntityNotFoundException("User with this name doesn't exist");
        }
        return repository.getUserWishList(username);
    }

    @Override
    public void addBeerToWishList(String username, int beerId) {
        if (!repository.checkUserExists(username)) {
            throw new EntityNotFoundException("User with this name doesn't exist");
        }
        UserDetails user = repository.getByUsername(username);
        Beer beer = beersService.getById(beerId);
        if (user.getWishlist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
            throw new DuplicateEntityException("You have already drank this beer.");
        }
        if (user.getDranklist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
            throw new DuplicateEntityException("You have already drank this beer.");
        }
        repository.addBeerToWishList(user, beer);
    }

    @Override
    public List<Beer> topThreeBeers(String username) {
        if (!repository.checkUserExists(username)) {
            throw new EntityNotFoundException("User with this name doesn't exist");
        }
        return repository.getTopThreeBeers(username);
    }


    @Override
    public void addBeerToDrankList(String username, int beerId) {
        if (!repository.checkUserExists(username)) {
            throw new EntityNotFoundException("User with this name doesn't exist");
        }
        UserDetails user = repository.getByUsername(username);
        Beer beer = beersService.getById(beerId);
        if (user.getDranklist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
            throw new DuplicateEntityException("You have already drank this beer.");
        }
//        if (user.getWishlist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
//            throw new DuplicateEntityException("You have already drank this beer.");
//        }
        repository.addBeerToDrankList(user, beer);
    }

    @Override
    public List<Beer> getDranklist(String username) {
        if (!repository.checkUserExists(username)) {
            throw new EntityNotFoundException("User with this name doesn't exist");
        }
        return repository.getUserDrankList(username);
    }

    @Override
    public List<Beer> createdBeers(String name) {
        if (!repository.checkUserExists(name)) {
            throw new EntityNotFoundException("User with this name doesn't exist");
        }
        return repository.createdBeers(name);
    }

    @Override
    public Page<UserDetails> UserDetailsPage(Pageable pageable) {
        List<UserDetails> userDetailsList = getAll();
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<UserDetails> userDetailsSubList;
        if (userDetailsList.size() < startItem) {
            userDetailsSubList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, userDetailsList.size());
            userDetailsSubList = userDetailsList.subList(startItem, toIndex);
        }
        return new PageImpl<UserDetails>(userDetailsSubList, PageRequest.of(currentPage, pageSize), userDetailsList.size());    }
}
