package com.telerikacademy.beers.services;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.UserDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Set;

public interface UsersService {

    void create(UserDetails user);

    List<UserDetails> getAll();

    UserDetails getById(int id);

    UserDetails getByUsername(String username);

    void update(UserDetails user, int id);

    void delete(int id);

    Set<Beer> getWishlist(String username);

    void addBeerToDrankList(String username,int beerId);

    void addBeerToWishList(String username,int beerId);

    List<Beer> topThreeBeers(String username);

    List<Beer> getDranklist(String username);

    List<Beer> createdBeers(String name);

    Page<UserDetails> UserDetailsPage(Pageable pageable);
}
