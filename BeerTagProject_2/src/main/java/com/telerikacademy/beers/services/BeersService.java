package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.NoAccessToRateBeerException;
import com.telerikacademy.beers.exceptions.RatingOutOfRangeException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.UserDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface BeersService {

    void create(Beer beer);

    List<Beer> getAll();

    Beer getById(int id);

    void update(Beer beer);


    void delete(int id, UserDetails user);

    List<Beer> sortBy(List<Beer> result, String sortBy);

    List<Beer> filterByTag(List<Beer> result, String tag);

    List<Beer> filterByStyle(List<Beer> result, String style);

    List<Beer> filterByCountry(List<Beer> result, String country);

    void rateBeer(String beerName, int userId, double rating) throws NoAccessToRateBeerException, RatingOutOfRangeException;

    Page<Beer> beersPage(Pageable pageable,List<Beer> beers);

    Page<Beer> createdBeersPage(Pageable pageable,UserDetails user);

}
