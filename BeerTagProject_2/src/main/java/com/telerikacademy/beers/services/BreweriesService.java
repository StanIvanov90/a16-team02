package com.telerikacademy.beers.services;

import com.telerikacademy.beers.models.Brewery;

import java.util.List;

public interface BreweriesService {
    List<Brewery> getAll();

    Brewery getById(int id);

    List<Brewery> filteredByName(String name);

    List<Brewery> filteredByCountry(String country);

    List<Brewery> filteredByNameAndCountry(String name, String country);

    void create(Brewery brewery);

    void update(int id, Brewery brewery);

    void delete(int id);

}
