package com.telerikacademy.beers.services;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Tag;

import java.util.List;

public interface TagsService {
    List<Tag> getAll();

    Tag getById(int id);

    void create(Tag tag);

    void update(int id, Tag tag);

    void delete(int id);

    List<Beer> getTagBeers(int tagId);
}
