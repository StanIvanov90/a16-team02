package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Style;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.StylesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StylesServiceImpl implements StylesService {
    private StylesRepository stylesRepository;
    private BeerRepository beerRepository;

    @Autowired
    public StylesServiceImpl(StylesRepository stylesRepository, BeerRepository beerRepository) {
        this.stylesRepository = stylesRepository;
        this.beerRepository=beerRepository;
    }

    @Override
    public List<Style> getAll() {
        return stylesRepository.getAll();
    }

    @Override
    public Style getById(int id) {
        return stylesRepository.getById(id);
    }

    @Override
    public void create(Style style) {
        if (stylesRepository.isDeleted(style.getName())) {
            style = stylesRepository.deletedStyle(style.getName());
            stylesRepository.update(style.getId(), style);
        }else if(stylesRepository.checkStyleExists(style.getName())){
            throw new DuplicateEntityException(
                    String.format("Style with name %s already exists", style.getName())
            );
        }else{
            stylesRepository.create(style);
        }
    }

    @Override
    public void update(int id, Style style) {
         stylesRepository.update(id, style);
    }

    @Override
    public void delete(int id) {
        if(!beerRepository.checkIfStyleHasBeer(id).isEmpty()){
            Style style = stylesRepository.getById(id);
            throw new ReferentialIntegrityViolationException(
                    String.format("Style with name %s has beers. Please delete them or change their style.", style.getName()));
        }

        stylesRepository.delete(id);
    }

    @Override
    public List<Style> filteredByName(String name) {
        return stylesRepository.getByName(name);
    }

    @Override
    public List<Beer> getStyleBeers(int styleId) {
        return beerRepository.getByStyle(styleId);
    }
}
