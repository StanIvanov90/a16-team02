package com.telerikacademy.beers.services;

import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Country;

import java.util.List;

public interface CountriesService {

    void create(Country country);

    List<Country> getAll();

    Country getById(int id);

    void update(int id, Country country);

    void delete(int id);

    List<Beer> getCountryBeers(int countryId);
}
