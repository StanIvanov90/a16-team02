package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Country;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.CountriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CountriesServiceImpl implements CountriesService {
    private CountriesRepository countriesRepository;
    private BeerRepository beerRepository;

    @Autowired
    public CountriesServiceImpl(CountriesRepository countriesRepository, BeerRepository beerRepository) {
        this.countriesRepository = countriesRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public void create(Country country) {
        if (countriesRepository.isDeleted(country.getName())) {
            country = countriesRepository.deletedCountry(country.getName());
            countriesRepository.update(country.getId(),country);
        } else if (countriesRepository.checkCountryExists(country.getName())) {
            throw new DuplicateEntityException(
                    String.format("Country with name %s already exists", country.getName())
            );
        } else {
            countriesRepository.create(country);
        }
    }


    @Override
    public List<Country> getAll() {
       return  countriesRepository.getAll();
    }

    @Override
    public Country getById(int id) {
        return countriesRepository.getById(id);
    }

    @Override
    public void update(int id, Country country) {
         countriesRepository.update(id, country);
    }

    @Override
    public void delete(int id) {
        if(!beerRepository.checkIfCountryHasBeers(id).isEmpty()){
            Country country = countriesRepository.getById(id);
            throw new ReferentialIntegrityViolationException(
                    String.format("Country with name %s has beers. Please delete them or change their country.", country.getName()));
        }
        countriesRepository.delete(id);
    }

    @Override
    public List<Beer> getCountryBeers(int countryId) {

        return beerRepository.getByCountry(countryId);
    }
}
