package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Brewery;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.BreweriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BreweriesServiceImpl implements BreweriesService {
    private BreweriesRepository breweriesRepository;
    private BeerRepository beerRepository;

    @Autowired
    public BreweriesServiceImpl(BreweriesRepository breweriesRepository, BeerRepository beerRepository) {
        this.breweriesRepository = breweriesRepository;
        this.beerRepository = beerRepository;
    }

    @Override
    public List<Brewery> getAll() {
        return breweriesRepository.getAll();
    }

    @Override
    public Brewery getById(int id) {
        return breweriesRepository.getById(id);
    }

    @Override
    public List<Brewery> filteredByName(String name) {
        return breweriesRepository.getByName(name);
    }

    @Override
    public List<Brewery> filteredByCountry(String country) {
        return breweriesRepository.getByCountry(country);
    }

    @Override
    public List<Brewery> filteredByNameAndCountry(String name, String country) {
        List<Brewery> breweries = breweriesRepository.getByName(name);
        return breweries.stream().filter(brewery -> brewery.getCountry().getName().contains(country)).collect(Collectors.toList());
    }

    @Override
    public void create(Brewery brewery) {
        if (breweriesRepository.isDeleted(brewery.getName())) {
            brewery.setId(breweriesRepository.getBreweryID(brewery.getName()));
            breweriesRepository.update(brewery.getId(), brewery);
        } else if (breweriesRepository.checkBreweryExists(brewery.getName())) {
            throw new DuplicateEntityException(
                    String.format("Brewery with name %s already exists", brewery.getName())
            );
        } else {
            breweriesRepository.create(brewery);
        }
    }

    @Override
    public void update(int id, Brewery brewery) {
        breweriesRepository.update(id, brewery);
    }

    @Override
    public void delete(int id) {
        if(!beerRepository.checkIfBreweryHasBeers(id).isEmpty()){
            Brewery brewery = breweriesRepository.getById(id);
            throw new ReferentialIntegrityViolationException(
                    String.format("Brewery with name %s has beers. Please delete them or change their brewery.", brewery.getName()));
        }
        breweriesRepository.delete(id);
    }
}
