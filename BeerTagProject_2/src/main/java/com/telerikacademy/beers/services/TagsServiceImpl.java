package com.telerikacademy.beers.services;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Tag;
import com.telerikacademy.beers.repositories.BeerRepository;
import com.telerikacademy.beers.repositories.TagsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TagsServiceImpl implements TagsService {
    private TagsRepository tagsRepository;
    private BeerRepository beerRepository;

    @Autowired
    public TagsServiceImpl(TagsRepository tagsRepository, BeerRepository beerRepository) {
        this.tagsRepository = tagsRepository;
        this.beerRepository = beerRepository;
    }


    @Override
    public List<Tag> getAll() {
        return tagsRepository.getAll();
    }

    @Override
    public Tag getById(int id) {
        return tagsRepository.getById(id);
    }

    @Override
    public void create(Tag tag) {

        if (tagsRepository.isDeleted(tag.getName())) {
            tag.setId(tagsRepository.getTagID(tag.getName()));
            tagsRepository.update(tag.getId(), tag);
        } else if (tagsRepository.checkTagExists(tag.getName())) {
            throw new DuplicateEntityException(
                    String.format("Tag with name %s already exists", tag.getName())
            );
        } else {
            tagsRepository.create(tag);
        }
    }

    @Override
    public void update(int id, Tag tag) {
        if (tagsRepository.checkIfExists(id)) {
            throw new EntityNotFoundException("Tag", id);
        }
        tagsRepository.update(id, tag);
    }

    @Override
    public void delete(int id) {
        if(!beerRepository.checkIfTagHasBeers(id).isEmpty()){
            Tag tag = tagsRepository.getById(id);
            throw new ReferentialIntegrityViolationException(
                    String.format("Tag with name %s has beers. Please delete them or change their tag.", tag.getName()));
        }

        tagsRepository.delete(id);
    }

    @Override
    public List<Beer> getTagBeers(int tagId) {
        return beerRepository.getByTag(tagId);
    }
}
