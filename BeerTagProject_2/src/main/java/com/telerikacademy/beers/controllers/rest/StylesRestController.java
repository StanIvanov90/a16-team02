package com.telerikacademy.beers.controllers.rest;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Style;
import com.telerikacademy.beers.services.StylesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/styles")
public class StylesRestController {
    private StylesService stylesService;

    @Autowired
    public StylesRestController(StylesService stylesService) {
        this.stylesService = stylesService;
    }

    @GetMapping
    public List<Style> getAll(@RequestParam(value = "name", required = false)String name) {
        if (name != null && !name.isEmpty()) {
            return stylesService.filteredByName(name);
        }
        return stylesService.getAll();
    }

    @GetMapping("/{id}")
    public Style getById(@PathVariable int id) {
        try {
            return stylesService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Style create(@RequestBody @Valid Style style) {
        try {
             stylesService.create(style);
            return style;
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Style update(@PathVariable int id, @RequestBody @Valid Style style) {
        try {
            stylesService.update(id, style);
            return style;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            stylesService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ReferentialIntegrityViolationException e1){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e1.getMessage());
        }
    }

    @GetMapping("/{styleId}/beers")
    public List<Beer> getStyleBeers(@PathVariable int styleId) {
        try {
            return stylesService.getStyleBeers(styleId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
