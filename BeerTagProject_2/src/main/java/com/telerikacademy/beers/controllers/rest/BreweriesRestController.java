package com.telerikacademy.beers.controllers.rest;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Brewery;
import com.telerikacademy.beers.models.BreweryDto;
import com.telerikacademy.beers.models.DtoMapper;
import com.telerikacademy.beers.services.BreweriesService;
import com.telerikacademy.beers.services.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/breweries")
public class BreweriesRestController {
    private BreweriesService breweriesService;
    private CountriesService countryService;
    private DtoMapper mapper;

    @Autowired
    public BreweriesRestController(BreweriesService breweriesService, CountriesService countryService, DtoMapper mapper) {
        this.breweriesService = breweriesService;
        this.countryService = countryService;
        this.mapper = mapper;
    }


    @GetMapping
    public List<Brewery> getAll(@RequestParam(value = "name", required = false) String name,
                                @RequestParam(value = "country", required = false) String countryOrigin) {
        if (name != null && !name.isEmpty()) {
            if (countryOrigin != null && !countryOrigin.isEmpty()) {
                return breweriesService.filteredByNameAndCountry(name, countryOrigin);
            } else {
                return breweriesService.filteredByName(name);
            }
        }
        return breweriesService.getAll();
    }

    @GetMapping("/{id}")
    public Brewery getById(@PathVariable int id) {
        try {
            return breweriesService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Brewery create(@RequestBody @Valid BreweryDto breweryDto) {
        try {
            Brewery newBrewery = mapper.fromDto(breweryDto);
            newBrewery.setName(breweryDto.getName());
            newBrewery.setCountry(countryService.getById(breweryDto.getCountryId()));
            breweriesService.create(newBrewery);
            return newBrewery;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Brewery update(@PathVariable int id, @RequestBody @Valid BreweryDto breweryDto) {
        try {
            Brewery newBrewery = breweriesService.getById(id);
            newBrewery = mapper.updateBreweryFromDto(newBrewery, breweryDto);
            breweriesService.update(id, newBrewery);
            return newBrewery;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            breweriesService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ReferentialIntegrityViolationException e1){
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e1.getMessage());
        }
    }

}
