package com.telerikacademy.beers.controllers;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.models.DtoMapper;
import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.models.UserDto;
import com.telerikacademy.beers.repositories.UserRepository;
import com.telerikacademy.beers.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@Controller
public class RegistrationController {
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder;
    private UsersService usersService;
    private DtoMapper dtoMapper;


    @Autowired
    public RegistrationController(UserDetailsManager userDetailsManager, PasswordEncoder passwordEncoder, UsersService usersService, DtoMapper dtoMapper) {
        this.userDetailsManager = userDetailsManager;
        this.passwordEncoder = passwordEncoder;
        this.usersService = usersService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/register")
    public String showRegisterPage(Model model) {
        model.addAttribute("userDto", new UserDto());
        return "register";
    }

    @PostMapping("/register")
    public String registerUser(@Valid @ModelAttribute UserDto userDto, BindingResult bindingResult, Model model) {

        if (!userDto.getPassword().equals(userDto.getPasswordConfirmation())) {
            model.addAttribute("error", "Password does't match!");
            return "register";
        }

        if (bindingResult.hasErrors()) {

            model.addAttribute("error", "Username/password/email can't be empty!");
            return "register";
        }

        if (userDetailsManager.userExists(userDto.getName())) {
            model.addAttribute("error", "User with the same username already exists!");
            return "register";
        }


//        if (userRepository.checkEmailExists(userDto.getEmail())) {
//            model.addAttribute("error", "This e-mail has been already registered.");
//            return "register";
//        }


        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("ROLE_USER");
        org.springframework.security.core.userdetails.User newUser =
                new org.springframework.security.core.userdetails.User(
                        userDto.getName(),
                        passwordEncoder.encode(userDto.getPassword()),
                        authorities);
        try {
            UserDetails user = dtoMapper.userFromDto(userDto);
            usersService.create(user);
        } catch (DuplicateEntityException de){
            model.addAttribute("error", "This e-mail has been already registered.");
            return "register";
        }
//        UserDetails user.html = dtoMapper.userFromDto(userDto);
//        usersService.create(user.html);
        userDetailsManager.createUser(newUser);


        return "register-confirmation";
    }

    @GetMapping("/register-confirmation")
    public String showRegisterConfirmation() {
        return "register-confirmation";
    }


}
