package com.telerikacademy.beers.controllers.rest;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Country;
import com.telerikacademy.beers.models.DtoMapper;
import com.telerikacademy.beers.services.CountriesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/countries")
public class CountriesRestController {
    private CountriesService countriesService;

    @Autowired
    public CountriesRestController(CountriesService countriesService) {
        this.countriesService = countriesService;
    }

    @GetMapping
    public List<Country> getAll() {
        return countriesService.getAll();
    }

    @GetMapping("/{id}")
    public Country getById(@PathVariable int id) {
        try {
            return countriesService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Country create(@RequestBody @Valid Country country) {
        try {
            countriesService.create(country);
            return country;
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Country update(@PathVariable int id, @RequestBody @Valid Country country) {
        try {
            countriesService.update(id, country);
            return country;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            countriesService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (ReferentialIntegrityViolationException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e1.getMessage());
        }
    }

    @GetMapping("/{countryId}/beers")
    public List<Beer> getCountryBeer(@PathVariable int countryId) {
        try {
            return countriesService.getCountryBeers(countryId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
