package com.telerikacademy.beers.controllers.rest;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.DtoMapper;
import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.models.UserDto;
import com.telerikacademy.beers.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api/users")
public class UsersRestController {
    private UsersService service;
    private DtoMapper mapper;

    @Autowired
    public UsersRestController(UsersService service, DtoMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    public List<UserDetails> getAll() {
        try {
            return service.getAll();
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

//    @GetMapping("/{id}")
//    public User getById(@PathVariable int id) {
//        try {
//            return service.getById(id);
//        } catch (EntityNotFoundException e) {
//            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
//        }
//    }


    @GetMapping("/{username}")
    public UserDetails getByName(@PathVariable String username) {
        try {
            return service.getByUsername(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public UserDetails create(@RequestBody @Valid UserDto userDto) {
        try {
            UserDetails newUser = mapper.userFromDto(userDto);
            service.create(newUser);
            return newUser;
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserDetails update(@RequestBody @Valid UserDetails user, @PathVariable int id) {
        try {
            UserDetails newUser = service.getById(id);
            newUser.setName(user.getName());
            newUser.setEmail(user.getEmail());
            service.update(user, id);
            return user;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            service.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping("/{username}/dranklist/{beerId}")
    public void addBeerToDrankList(@PathVariable String username,
                                   @PathVariable int beerId) {
        try {
            service.addBeerToDrankList(username, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException de) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, de.getMessage());
        }
    }

    @PostMapping("/{username}/wishlist/{beerId}")
    public void addBeerToWishList(@PathVariable String username,
                                   @PathVariable int beerId) {
        try {
            service.addBeerToWishList(username, beerId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException de) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, de.getMessage());
        }
    }


    @GetMapping("/{username}/dranklist")
    public List<Beer> getDranklist(@PathVariable String username) {
        try {
            return service.getDranklist(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{username}/wishlist")
    public Set<Beer> getWishlist(@PathVariable String username) {
        try {
            return service.getWishlist(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{username}/topthree")
    public List<Beer> top3beers(@PathVariable String username) {
        try {
            return service.topThreeBeers(username);
        } catch (EntityNotFoundException  e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{username}/createdBeers")
    public List<Beer> createdBeers(@PathVariable String username) {
        try {
            return service.createdBeers(username);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}

