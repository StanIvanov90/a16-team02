package com.telerikacademy.beers.controllers;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.NoAccessToRateBeerException;
import com.telerikacademy.beers.exceptions.RatingOutOfRangeException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public String handleIdError(EntityNotFoundException e){
        return "error-404.html";
    }

    @ExceptionHandler(NoAccessToRateBeerException.class)
    public String handleRateBeerError(NoAccessToRateBeerException e){
        return "cantRateBeer.html";
    }
    @ExceptionHandler(DuplicateEntityException.class)
    public String handleDuplicateError(DuplicateEntityException e){
        return "duplicate.html";
    }
    @ExceptionHandler(RatingOutOfRangeException.class)
    public String handleRatingOutOfRange(RatingOutOfRangeException e){
        return "access-denied.html";
    }
}
