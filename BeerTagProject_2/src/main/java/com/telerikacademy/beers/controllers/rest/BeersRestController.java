package com.telerikacademy.beers.controllers.rest;

import com.telerikacademy.beers.exceptions.*;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.BeerDto;
import com.telerikacademy.beers.models.DtoMapper;

import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;


import java.util.List;


@RestController
@RequestMapping("/api/beers")
public class BeersRestController {
    private BeersService service;
    private DtoMapper mapper;
    private UsersService usersService;

    @Autowired
    public BeersRestController(BeersService service, DtoMapper mapper, UsersService usersService) {
        this.service = service;
        this.mapper = mapper;
        this.usersService = usersService;
    }

    @GetMapping
    public List<Beer> getAll(@RequestParam(defaultValue = "") String style,
                             @RequestParam(defaultValue = "") String country,
                             @RequestParam(defaultValue = "") String tag,
                             @RequestParam(defaultValue = "") String sort) {
        try {
            List<Beer> result = service.getAll();
            result = service.sortBy(result, sort);
            result = service.filterByTag(result, tag);
            result = service.filterByCountry(result, country);
            result = service.filterByStyle(result, style);
            return result;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public Beer getById(@PathVariable int id) {
        try {
            return service.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Beer create(@RequestBody @Valid BeerDto beerDto, @RequestHeader(name = "Authorization") String authorization) {
        try {
            UserDetails requestUser = usersService.getByUsername(authorization);
            Beer newBeer = mapper.fromDto(beerDto);
            newBeer.setCreatedBy(requestUser);
            service.create(newBeer);
            return newBeer;
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Beer update(@PathVariable int id, @RequestBody @Valid BeerDto beerDto, @RequestHeader(name = "Authorization") String authorization) {
        try {
            UserDetails requestUser = usersService.getByUsername(authorization);
            Beer newBeer = mapper.fromDto(beerDto);
            UserDetails userOfOldBeer = service.getById(id).getCreatedBy();
            newBeer.setCreatedBy(userOfOldBeer);
            service.update(newBeer);
            return newBeer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader(name = "Authorization") String authorization) {
        try {
            UserDetails user = usersService.getByUsername(authorization);
            service.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (InvalidOperationException e) {
        throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getLocalizedMessage());
    }
}


    @PutMapping("/{id}/wishbeer")
    public void addBeerToWishlist(@PathVariable int id, @RequestHeader(name = "Authorization") String authorization) {
        try {
            UserDetails user = usersService.getByUsername(authorization);
            Beer beer = service.getById(id);
            usersService.addBeerToWishList(user.getName(), beer.getId());
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}/drinkbeer")
    public void addBeerToDranklist(@PathVariable int id, @RequestHeader(name = "Authorization") String authorization) {
        try {
            UserDetails user = usersService.getByUsername(authorization);
            Beer beer = service.getById(id);
            usersService.addBeerToDrankList(user.getName(), beer.getId());
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @PutMapping("/{id}/rate/{rating}")
    public void rateBeer(@PathVariable int id, @PathVariable double rating, @RequestHeader(name = "Authorization") String authorization) {
        try {
            UserDetails user = usersService.getByUsername(authorization);
            Beer beer = service.getById(id);
            service.rateBeer(beer.getName(), user.getId(), rating);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (NoAccessToRateBeerException ex) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, ex.getMessage());
        } catch (RatingOutOfRangeException er) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, er.getMessage());
        }
    }


}
