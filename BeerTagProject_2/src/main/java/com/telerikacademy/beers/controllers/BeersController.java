package com.telerikacademy.beers.controllers;

import com.sun.org.apache.xpath.internal.operations.Mod;
import com.telerikacademy.beers.exceptions.NoAccessToRateBeerException;
import com.telerikacademy.beers.exceptions.RatingOutOfRangeException;
import com.telerikacademy.beers.models.*;
import com.telerikacademy.beers.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class BeersController {
    private BeersService service;
    private StylesService stylesService;
    private DtoMapper dtoMapper;
    private BreweriesService breweriesService;
    private TagsService tagsService;
    private UsersService usersService;

    @Autowired
    public BeersController(BeersService service,
                           StylesService stylesService,
                           DtoMapper dtoMapper,
                           BreweriesService breweriesService,
                           TagsService tagsService,
                           UsersService usersService) {
        this.service = service;
        this.stylesService = stylesService;
        this.dtoMapper = dtoMapper;
        this.breweriesService = breweriesService;
        this.tagsService = tagsService;
        this.usersService = usersService;
    }

    @ModelAttribute("styles")
    public List<Style> populateStyles() {
        return stylesService.getAll();
    }

    @ModelAttribute("breweries")
    public List<Brewery> populateBrewery() {
        return breweriesService.getAll();
    }

    @ModelAttribute("tags")
    public List<Tag> populateTags() {
        return tagsService.getAll();
    }

    @GetMapping("/beers/new")
    public String showNewBeerForm(Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            model.addAttribute("beer", new BeerDto());
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            model.addAttribute("user", userDetails);
            return "beer";
        }
        return "access-denied";
    }

    @PostMapping("/beers/new")
    public String createBeer(@Valid @ModelAttribute("beer") BeerDto beer,
                             BindingResult errors,
                             Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
        if (errors.hasErrors()) {
            return "beer";
        }
        Beer newBeer = dtoMapper.fromDto(beer);
        newBeer.setCreatedBy(usersService.getByUsername(principal.getName()));
        service.create(newBeer);
        return "redirect:/beers";
    }   return "access-denied";
    }


    @GetMapping("/beers/{id}")
    public String showBeerPage(Model model, @PathVariable int id) {
        Beer beer = getVerifiedBeer(id);
        model.addAttribute("beer", beer);
        return "beerId";
    }

    private Beer getVerifiedBeer(int id) {
        try {
            return service.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Beer not found!");
        }
    }


    @GetMapping("/beers")
    public String showBeers(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                            @RequestParam(name = "size", defaultValue = "6") int pageSize,
                            @RequestParam(defaultValue = "") String sort,
                            @RequestParam(defaultValue = "") String tag,
                            @RequestParam(defaultValue = "") String style,
                            @RequestParam(defaultValue = "") String country,
                            Model model,
                            Principal principal,
                            SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            List<Beer> result = service.getAll();
            result = service.sortBy(result, sort);
            result = service.filterByTag(result, tag);
            result = service.filterByCountry(result, country);
            result = service.filterByStyle(result, style);
            model.addAttribute("sort", sort);
            model.addAttribute("tag", tag);
            model.addAttribute("style", style);
            model.addAttribute("country", country);
            UserDetails user = usersService.getByUsername(principal.getName());
            model.addAttribute("user", user);
            Page<Beer> beerPage = service.beersPage(PageRequest.of(currentPage - 1, pageSize), result);
            model.addAttribute("beerPage", beerPage);
            int totalPages = beerPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "beers";
        } else {
            List<Beer> result = service.getAll();
            result = service.sortBy(result, sort);
            result = service.filterByTag(result, tag);
            result = service.filterByCountry(result, country);
            result = service.filterByStyle(result, style);
            model.addAttribute("sort", sort);
            model.addAttribute("tag", tag);
            model.addAttribute("style", style);
            model.addAttribute("country", country);

            Page<Beer> beerPage = service.beersPage(PageRequest.of(currentPage - 1, pageSize), result);
            model.addAttribute("beerPage", beerPage);
            int totalPages = beerPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "beers";
        }
    }


    @GetMapping("/beers/myBeers")
    public String showCreatedBeers(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                                   @RequestParam(name = "size", defaultValue = "6") int pageSize,
                                   Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
        UserDetails userDetails = usersService.getByUsername(principal.getName());
        Page<Beer> beerPage = service.createdBeersPage(PageRequest.of(currentPage - 1, pageSize), userDetails);
        model.addAttribute("beerPage", beerPage);
        int totalPages = beerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        model.addAttribute("user", userDetails);
        return "createdBeer";
    }    return "access-denied";
    }

    @GetMapping("/beers/update/{id}")
    public String showBeerUpdateForm(@PathVariable int id,
                                     Model model,
                                     Principal principal,
                                     SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {

            Beer beerToUpdate = service.getById(id);
            BeerDto beerDto = new BeerDto();
            beerDto.setName(beerToUpdate.getName());
            beerDto.setAbv(beerToUpdate.getAbv());
            beerDto.setId(beerDto.getId());
            beerDto.setBreweryId(beerToUpdate.getBrewery().getId());
            beerDto.setDescription(beerToUpdate.getDescription());
            beerDto.setPicture(beerToUpdate.getPicture().getUrl());
            beerDto.setStyleId(beerToUpdate.getStyle().getId());
//        beerDto.setCreatedBy(usersService.getByUsername(principal.getName()));


            model.addAttribute("beer", beerToUpdate);
            model.addAttribute("styles", stylesService.getAll());
            model.addAttribute("breweries", breweriesService.getAll());
            model.addAttribute("tags", tagsService.getAll());
            model.addAttribute("beerDto", beerDto);

            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");

            if (beerToUpdate.getCreatedBy().getName().equals(principal.getName()) || isAdmin) {
                return "updatebeer";
            }
        }

        return "access-denied";
    }


    @PostMapping("/beers/update/{id}")
    public String updateBeer(@PathVariable int id, @Valid @ModelAttribute("beerDto") BeerDto beerDto) {
        Beer beerToUpdate = service.getById(id);
        service.update(dtoMapper.updateBeerFromDto(beerToUpdate, beerDto));


        return "redirect:/beers/" + id;
    }

    @RequestMapping("/beers/delete/{id}")
    public String delete(@PathVariable int id, RedirectAttributes redirectAttributes,
                         Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            Beer beer = service.getById(id);
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (isAdmin || beer.getCreatedBy().getName().equals(principal.getName())) {
                beer.setCreatedBy(userDetails);
                service.delete(id, userDetails);
                redirectAttributes.addFlashAttribute("message", "Beer was deleted!");
                return "redirect:/beers";
            }
        }

        return "access-denied";
    }


    @RequestMapping("/{id}/wishbeer")
    public String addBeerToWishlist(@PathVariable int id, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            Beer beer = service.getById(id);
            if (userDetails.getDranklist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
                return "dranklistError";
            }
            if (userDetails.getWishlist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
                return "wishlistError";
            }
            usersService.addBeerToWishList(userDetails.getName(), beer.getId());
            return "wishSuccess";
        }
        return "access-denied";
    }

    @RequestMapping("/{id}/drankbeer")
    public String addBeerToDranklist(@PathVariable int id, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            Beer beer = service.getById(id);
            if (userDetails.getDranklist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
                return "dranklistError";
            }
//            if (userDetails.getWishlist().stream().anyMatch(beer1 -> beer1.getName().equals(beer.getName()))) {
//                return "access-denied";
//            }
            usersService.addBeerToDrankList(userDetails.getName(), beer.getId());
            return "drankSuccess";

        }
        return "access-denied";
    }


    @GetMapping("/beers/rate/{id}")
    public String showRateBeerFormView(@PathVariable int id, Model model) {
        Beer beer = service.getById(id);
        Rating rating = new Rating();
        model.addAttribute("beer", beer);
        model.addAttribute("newRating", rating);
        return "rateBeer";
    }

    @PostMapping("/beers/rate/{beerid}")
    public String rateBeer(@PathVariable int beerid, Principal principal, @ModelAttribute("newRating") Rating rating, SecurityContextHolderAwareRequestWrapper request)
            throws NoAccessToRateBeerException, RatingOutOfRangeException {

            if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
                Beer beer = service.getById(beerid);
                UserDetails user = usersService.getByUsername(principal.getName());
                int userId = user.getId();
                int newRating2 = (int) rating.getRating();
                service.rateBeer(beer.getName(), userId, newRating2);

                return "redirect:/beers";
            }
            return "access-denied";
    }
}