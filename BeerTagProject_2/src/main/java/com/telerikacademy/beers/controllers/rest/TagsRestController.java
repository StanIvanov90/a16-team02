package com.telerikacademy.beers.controllers.rest;

import com.telerikacademy.beers.exceptions.DuplicateEntityException;
import com.telerikacademy.beers.exceptions.EntityNotFoundException;
import com.telerikacademy.beers.exceptions.ReferentialIntegrityViolationException;
import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.Tag;
import com.telerikacademy.beers.services.TagsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/tags")
public class TagsRestController {
    private TagsService tagsService;

    @Autowired
    public TagsRestController(TagsService tagsService) {
        this.tagsService = tagsService;
    }

    @GetMapping
    public List<Tag> getAll() {

        return tagsService.getAll();
    }

    @GetMapping("/{id}")
    public Tag getById(@PathVariable int id) {
        try {
            return tagsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    public Tag create(@RequestBody @Valid Tag tag) {
        try {
            tagsService.create(tag);
            return tag;
        } catch (EntityNotFoundException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e1.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Tag update(@PathVariable int id, @RequestBody @Valid Tag tag) {
        try {
            Tag newTag = tagsService.getById(id);
            newTag.setName(tag.getName());
            tagsService.update(id, newTag);
            return newTag;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            tagsService.delete(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (
                ReferentialIntegrityViolationException e1) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE, e1.getMessage());
        }
    }

    @GetMapping("/{tagId}/beers")
    public List<Beer> getTagBeer(@PathVariable int tagId) {
        try {
            return tagsService.getTagBeers(tagId);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
