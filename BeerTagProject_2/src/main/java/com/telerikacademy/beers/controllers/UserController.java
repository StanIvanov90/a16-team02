package com.telerikacademy.beers.controllers;


import com.telerikacademy.beers.models.Beer;
import com.telerikacademy.beers.models.DtoMapper;
import com.telerikacademy.beers.models.UserDetails;
import com.telerikacademy.beers.models.UserDto;
import com.telerikacademy.beers.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
public class UserController {

    private BeersService service;
    private StylesService stylesService;
    private DtoMapper dtoMapper;
    ;
    private BreweriesService breweriesService;
    private TagsService tagsService;
    private UsersService usersService;

    @Autowired
    public UserController(BeersService service, StylesService stylesService, DtoMapper dtoMapper, BreweriesService breweriesService, TagsService tagsService, UsersService usersService) {
        this.service = service;
        this.stylesService = stylesService;
        this.dtoMapper = dtoMapper;
        this.breweriesService = breweriesService;
        this.tagsService = tagsService;
        this.usersService = usersService;
    }

    @GetMapping("/users")
    public String shwoUsers(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                            @RequestParam(name = "size", defaultValue = "6") int pageSize, Model model) {
        Page<UserDetails> userPage = usersService.UserDetailsPage(PageRequest.of(currentPage - 1, pageSize));
        model.addAttribute("userPage", userPage);
        int totalPages = userPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "users";
    }


    @GetMapping("/users/{id}")
    public String showBeerPage(Model model, Principal principal, @PathVariable int id, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getById(id);
            model.addAttribute("user", userDetails);
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (userDetails.getName().equals(principal.getName()) || isAdmin) {
                return "user";
            }
            return "access-denied";
        }
        return "access-denied";
    }


    @GetMapping("/users/myProfile/dranklist")
    public String showDranklist(Model model, Principal principal,SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            model.addAttribute("user", usersService.getDranklist(principal.getName()));
            return "dranklist";
        }
        return "access-denied";
    }

    @GetMapping("/users/myProfile/wishlist")
    public String showWishlist(Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            model.addAttribute("user", usersService.getWishlist(principal.getName()));
            return "wishlist";
        }
        return "access-denied";
    }

    @GetMapping("/users/myProfile/topthreebeers")
    public String showTopThreeBeers(Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
        model.addAttribute("top", usersService.topThreeBeers(principal.getName()));
        return "top";
    }   return "access-denied";
    }

    @GetMapping("/users/update/{id}")
    public String getProfile(@PathVariable int id, Model model, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getById(id);
            UserDto userDto = new UserDto();
            userDto.setFirstName(userDetails.getFirstName());
            userDto.setLastName(userDetails.getLastname());
            userDto.setEmail(userDetails.getEmail());
            userDto.setPicture(userDetails.getPicture().getUrl());
            model.addAttribute("username", userDetails);
            model.addAttribute("userDto", userDto);
            return "userUpdate";
        }
        return "access-denied";
    }

    @PostMapping("/users/update/{id}")
    public String editProfile(@PathVariable int id, @ModelAttribute("userDto") UserDto userDto, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getById(id);
            usersService.update(dtoMapper.updateUserFromDto(userDetails, userDto), userDetails.getId());
            return "redirect:/users/{id}";
        }
        return "access-denied";
    }


    @RequestMapping(method = RequestMethod.GET)
    public String showUserProfile(Model model, Principal principal) {
        return "user";
    }

}
