package com.telerikacademy.beers.exceptions;

public class RatingOutOfRangeException extends Exception {

    public RatingOutOfRangeException(String string) {
        super(string);
    }

}
