package com.telerikacademy.beers.exceptions;

public class ReferentialIntegrityViolationException extends RuntimeException {

    public ReferentialIntegrityViolationException(String string) {
        super(string);
    }
}
