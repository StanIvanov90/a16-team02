package com.telerikacademy.beers.exceptions;

public class NoAccessToRateBeerException extends Exception {

    public NoAccessToRateBeerException(String string) {
        super(string);
    }

}
