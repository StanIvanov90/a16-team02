package com.telerikacademy.workitemmanagement;

import com.telerikacademy.workitemmanagement.core.EngineImpl;

public class StartUp {

    public static void main(String[] args) {
        EngineImpl engine = new EngineImpl();
        engine.start();
    }
}
