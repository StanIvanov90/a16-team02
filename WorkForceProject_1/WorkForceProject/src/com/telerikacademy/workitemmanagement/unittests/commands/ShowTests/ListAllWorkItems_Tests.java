package com.telerikacademy.workitemmanagement.unittests.commands.ShowTests;

import com.telerikacademy.workitemmanagement.commands.ListAllWorkItems;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.StoryImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Story;
import com.telerikacademy.workitemmanagement.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ListAllWorkItems_Tests {

    private Command testCommand;
    private Bug testBug;
    private Story testStory;
    private Feedback testFeedBack;

    private WimRepository wimRepository;
    private WimFactory wimFactory;


    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ListAllWorkItems(wimFactory, wimRepository);
        testBug = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SeverityType.MINOR, BugStatusType.ACTIVE);
        testStory = new StoryImpl("StorySasfasa", "StoryDescacsasa", PriorityType.LOW, SizeType.SMALL, StoryStatusType.INPROGRESS);
        testFeedBack = new FeedBackImpl("Feedbackasagaga", "FeedbackStraete", FeedbackStatusType.UNSCHEDULED, 3);
        wimRepository.addWorkItem(testBug.getTitle(), testBug);
        wimRepository.addWorkItem(testFeedBack.getTitle(), testFeedBack);
        wimRepository.addWorkItem(testStory.getTitle(), testStory);
    }

    @Test
    public void execute_should_showBoardItems_when_inputIsValid() {
        // Arrange
        String expected = "";
        List<String> testList = new ArrayList<>();
        expected += testStory.getTitle()+ ", ";
        expected += testBug.getTitle()+ ", ";
        expected += testFeedBack.getTitle();
        String listAll = testCommand.execute(testList);


//Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expected, listAll);
    }
}
