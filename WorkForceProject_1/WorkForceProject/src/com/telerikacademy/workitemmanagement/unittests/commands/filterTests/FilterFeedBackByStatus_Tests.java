package com.telerikacademy.workitemmanagement.unittests.commands.filterTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.filter.FilterBugByStatus;
import com.telerikacademy.workitemmanagement.commands.filter.FilterFeedBackByStatus;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterFeedBackByStatus_Tests {

  private Feedback testFeedback;
  private Feedback testFeedback1;
  private Command testCommand;
  private WimRepository wimRepository;
  private WimFactory wimFactory;
  @Before
  public void before() {
    wimFactory = new WimFactoryImpl();
    wimRepository = new WimRepositoryImpl();
    testCommand = new FilterFeedBackByStatus(wimFactory, wimRepository);
    testFeedback = new FeedBackImpl("BugADSAAASDAGGA", "BugDescriptionasga", FeedbackStatusType.NEW, 12);
    testFeedback1 = new FeedBackImpl("BugADSAAASDAGGA", "BugDescriptionasga1", FeedbackStatusType.DONE, 8);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedLessArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    // Act & Assert
    testCommand.execute(testList);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedMoreArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    // Act & Assert
    testCommand.execute(testList);
  }
  @Test
  public void execute_should_FilterFeedbackByStatus_when_inputIsValid() {
    // Arrange
    String expected = testFeedback.getTitle();
    List<String> testList = new ArrayList<>();
    testList.add("New");
    wimRepository.addWorkItem(testFeedback.getTitle(),testFeedback);
    wimRepository.addWorkItem(testFeedback1.getTitle(),testFeedback1);
    //Act
    String filter = testCommand.execute(testList);
    //Assert
    Assert.assertEquals(expected,filter);
  }


}
