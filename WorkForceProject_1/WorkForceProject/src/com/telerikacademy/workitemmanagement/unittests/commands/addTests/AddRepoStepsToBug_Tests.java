package com.telerikacademy.workitemmanagement.unittests.commands.addTests;

import com.telerikacademy.workitemmanagement.commands.add.AddRepoStepsToBug;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BoardsImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddRepoStepsToBug_Tests {

    private Command testCommand;
    private Board testBoard;
    private Team testTeam;
    private Person testPerson;
    private Bug testBug;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new AddRepoStepsToBug(wimFactory, wimRepository);
        testBoard = new BoardsImpl("Board1");
        testTeam = new TeamsImpl("Team1");
        testPerson = new PersonImpl("Pesho");
        testBug = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SeverityType.MINOR, BugStatusType.ACTIVE);
        wimRepository.addPerson("Pesho",testPerson);
        wimRepository.addBoardToBoards("Board1", testBoard);
        wimRepository.addTeam("Team1",testTeam);
        wimRepository.addWorkItem("BugDescriptionasga",testBug);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");


        // Act & Assert
        testCommand.execute(testList);
    }
    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("BugADSAAASDAGGA");
        wimRepository.addWorkItem("BugADSAAASDAGGA", testBug);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_AddBoardToTeam_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add(testBug.getTitle());
        testList.add("RepoTest");
        testList.add(testPerson.getname());
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());
        testTeam.addMember(testPerson);
        testTeam.addBoard(testBoard);
        testBoard.addBug(testBug);
        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testBug.getStepsToReport().size() );

    }
}
