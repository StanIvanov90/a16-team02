package com.telerikacademy.workitemmanagement.unittests.commands.changeTests;

import com.telerikacademy.workitemmanagement.commands.change.ChangeFeedBackRating;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BoardsImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeFeedbackRating_Tests {

  private Command testCommand;
  private Board testBoard;
  private Team testTeam;
  private Person testPerson;
  private Feedback testFeedback;
  private WimRepository wimRepository;
  private WimFactory wimFactory;

  @Before
  public void before() {
    wimFactory = new WimFactoryImpl();
    wimRepository = new WimRepositoryImpl();
    testCommand = new ChangeFeedBackRating(wimFactory, wimRepository);
    testBoard = new BoardsImpl("Board1");
    testTeam = new TeamsImpl("Team1");
    testPerson = new PersonImpl("Pesho");
    testFeedback = new FeedBackImpl("BugADSAAASDAGGA", "BugDescriptionasga", FeedbackStatusType.NEW, 12);
    wimRepository.addWorkItem("BugDescriptionasga",testFeedback);
    wimRepository.addBoardToBoards("Board1",testBoard);
    wimRepository.addTeam("Team1",testTeam);
    wimRepository.addPerson("Pesho",testPerson);
    testTeam.addMember(testPerson);
    testTeam.addBoard(testBoard);
    testBoard.addFeedback(testFeedback);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedLessArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("asdasd");
    // Act & Assert
    testCommand.execute(testList);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedMoreArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");


    // Act & Assert
    testCommand.execute(testList);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_boardDoesNotExist() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("Team1");
    testList.add("Board1");
    wimRepository.addTeam("Team1", testTeam);

    // Act & Assert
    testCommand.execute(testList);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_teamDoesNotExist() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("Team1");
    testList.add("Board1");
    wimRepository.addBoardToBoards("Board1", testBoard);

    // Act & Assert
    testCommand.execute(testList);
  }

  @Test
  public void execute_should_ChangeFeedbackRating_when_inputIsValid() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("BugDescriptionasga");
    testList.add("19");
    testList.add("Pesho");
    testList.add("Board1");
    testList.add("Team1");


    // Act
    testCommand.execute(testList);

    // Assert
    Assert.assertEquals(testFeedback.getRating(), 19);

  }

}