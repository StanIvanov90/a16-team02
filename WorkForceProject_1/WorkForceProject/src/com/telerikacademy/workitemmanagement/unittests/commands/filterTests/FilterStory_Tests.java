package com.telerikacademy.workitemmanagement.unittests.commands.filterTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.filter.FilterStory;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.StoryImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Story;
import com.telerikacademy.workitemmanagement.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterStory_Tests {

    private Bug testBug;
    private Story testStory;
    private Feedback testFeedBack;
    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new FilterStory(wimFactory, wimRepository);
        testBug = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SeverityType.MINOR, BugStatusType.ACTIVE);
        testStory = new StoryImpl("StorySasfasa", "StoryDescacsasa", PriorityType.LOW, SizeType.SMALL, StoryStatusType.INPROGRESS);
        testFeedBack = new FeedBackImpl("Feedbackasagaga", "FeedbackStraete", FeedbackStatusType.NEW, 3);
        wimRepository.addWorkItem("BugADSAAASDAGGA", testBug);
        wimRepository.addWorkItem("StoryDescacsasa", testStory);
        wimRepository.addWorkItem("FeedbackStraete", testFeedBack);
    }

    @Test
    public void execute_should_FilterStory_when_inputIsValid() {
        // Arrange
        String expected = testStory.getTitle();
        List<String> testList = new ArrayList<>();
        String filter = testCommand.execute(testList);
        //Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expected, filter);
    }
}
