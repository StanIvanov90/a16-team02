package com.telerikacademy.workitemmanagement.unittests.commands.changeTests;

import com.telerikacademy.workitemmanagement.commands.change.ChangeFeedBackStatus;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BoardsImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeFeedBackStatus_Tests {


    private Command testCommand;
    private Board testBoard;
    private Team testTeam;
    private Person testPerson;
    private Feedback testFeedBack;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ChangeFeedBackStatus(wimFactory, wimRepository);
        testBoard = new BoardsImpl("Board1");
        testTeam = new TeamsImpl("Team1");
        testPerson = new PersonImpl("Pesho");
        testFeedBack = new FeedBackImpl("{{Feedbackasagaga}}", "FeedbackTitleasad", FeedbackStatusType.NEW, 3);
        wimRepository.addWorkItem("FeedbackTitleasad", testFeedBack);
        wimRepository.addBoardToBoards("Board1", testBoard);
        wimRepository.addTeam("Team1", testTeam);
        wimRepository.addPerson("Pesho", testPerson);
        testTeam.addMember(testPerson);
        testTeam.addBoard(testBoard);
        testBoard.addFeedback(testFeedBack);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");


        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Board1");
        wimRepository.addTeam("Team1", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Board1");
        wimRepository.addBoardToBoards("Board1", testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_ChangeFeedBackStatus_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FeedbackTitleasad");
        testList.add("Scheduled");
        testList.add("Pesho");
        testList.add("Board1");
        testList.add("Team1");




        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(testFeedBack.getStatus(), "Scheduled");

    }
}
