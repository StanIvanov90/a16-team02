package com.telerikacademy.workitemmanagement.unittests.commands.filterTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.filter.FilterBugsByStatusAndAssignee;
import com.telerikacademy.workitemmanagement.commands.filter.FilterFeedBackByStatus;
import com.telerikacademy.workitemmanagement.commands.filter.FilterFeedBackByStatusAndAssignee;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterFeedBackByStatusAndAssignee_Tests {

  private Person testPerson;
  private Person testPerson1;
  private Feedback testFeedback;
  private Feedback testFeedback1;
  private Command testCommand;
  private WimRepository wimRepository;
  private WimFactory wimFactory;
  @Before
  public void before() {
    wimFactory = new WimFactoryImpl();
    wimRepository = new WimRepositoryImpl();
    testCommand = new FilterFeedBackByStatusAndAssignee(wimFactory, wimRepository);
    testPerson = new PersonImpl("Gosho");
    testPerson1 = new PersonImpl("Pesho");
    testFeedback = new FeedBackImpl("BugADSAAASDAGGA", "BugDescriptionasga", FeedbackStatusType.DONE, 12);
    testFeedback1 = new FeedBackImpl("BugADSAAASDAGGA", "BugDescriptionasga1", FeedbackStatusType.NEW, 8);
    testPerson.assignWork(testFeedback);
    testPerson.assignWork(testFeedback1);

  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedLessArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("asdasd");
    // Act & Assert
    testCommand.execute(testList);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedMoreArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    // Act & Assert
    testCommand.execute(testList);
  }
  @Test
  public void execute_should_FilterFeedBackByStatusAndAssignee_when_inputIsValid() {
    // Arrange
    String expected = testFeedback.getTitle();
    List<String> testList = new ArrayList<>();
    testList.add(testPerson.getname());
    testList.add("Done");
    wimRepository.addPerson(testPerson.getname(),testPerson);
    wimRepository.addWorkItem(testFeedback.getTitle(),testFeedback);
    wimRepository.addWorkItem(testFeedback1.getTitle(),testFeedback1);
    //Act
    String filter = testCommand.execute(testList);
    //Assert
    Assert.assertEquals(expected,filter);
  }
}
