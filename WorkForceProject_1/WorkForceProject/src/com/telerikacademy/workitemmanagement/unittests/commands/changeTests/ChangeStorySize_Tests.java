package com.telerikacademy.workitemmanagement.unittests.commands.changeTests;

import com.telerikacademy.workitemmanagement.commands.change.ChangeStorySize;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BoardsImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.StoryImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Story;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SizeType;
import com.telerikacademy.workitemmanagement.models.enums.StoryStatusType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ChangeStorySize_Tests {

    private Command testCommand;
    private Board testBoard;
    private Team testTeam;
    private Person testPerson;
    private Story testStory;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ChangeStorySize(wimFactory, wimRepository);
        testBoard = new BoardsImpl("Board1");
        testTeam = new TeamsImpl("Team1");
        testPerson = new PersonImpl("Pesho");
        testStory = new StoryImpl("{{Feedbackasagaga}}", "FeedbackTitleasad", PriorityType.LOW, SizeType.LARGE, StoryStatusType.DONE);
        wimRepository.addWorkItem("FeedbackTitleasad", testStory);
        wimRepository.addBoardToBoards("Board1", testBoard);
        wimRepository.addTeam("Team1", testTeam);
        wimRepository.addPerson("Pesho", testPerson);
        testTeam.addMember(testPerson);
        testTeam.addBoard(testBoard);
        testBoard.addFeedback(testStory);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");


        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_boardDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Board1");
        wimRepository.addTeam("Team1", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Board1");
        wimRepository.addBoardToBoards("Board1", testBoard);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_ChangeStoryPriority_when_inputIsValid() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("FeedbackTitleasad");
        testList.add("Medium");
        testList.add("Pesho");
        testList.add("Board1");
        testList.add("Team1");




        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(testStory.getSize(), SizeType.MEDIUM);

    }
}
