package com.telerikacademy.workitemmanagement.unittests.commands.filterTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.filter.FilterBugByStatus;
import com.telerikacademy.workitemmanagement.commands.filter.FilterFeedBackByStatus;
import com.telerikacademy.workitemmanagement.commands.filter.FilterStoryByStatus;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.StoryImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Story;
import com.telerikacademy.workitemmanagement.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterStoryByStatus_Tests {

  private Story testStory;
  private Story testStory1;
  private Command testCommand;
  private WimRepository wimRepository;
  private WimFactory wimFactory;
  @Before
  public void before() {
    wimFactory = new WimFactoryImpl();
    wimRepository = new WimRepositoryImpl();
    testCommand = new FilterStoryByStatus(wimFactory, wimRepository);
    testStory = new StoryImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SizeType.LARGE, StoryStatusType.DONE);
    testStory1 = new StoryImpl("BugADSAAASDAGGA", "BugDescriptionasga1", PriorityType.HIGH, SizeType.SMALL, StoryStatusType.NOTDONE);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedLessArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    // Act & Assert
    testCommand.execute(testList);
  }

  @Test(expected = IllegalArgumentException.class)
  public void execute_should_throwException_when_passedMoreArguments() {
    // Arrange
    List<String> testList = new ArrayList<>();
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    testList.add("asdasd");
    // Act & Assert
    testCommand.execute(testList);
  }
  @Test
  public void execute_should_FilterStoryByStatus_when_inputIsValid() {
    // Arrange
    String expected = testStory.getTitle();
    List<String> testList = new ArrayList<>();
    testList.add("Done");
    wimRepository.addWorkItem(testStory.getTitle(),testStory);
    wimRepository.addWorkItem(testStory1.getTitle(),testStory1);
    //Act
    String filter = testCommand.execute(testList);
    //Assert
    Assert.assertEquals(expected,filter);
  }


}
