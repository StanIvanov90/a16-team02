package com.telerikacademy.workitemmanagement.unittests.commands.ShowTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.show.ShowBoardWorkItems;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.*;
import com.telerikacademy.workitemmanagement.models.contracts.*;
import com.telerikacademy.workitemmanagement.models.enums.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowBoardWorkItems_Tests {

    private static final String WORK_ITEM_HEADER = "----------------";

    private Command testCommand;
    private Board testBoard;
    private Team testTeam;
    private Bug testBug;
    private Story testStory;
    private Feedback testFeedBack;

    private WimRepository wimRepository;
    private WimFactory wimFactory;


    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new ShowBoardWorkItems(wimFactory, wimRepository);
        testBoard = new BoardsImpl("Board1");
        testTeam = new TeamsImpl("Team1");
        testBug = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SeverityType.MINOR, BugStatusType.ACTIVE);
        testStory = new StoryImpl("StorySasfasa", "StoryDescacsasa", PriorityType.LOW, SizeType.SMALL, StoryStatusType.INPROGRESS);
        testFeedBack = new FeedBackImpl("Feedbackasagaga", "FeedbackStraete", FeedbackStatusType.NEW, 3);
        wimRepository.addWorkItem("BugADSAAASDAGGA", testBug);
        wimRepository.addBoardToBoards("Board1", testBoard);
        wimRepository.addTeam("Team1", testTeam);
        testTeam.addBoard(testBoard);
        testBoard.addBug(testBug);
        testBoard.addStory(testStory);
        testBoard.addFeedback(testFeedBack);
        testBug.addWorkItemHistory("Test");
        testFeedBack.addWorkItemHistory("Test");
        testStory.addWorkItemHistory("Test");
        testBug.addComment("TestComment");
        testBug.addStepToRepo("TestStep");
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void execute_should_showBoardItems_when_inputIsValid() {
        // Arrange
        String expected = "";
        List<String> testList = new ArrayList<>();
        testList.add(testBoard.getName());
        testList.add(testTeam.getName());

        expected += (String.format("Board with name %s consists of the following items:", testBoard.getName()));
        expected += System.lineSeparator();
        for (WorkItems workItems : testBoard.listWorkItems()) {
            expected += workItems.toString();
            expected += System.lineSeparator();
            expected += WORK_ITEM_HEADER;
            expected += System.lineSeparator();
        }
        String expectedSubstring = expected.substring(0,expected.length()-1);

//        expected += (String.format("Board with name %s consists of the following items:", testBoard.getName()));
//        expected += (System.lineSeparator());
//        expected += ("Bug: with the following details:");
//        expected += (System.lineSeparator());
//        expected += (String.format("id: %d", testBug.getId()));
//        expected += (System.lineSeparator());
//        expected += (String.format("title: %s", testBug.getTitle()));
//        expected += (System.lineSeparator());
//        expected += (String.format("description: %s", testBug.Description()));
//        expected += (System.lineSeparator());
//        expected += (String.format("history: %s", testBug.getHistory()
//                .toString()).replace("[", "")
//                .replace("]", ""));
//        expected += (System.lineSeparator());
//        expected += (String.format("Bug Status: %s", testBug.getStatus()));
//        expected += (System.lineSeparator());
//        expected += (String.format("Severity Type: %s", testBug.getSeverity()));
//        expected += (System.lineSeparator());
//        expected += (String.format("Priority Type: %s",
//                testBug.getBugPriority()));
//        expected += (System.lineSeparator());
//        expected += (System.lineSeparator());
//        expected += ("Story: with the following details:");
//        expected += (System.lineSeparator());
//        expected += (String.format("id: %d", testStory.getId()));
//        expected += (System.lineSeparator());
//        expected += (String.format("title: %s", testStory.getTitle()));
//        expected += (System.lineSeparator());
//        expected += (String.format("description: %s", testStory.Description()));
//        expected += (System.lineSeparator());
//        expected += (String.format("history: %s", testStory.getHistory()
//                .toString()).replace("[", "")
//                .replace("]", ""));
//        expected += (System.lineSeparator());
//        expected += (String.format("Story Size: %s", testStory.getSize()));
//        expected += (System.lineSeparator());
//        expected += (String.format("Priority Type: %s",
//                testStory.getStoryPriority()));
//        expected += (System.lineSeparator());
//        expected += (String.format("Story Status Type: %s", testStory.getStatus()));
//        expected += (System.lineSeparator());
//        expected += (System.lineSeparator());
//        expected += ("FeedBack: with the following details:");
//        expected += (System.lineSeparator());
//        expected += (String.format("id: %d", testFeedBack.getId()));
//        expected += (System.lineSeparator());
//        expected += (String.format("title: %s", testFeedBack.getTitle()));
//        expected += (System.lineSeparator());
//        expected += (String.format("description: %s", testFeedBack.Description()));
//        expected += (System.lineSeparator());
//        expected += (String.format("history: %s", testFeedBack.getHistory()
//                .toString()).replace("[", "")
//                .replace("]", ""));
//        expected += (System.lineSeparator());
//        expected += (String.format("Bug Status: %s", testFeedBack.getStatus()));
//        expected += (System.lineSeparator());
//        expected += (String.format("Rating %d", testFeedBack.getRating()));

        // Act
        testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expectedSubstring, testBoard.workItemsDetails());
    }
}
