package com.telerikacademy.workitemmanagement.unittests.commands.createTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BoardsImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class CreateBoard {

    private Command testCommand;
    private Person testPerson;
    private Team testTeam;
    private Board testBoard;
    private WimRepository wimRepository;
    private WimFactory wimFactory;


    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new com.telerikacademy.workitemmanagement.commands.create.CreateBoard(wimFactory,wimRepository);
        testPerson = new PersonImpl("Pesho");
        testTeam = new TeamsImpl("Team1");
        testBoard = new BoardsImpl("Board1");
        wimRepository.addPerson("Pesho", testPerson);
        testTeam.addMember(testPerson);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");


        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void execute_should_CreateBoard_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Board1");
        testList.add("Pesho");
        testList.add("Team1");

        wimRepository.addTeam(testTeam.getName(),testTeam);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testTeam.showBoard().size());

    }
}
