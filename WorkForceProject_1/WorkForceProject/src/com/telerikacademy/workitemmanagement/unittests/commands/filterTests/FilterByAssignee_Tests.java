package com.telerikacademy.workitemmanagement.unittests.commands.filterTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.filter.FilterByAssignee;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterByAssignee_Tests {
    private Person testPerson;
    private Person testPerson1;
    private Bug testBug;
    private Bug testBug1;
    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new FilterByAssignee(wimFactory, wimRepository);
        testPerson = new PersonImpl("Gosho");
        testPerson1 = new PersonImpl("Pesho");
        testBug = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SeverityType.MINOR, BugStatusType.ACTIVE);
        testBug1 = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga1", PriorityType.LOW, SeverityType.MINOR, BugStatusType.FIXED);
        testPerson.assignWork(testBug);
        testPerson1.assignWork(testBug1);

    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void execute_should_FilterBugByStatusAndAssignee_when_inputIsValid() {
        // Arrange
        String expected = testBug.toString();
        List<String> testList = new ArrayList<>();
        testList.add(testPerson.getname());
        wimRepository.addPerson(testPerson.getname(),testPerson);
        wimRepository.addWorkItem(testBug.getTitle(),testBug);
        wimRepository.addWorkItem(testBug1.getTitle(),testBug1);
        //Act
        String filter = testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expected,filter);
    }
}
