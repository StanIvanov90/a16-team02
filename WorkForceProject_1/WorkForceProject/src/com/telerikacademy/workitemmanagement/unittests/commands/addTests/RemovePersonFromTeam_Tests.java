package com.telerikacademy.workitemmanagement.unittests.commands.addTests;

import com.telerikacademy.workitemmanagement.commands.RemovePersonFromTeam;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class RemovePersonFromTeam_Tests {

    private Command testCommand;
    private Team testTeam;
    private Person testPerson;
    private Person testPerson1;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new RemovePersonFromTeam(wimFactory, wimRepository);
        testTeam = new TeamsImpl("Team1");
        testPerson = new PersonImpl("Pesho");
        testPerson1 = new PersonImpl("Gosho");
        testTeam.addMember(testPerson);
        testTeam.addMember(testPerson1);
        wimRepository.addPerson("Pesho", testPerson);
        wimRepository.addPerson("Gosho", testPerson1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");


        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Pesho");
        wimRepository.addPerson("Pesho", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_personDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Pesho");
        wimRepository.addTeam("Pesho", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_AddBoardToTeam_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Gosho");
        testList.add("Team1");
        testList.add("Pesho");
        wimRepository.addTeam("Team1", testTeam);
        wimRepository.addPerson("Gosho", testPerson);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(1, testTeam.showTeamMembers().size());

    }
}
