package com.telerikacademy.workitemmanagement.unittests.commands.filterTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.filter.FilterBugByStatus;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BugImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class FilterBugByStatus_Tests {

    private Bug testBug;
    private Bug testBug1;
    private Command testCommand;
    private WimRepository wimRepository;
    private WimFactory wimFactory;
    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new FilterBugByStatus(wimFactory, wimRepository);
        testBug = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga", PriorityType.LOW, SeverityType.MINOR, BugStatusType.ACTIVE);
        testBug1 = new BugImpl("BugADSAAASDAGGA", "BugDescriptionasga1", PriorityType.LOW, SeverityType.MINOR, BugStatusType.FIXED);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }
    @Test
    public void execute_should_FilterBugByStatus_when_inputIsValid() {
        // Arrange
        String expected = testBug.getTitle();
        List<String> testList = new ArrayList<>();
        testList.add("Active");
        wimRepository.addWorkItem(testBug.getTitle(),testBug);
        wimRepository.addWorkItem(testBug1.getTitle(),testBug1);
        //Act
        String filter = testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expected,filter);
    }


    }
