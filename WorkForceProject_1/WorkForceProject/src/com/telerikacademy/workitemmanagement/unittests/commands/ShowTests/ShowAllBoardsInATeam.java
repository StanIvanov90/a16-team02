package com.telerikacademy.workitemmanagement.unittests.commands.ShowTests;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.BoardsImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class ShowAllBoardsInATeam {

    private Command testCommand;
    private Board testBoard;
    private Board testBoard1;
    private Team testTeam;
    private WimRepository wimRepository;
    private WimFactory wimFactory;


    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new com.telerikacademy.workitemmanagement.commands.show.ShowAllBoardsInATeam(wimFactory, wimRepository);
        testBoard = new BoardsImpl("Board");
        testBoard1 = new BoardsImpl("Board1");
        testTeam = new TeamsImpl("Team1");
        wimRepository.addTeam(testTeam.getName(),testTeam);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_showBoardItems_when_inputIsValid() {
        // Arrange
        String expected = "";
        List<String> testList = new ArrayList<>();
        expected += testBoard.getName() + ", ";
        expected += testBoard1.getName();

        testList.add(testTeam.getName());
        testTeam.addBoard(testBoard);
        testTeam.addBoard(testBoard1);



        //Act
       String showBoards = testCommand.execute(testList);
        //Assert
        Assert.assertEquals(expected,showBoards);
    }
}
