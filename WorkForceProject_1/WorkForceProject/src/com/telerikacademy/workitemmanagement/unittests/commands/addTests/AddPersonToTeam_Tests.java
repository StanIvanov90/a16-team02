package com.telerikacademy.workitemmanagement.unittests.commands.addTests;

import com.telerikacademy.workitemmanagement.commands.add.AddPersonToTeam;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.WimRepositoryImpl;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class AddPersonToTeam_Tests {

    private Command testCommand;
    private Team testTeam;
    private Person testPerson;
    private WimRepository wimRepository;
    private WimFactory wimFactory;

    @Before
    public void before() {
        wimFactory = new WimFactoryImpl();
        wimRepository = new WimRepositoryImpl();
        testCommand = new AddPersonToTeam(wimFactory, wimRepository);
        testTeam = new TeamsImpl("Team1");
        testPerson = new PersonImpl("Pesho");
        testTeam.addMember(testPerson);
        wimRepository.addPerson("Pesho", testPerson);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedLessArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_passedMoreArguments() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");
        testList.add("asdasd");


        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_teamDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Pesho");
        wimRepository.addPerson("Pesho", testPerson);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test(expected = IllegalArgumentException.class)
    public void execute_should_throwException_when_personDoesNotExist() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Team1");
        testList.add("Pesho");
        wimRepository.addTeam("Pesho", testTeam);

        // Act & Assert
        testCommand.execute(testList);
    }

    @Test
    public void execute_should_AddBoardToTeam_whenInputCorrect() {
        // Arrange
        List<String> testList = new ArrayList<>();
        testList.add("Gosho");
        testList.add("Team1");
        testList.add("Pesho");
        wimRepository.addTeam("Team1", testTeam);
        wimRepository.addPerson("Gosho", testPerson);

        // Act
        testCommand.execute(testList);

        // Assert
        Assert.assertEquals(2, testTeam.showTeamMembers().size());

    }
}
