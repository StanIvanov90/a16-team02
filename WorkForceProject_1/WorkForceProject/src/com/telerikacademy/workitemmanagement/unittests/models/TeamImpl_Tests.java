package com.telerikacademy.workitemmanagement.unittests.models;


import com.telerikacademy.workitemmanagement.models.TeamsImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import org.junit.Test;

public class TeamImpl_Tests {

    @Test(expected = NullPointerException.class)
    public void constructor_should_throwError_when_nameIsNull() {
        // Arrange, Act, Assert
        Team team = new TeamsImpl(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsLessThanValid() {
        // Arrange, Act, Assert
        Team team = new TeamsImpl("Teaam");
        Team team1 = new TeamsImpl("Te");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsMoreThanValid() {
        // Arrange, Act, Assert
        Team team = new TeamsImpl("Teamteamteamteamteamteamddddddddddddddddddddddddddddddddddddd");
        Team team1 = new TeamsImpl("Teamteamteamteamteamteamteamdddddddd");
    }
}
