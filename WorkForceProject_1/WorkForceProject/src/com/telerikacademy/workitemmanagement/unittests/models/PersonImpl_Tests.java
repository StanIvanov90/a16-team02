package com.telerikacademy.workitemmanagement.unittests.models;

import com.telerikacademy.workitemmanagement.models.PersonImpl;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import org.junit.Test;

public class PersonImpl_Tests {

    @Test(expected = NullPointerException.class)
    public void constructor_should_throwError_when_nameIsNull() {
        // Arrange, Act, Assert
        Person person1 = new PersonImpl(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsLessThanValid() {
        // Arrange, Act, Assert
        Person person = new PersonImpl("Pesho");
        Person perso1 = new PersonImpl("Pesh");
    }

    @Test(expected = IllegalArgumentException.class)
    public void constructor_should_throwError_when_nameIsMoreThanValid() {
        // Arrange, Act, Assert
        Person person = new PersonImpl("Pesho");
        Person perso1 = new PersonImpl("PeshoPeshoPeshoPeshoPesho");
    }
}
