package com.telerikacademy.workitemmanagement.unittests.models;

import com.telerikacademy.workitemmanagement.models.FeedBackImpl;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;
import org.junit.Test;

public class FeedBackImpl_Tests {

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenTitleLengthIsBelow10() {
        // Act
        FeedBackImpl feedBack = new FeedBackImpl("DescriptionTest12", "name", FeedbackStatusType.NEW, 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenTitleLengthIsAbove15() {
        // Act
        FeedBackImpl feedBack = new FeedBackImpl("DescriptionTest12", "namenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamenamename", FeedbackStatusType.NEW, 4);
    }

    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenRatingIsNegative() {
        // Act
        FeedBackImpl feedBack = new FeedBackImpl("DescriptionTest12", "name", FeedbackStatusType.NEW, -4);
    }
    @Test(expected = IllegalArgumentException.class)
    public void Constructor_ShouldThrow_WhenDescriptionLengthIsBelow10() {
        // Act
        FeedBackImpl feedBack = new FeedBackImpl("Descri", "name", FeedbackStatusType.NEW, 4);
    }
}
