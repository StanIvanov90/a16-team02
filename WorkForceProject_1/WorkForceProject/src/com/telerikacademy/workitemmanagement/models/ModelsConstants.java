package com.telerikacademy.workitemmanagement.models;

class ModelsConstants {
  final static int MEMBER_MIN_TITLE_LENGTH = 5;
  final static int MEMBER_MAX_TITLE_LENGTH = 15;
  final static int TEAM_MIN_NAME_LENGTH = 5;
  final static int TEAM_MAX_NAME_LENGTH = 15;
  final static int BOARD_NAME_MIN_LENGTH = 5;
  final static int BOARD_NAME_MAX_LENGTH = 10;
  final static int MIN_TITLE_NAME = 10;
  final static int MAX_TITLE_NAME = 50;
  final static int MIN_DESCRIPTION_LENGTH = 10;
  final static int MAX_DESCRIPTION_LENGTH = 500;
}
