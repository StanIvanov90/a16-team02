package com.telerikacademy.workitemmanagement.models.enums;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.WRONG_FEEDBACK_STATUS_TYPE;

public enum FeedbackStatusType {
  NEW,
  DONE,
  SCHEDULED,
  UNSCHEDULED;

  @Override
  public String toString() {
    switch (this) {
      case NEW:
        return "New";
      case DONE:
        return "Done";
      case SCHEDULED:
        return "Scheduled";
      case UNSCHEDULED:
        return "Unscheduled";
      default:
        throw new IllegalArgumentException(WRONG_FEEDBACK_STATUS_TYPE);
    }
  }
}
