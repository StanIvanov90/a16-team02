package com.telerikacademy.workitemmanagement.models.enums;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.WRONG_STORY_STATUS_TYPE;

public enum StoryStatusType {
    NOTDONE,
    INPROGRESS,
    DONE;

    @Override
    public String toString() {
        switch (this) {
            case DONE:
                return "Done";
            case NOTDONE:
                return "NotDone";
            case INPROGRESS:
                return "InProgress";
            default:
                throw new IllegalArgumentException(WRONG_STORY_STATUS_TYPE);
        }
    }
}
