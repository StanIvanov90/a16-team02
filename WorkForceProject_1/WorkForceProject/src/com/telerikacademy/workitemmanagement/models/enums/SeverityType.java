package com.telerikacademy.workitemmanagement.models.enums;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.WRONG_SEVERITY_TYPE;

public enum SeverityType {

    CRITICAL,
    MAJOR,
    MINOR;

    @Override
    public String toString(){
        switch (this) {
            case CRITICAL:
                return "Critical";
            case MAJOR:
                return "Major";
            case MINOR:
                return "Minor";
            default:
                throw new IllegalArgumentException(WRONG_SEVERITY_TYPE);
        }
    }
}
