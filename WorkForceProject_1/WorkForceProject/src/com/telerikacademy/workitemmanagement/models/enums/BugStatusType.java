package com.telerikacademy.workitemmanagement.models.enums;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.WRONG_BUG_STATUS_TYPE;

public enum BugStatusType {

    ACTIVE,
    FIXED;

    @Override
    public String toString(){
        switch (this) {
            case ACTIVE:
                return "Active";
            case FIXED:
                return "Fixed";
            default:
                throw new IllegalArgumentException(WRONG_BUG_STATUS_TYPE);
        }
    }
}
