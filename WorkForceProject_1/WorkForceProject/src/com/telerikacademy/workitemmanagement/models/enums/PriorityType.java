package com.telerikacademy.workitemmanagement.models.enums;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.WRONG_PRIORITY_TYPE;

public enum PriorityType {
    LOW,
    MEDIUM,
    HIGH;

    @Override
    public String toString() {
        switch (this) {
            case MEDIUM:
                return "Medium";
            case LOW:
                return "Low";
            case HIGH:
                return "High";
            default:
                throw new IllegalArgumentException(WRONG_PRIORITY_TYPE);
        }
    }


}
