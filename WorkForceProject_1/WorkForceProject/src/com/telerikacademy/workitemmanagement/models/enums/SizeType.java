package com.telerikacademy.workitemmanagement.models.enums;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.WRONG_SIZE_TYPE;

public enum SizeType {

    LARGE,
    MEDIUM,
    SMALL;

    @Override
    public String toString(){
        switch (this) {
            case LARGE:
                return "Large";
            case MEDIUM:
                return "Medium";
            case SMALL:
                return "Small";
            default:
                throw new IllegalArgumentException(WRONG_SIZE_TYPE);
        }
    }
}
