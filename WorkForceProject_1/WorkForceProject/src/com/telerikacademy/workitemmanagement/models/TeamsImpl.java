package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.models.ModelsConstants.TEAM_MAX_NAME_LENGTH;
import static com.telerikacademy.workitemmanagement.models.ModelsConstants.TEAM_MIN_NAME_LENGTH;

public class TeamsImpl implements Team {

  private String name;
  private List<Person> members;
  private List<Board> boardList;
  private List<String> teamActivityList;

  public TeamsImpl(final String name) {
    setName(name);
    members = new ArrayList<>();
    boardList = new ArrayList<>();
    teamActivityList = new ArrayList<>();
  }

  @Override
  public void addMember(final Person person) {
    ValidationHelper.validateNull(person);
    members.add(person);
  }

  @Override
  public void removeMember(final Person person) {
    ValidationHelper.validateNull(person);
    members.remove(person);
  }

  @Override
  public List<Person> showTeamMembers() {
    return new ArrayList<>(members);
  }

  @Override
  public List<String> teamActivity() {
    return new ArrayList<>(teamActivityList);
  }

  @Override
  public void addBoard(final Board board) {
    ValidationHelper.validateNull(board);
    for (Board b : boardList) {
      if (b.getName().equals(board.getName())) {
        throw new IllegalArgumentException("TUK");
      }
    }
    boardList.add(board);
  }

  @Override
  public void removeBoard(final Board board) {
    ValidationHelper.validateNull(board);
    boardList.remove(board);
  }

  @Override
  public List<Board> showBoard() {
    return new ArrayList<>(boardList);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public void addActivityHistoryToTeam(final String activity) {
    teamActivityList.add(activity);
  }

  private void setName(final String name) {
    ValidationHelper.validateNull(name);
    ValidationHelper.validateStringLength(name.length(),
        TEAM_MIN_NAME_LENGTH,
        TEAM_MAX_NAME_LENGTH);
    this.name = name;
  }
}
