package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;

public class FeedBackImpl extends WorkItemsImpl implements Feedback {
  private FeedbackStatusType feedbackStatusType;
  private int rating;

  public FeedBackImpl(String description,
                      String title,
                      FeedbackStatusType feedbackStatusType,
                      int rating) {
    super(description, title);
    setFeedback(feedbackStatusType);
    setIntRating(rating);
  }

  public FeedbackStatusType getFeedbackStatusType() {
    return feedbackStatusType;
  }

  @Override
  public int getRating() {
    return rating;
  }

  @Override
  public void setFeedback(FeedbackStatusType feedbackStatusType) {
    this.feedbackStatusType = feedbackStatusType;
  }

  @Override
  public void setIntRating(int rating) {
    ValidationHelper.validateRating(rating);
    this.rating = rating;
  }

  @Override
  public String getStatus() {
    return feedbackStatusType.toString();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.toString()).append(String.format("FeedBack Status: %s", getStatus()))
      .append(System.lineSeparator())
      .append(String.format("Rating: %d", getRating()));
    return sb.toString();
  }
}
