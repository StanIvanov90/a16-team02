package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.BugAndStory;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;

public abstract class BugAndStoryImpl extends WorkItemsImpl implements BugAndStory {
  private PriorityType priorityType;
  private Person assignee;

  public BugAndStoryImpl(String title, String description, PriorityType priorityType) {
    super(title, description);
    setAssignee(assignee);
    this.priorityType = priorityType;
  }

  @Override
  public PriorityType getPriority() {
    return priorityType;
  }

  @Override
  public Person getAssignment() {
    return assignee;
  }

  @Override
  abstract public String getStatus();

  private void setAssignee(Person assignee) {
    this.assignee = assignee;
  }

}
