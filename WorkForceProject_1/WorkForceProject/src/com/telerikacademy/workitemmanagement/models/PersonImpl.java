package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.models.ModelsConstants.MEMBER_MAX_TITLE_LENGTH;
import static com.telerikacademy.workitemmanagement.models.ModelsConstants.MEMBER_MIN_TITLE_LENGTH;

public class PersonImpl implements Person {
  private String name;
  private List<WorkItems> workItems;
  private List<String> activityHistory;

  public PersonImpl(String name) {
    setName(name);
    workItems = new ArrayList<>();
    activityHistory = new ArrayList<>();
  }

  @Override
  public String getname() {
    return name;
  }

  @Override
  public void assignWork(WorkItems workItems) {
    this.workItems.add(workItems);
  }

  @Override
  public void unassignWork(WorkItems workItems) {
    this.workItems.remove(workItems);
  }

  @Override
  public void addActivityHistory(String activity) {
    activityHistory.add(activity);
  }

  @Override
  public List<String> showActivity() {
    return new ArrayList<>(activityHistory);
  }

  @Override
  public List<WorkItems> listWorkItems() {
    return new ArrayList<>(workItems);
  }

  @Override
  public String toString() {
    return this.name;
  }

  private void setName(String name) {
    ValidationHelper.validateNull(name);
    ValidationHelper.validateStringLength(name, MEMBER_MIN_TITLE_LENGTH, MEMBER_MAX_TITLE_LENGTH);
    this.name = name;
  }
}
