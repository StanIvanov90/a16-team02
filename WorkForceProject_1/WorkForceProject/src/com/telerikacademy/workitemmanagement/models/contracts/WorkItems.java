package com.telerikacademy.workitemmanagement.models.contracts;

import java.util.List;

public interface WorkItems {

    String getTitle();

    String Description();

    void addComment(String comment);

    void addWorkItemHistory(String itemHistory);

    String getStatus();

    List<String> getComments();

    List<String> getHistory();

    int getId();
}
