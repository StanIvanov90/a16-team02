package com.telerikacademy.workitemmanagement.models.contracts;

import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;

import java.util.List;

public interface Bug extends BugAndStory {

  SeverityType getSeverity();

  BugStatusType getBugStatusType();

  PriorityType getBugPriority();

  List<String> getStepsToReport();

  void setBugStatusType(BugStatusType bugStatusType);

  void setSeverity(SeverityType severityType);

  void setBugPriority(PriorityType bugPriority);

  void addStepToRepo(String step);
}
