package com.telerikacademy.workitemmanagement.models.contracts;

import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;

public interface Feedback extends WorkItems {

    int getRating();

    void setFeedback(FeedbackStatusType feedbackStatusType);

    void setIntRating(int rating);
}
