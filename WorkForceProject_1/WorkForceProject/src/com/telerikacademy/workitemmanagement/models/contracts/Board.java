package com.telerikacademy.workitemmanagement.models.contracts;

import java.util.List;

public interface Board {
  String getName();

  List<String> showActivity();

  List<WorkItems> listWorkItems();

  void addFeedback(WorkItems feedback);

  void addStory(WorkItems story);

  void addBug(WorkItems bug);

  void addActivityHistoryToBoard(String activity);

  String workItemsDetails();
}
