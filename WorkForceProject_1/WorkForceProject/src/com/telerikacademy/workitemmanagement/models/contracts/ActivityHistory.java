package com.telerikacademy.workitemmanagement.models.contracts;

public interface ActivityHistory {

  void getMemberName();

  void getWorkItem();

}
