package com.telerikacademy.workitemmanagement.models.contracts;

import java.util.List;

public interface Team {

    String getName();

    List<Board> showBoard();

    List<String> teamActivity();

    List<Person> showTeamMembers();

    void addMember(Person person);

    void removeMember(Person person);

    void addBoard(Board board);

    void removeBoard(Board board);

    void addActivityHistoryToTeam(String activity);

}
