package com.telerikacademy.workitemmanagement.models.contracts;

import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SizeType;
import com.telerikacademy.workitemmanagement.models.enums.StoryStatusType;

public interface Story extends BugAndStory {

    SizeType getSize();

    PriorityType getStoryPriority();

    void setStorySize(SizeType sizeType);

    void setStatusType(StoryStatusType storyStatusType);

    void setPriorityType(PriorityType priorityType);

    void getAssignee(Person person);
}
