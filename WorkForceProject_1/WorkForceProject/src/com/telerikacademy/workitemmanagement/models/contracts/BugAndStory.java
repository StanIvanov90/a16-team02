package com.telerikacademy.workitemmanagement.models.contracts;

import com.telerikacademy.workitemmanagement.models.enums.PriorityType;

public interface BugAndStory extends WorkItems {
  PriorityType getPriority();

  Person getAssignment();
}
