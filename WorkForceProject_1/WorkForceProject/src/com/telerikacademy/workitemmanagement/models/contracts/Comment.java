package com.telerikacademy.workitemmanagement.models.contracts;

public interface Comment {
  String getContent();
  String getAuthor();
}
