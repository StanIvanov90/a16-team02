package com.telerikacademy.workitemmanagement.models.contracts;

import java.util.List;

public interface Person {

    String getname();

    List<String> showActivity();

    List<WorkItems> listWorkItems();

    void assignWork(WorkItems workItems);

    void unassignWork(WorkItems workItems);

    void addActivityHistory(String activity);
}
