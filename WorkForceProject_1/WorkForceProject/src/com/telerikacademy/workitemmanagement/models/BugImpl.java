package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;

import java.util.ArrayList;
import java.util.List;

public class BugImpl extends BugAndStoryImpl implements Bug {
  private SeverityType severityType;
  private List<String> stepsToRepro;
  private BugStatusType bugStatusType;
  private PriorityType priorityType;

  public BugImpl(String description,
                 String title,
                 PriorityType priorityType,
                 SeverityType severityType,
                 BugStatusType bugStatusType) {
    super(description, title, priorityType);
    setBugStatusType(bugStatusType);
    setSeverity(severityType);
    setBugPriority(priorityType);
    stepsToRepro = new ArrayList<>();
  }

  public void add(String stepsToReproNew) {
    if (stepsToReproNew == null) {
      throw new IllegalArgumentException();
    }
    stepsToRepro.add(stepsToReproNew);
  }

  @Override
  public List<String> getStepsToReport() {
    return new ArrayList<>(stepsToRepro);
  }

  @Override
  public SeverityType getSeverity() {
    return severityType;
  }

  @Override
  public BugStatusType getBugStatusType() {
    return bugStatusType;
  }

  @Override
  public PriorityType getBugPriority() {
    return priorityType;
  }

  @Override
  public void setBugStatusType(BugStatusType bugStatusType) {
    this.bugStatusType = bugStatusType;
  }

  @Override
  public void setSeverity(SeverityType severityType) {
    this.severityType = severityType;
  }

  @Override
  public void setBugPriority(PriorityType bugPriority) {
    this.priorityType = bugPriority;
  }

  @Override
  public void addStepToRepo(String step) {
    stepsToRepro.add(step);
  }

  @Override
  public String getStatus() {
    return bugStatusType.toString();
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.toString())
            .append(String.format("Bug Status: %s", getStatus()))
        .append(System.lineSeparator())
        .append(String.format("Severity Type: %s", getSeverity())).append(System.lineSeparator())
        .append(String.format("Priority Type: %s",
        super.getPriority()));
    if (!stepsToRepro.isEmpty()) {
      sb.append(String.format("Steps to repo: %s", getStepsToReport().toString())
              .replace("[", "")
              .replace("]", ""));
    }
    return sb.toString();
  }
}
