package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.*;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.models.ModelsConstants.BOARD_NAME_MAX_LENGTH;
import static com.telerikacademy.workitemmanagement.models.ModelsConstants.BOARD_NAME_MIN_LENGTH;

public class BoardsImpl implements Board {
  private static final String NO_WORK_ITEMS_ON_THE_BOARD = "No work items on the board";
  private static final String SHOW_WORK_ITEMS_HEADER = "Board with name %s consists of the following items:";
  private static final String WORK_ITEM_HEADER = "----------------";
  private String name;
  private List<String> showActivity;
  private List<WorkItems> listWorkItems;

  public BoardsImpl(String name) {
    setName(name);
    showActivity = new ArrayList<>();
    listWorkItems = new ArrayList<>();
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public List<String> showActivity() {
    return new ArrayList<>(showActivity);
  }

  @Override
  public List<WorkItems> listWorkItems() {
    return new ArrayList<>(listWorkItems);
  }

  @Override
  public void addFeedback(WorkItems feedback) {
    listWorkItems.add(feedback);
  }

  @Override
  public void addStory(WorkItems story) {
    listWorkItems.add(story);
  }

  @Override
  public void addBug(WorkItems bug) {
    listWorkItems.add(bug);
  }

  @Override
  public void addActivityHistoryToBoard(String activity) {
    showActivity.add(activity);
  }

  @Override
  public String workItemsDetails() {
    StringBuilder strBuilder = new StringBuilder();
    strBuilder.append(String.format(SHOW_WORK_ITEMS_HEADER, getName()))
      .append(System.lineSeparator());
    if (listWorkItems().size() == 0) {
      strBuilder.append(NO_WORK_ITEMS_ON_THE_BOARD);
    }
    for (WorkItems workItems : listWorkItems) {
      strBuilder.append(workItems);
      strBuilder.append(System.lineSeparator());
      strBuilder.append(WORK_ITEM_HEADER);
      strBuilder.append(System.lineSeparator());
    }
    return strBuilder.toString().trim();
  }

  private void setName(String name) {
    ValidationHelper.validateStringLength(name.length(),
        BOARD_NAME_MIN_LENGTH,
        BOARD_NAME_MAX_LENGTH);
    this.name = name;
  }
}
