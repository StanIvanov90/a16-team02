package com.telerikacademy.workitemmanagement.models;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.OBJECT_CANNOT_BE_NULL;
import static com.telerikacademy.workitemmanagement.commands.CommandConstants.RATING_CANNOT_BE_NEGATIVE;

class ValidationHelper {

    static void validateStringLength(final String value, final int minValue, final int maxValue) {
        if (value == null || (value.length() < minValue || value.length() > maxValue)) {
            throw new IllegalArgumentException();
        }
    }

    static void validateStringLength(final int value, final int minValue, final int maxValue) {
        if (value < minValue || value > maxValue) {
            throw new IllegalArgumentException();
        }
    }

    static void validateRating(final int value) {
        if (value < 0) {
            throw new IllegalArgumentException(RATING_CANNOT_BE_NEGATIVE);
        }
    }

    static void validateNull(final Object value) {
        if (value == null) {
            throw new NullPointerException(OBJECT_CANNOT_BE_NULL);
        }
    }
}
