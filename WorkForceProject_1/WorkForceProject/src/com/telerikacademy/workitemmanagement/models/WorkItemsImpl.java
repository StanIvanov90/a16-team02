package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.models.ModelsConstants.*;

public abstract class WorkItemsImpl implements WorkItems {
  private static int idCounter = 1;

  private int id;
  private String title;
  private String description;
  private List<String> comments;
  private List<String> history;

  public WorkItemsImpl(final String description, final String title) {
    setDescription(description);
    setTitle(title);
    comments = new ArrayList<>();
    history = new ArrayList<>();
    id = idCounter++;
  }

  @Override
  public String getTitle() {
    return title;
  }

  @Override
  public String Description() {
    return description;
  }

  @Override
  public List<String> getComments() {
    return new ArrayList<>(comments);
  }

  @Override
  public List<String> getHistory() {
    return new ArrayList<>(history);
  }

  @Override
  public void addComment(final String comment) {
    comments.add(comment);
  }

  public void addWorkItemHistory(final String itemHistory) {
    history.add(itemHistory);
  }

  public int getId() {

    return id;
  }

  public abstract String getStatus();

  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    builder.append(String.format("%s:", this.getClass()
      .getSimpleName().replace("Impl", "")))
      .append(" with the following details:")
      .append(System.lineSeparator());
    builder.append(String.format("id: %d", getId())).append(System.lineSeparator());
    builder.append(String.format("title: %s", getTitle())).append(System.lineSeparator());
    builder.append(String.format("description: %s", description))
      .append(System.lineSeparator());
    builder.append(String.format("history: %s", getHistory()
      .toString()).replace("[", "")
      .replace("]", ""))
      .append(System.lineSeparator());
    if (!comments.isEmpty()) {
      builder.append(String.format("Comments: %s", getComments().toString())
        .replace("[", "")
        .replace("]", ""))
        .append(System.lineSeparator());
    }

    return builder.toString();
  }

  private void setTitle(final String title) {
    ValidationHelper.validateStringLength(title.length(), MIN_TITLE_NAME, MAX_TITLE_NAME);
    this.title = title;
  }

  private void setDescription(final String description) {
    ValidationHelper.validateStringLength(description.length(),
        MIN_DESCRIPTION_LENGTH, MAX_DESCRIPTION_LENGTH);
    this.description = description;
  }
}
