package com.telerikacademy.workitemmanagement.models;

import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Story;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SizeType;
import com.telerikacademy.workitemmanagement.models.enums.StoryStatusType;

public class StoryImpl extends BugAndStoryImpl implements Story {
  private SizeType sizeType;
  private StoryStatusType storyStatusType;
  private PriorityType priorityType;
  private Person person;

  public StoryImpl(final String description,
                   final String title,
                   PriorityType priorityType,
                   SizeType sizeType,
                   StoryStatusType storyStatusType) {
    super(description, title, priorityType);
    setStorySize(sizeType);
    setStatusType(storyStatusType);
    setPriorityType(priorityType);

  }

  @Override
  public SizeType getSize() {
    return sizeType;
  }

  @Override
  public void setStorySize(SizeType sizeType) {
    this.sizeType = sizeType;
  }

  @Override
  public PriorityType getStoryPriority() {
    return priorityType;
  }

  @Override
  public void setStatusType(StoryStatusType storyStatusType) {
    this.storyStatusType = storyStatusType;
  }

  @Override
  public void setPriorityType(PriorityType priorityType) {
    this.priorityType = priorityType;
  }

  @Override
  public void getAssignee(Person person) {
    this.person = person;
  }

  @Override
  public String getStatus() {
    return storyStatusType.toString();
  }

  @Override
  public PriorityType getPriority() {
    return priorityType;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append(super.toString())
      .append(String.format("Story Size: %s", getSize())).append(System.lineSeparator())
      .append(String.format("Priority Type: %s",
        super.getPriority())).append(System.lineSeparator())
      .append(String.format("Story Status: %s", getStatus()));
    return sb.toString();
  }
}
