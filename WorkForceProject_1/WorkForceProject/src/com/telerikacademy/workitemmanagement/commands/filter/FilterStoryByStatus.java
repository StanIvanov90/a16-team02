package com.telerikacademy.workitemmanagement.commands.filter;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.enums.StoryStatusType;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.workitemmanagement.commands.CommandConstants.NO_STORY_CREATED;

public class FilterStoryByStatus implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
  private static final String NO_EXISTING_PEOPLE = "No Existing People.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public FilterStoryByStatus(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    StoryStatusType changeStoryStatusType = StoryStatusType.valueOf(parameters.get(0).toUpperCase());

    String result = wimRepository.getWorkItems().values().stream()
        .filter(workItems -> workItems.getClass().getSimpleName()
        .replace("Impl", "")
        .equals("Story"))
        .filter(s -> s.getStatus().equals(changeStoryStatusType.toString()))
        .map(workItems -> workItems.getTitle())
        .collect(Collectors.joining(String.format("%n")));
    if (result.isEmpty()) {
      throw new IllegalArgumentException(NO_STORY_CREATED);
    }
    return result;
  }
}
