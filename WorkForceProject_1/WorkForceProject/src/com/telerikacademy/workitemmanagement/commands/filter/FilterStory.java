package com.telerikacademy.workitemmanagement.commands.filter;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.NO_STORY_CREATED;

public class FilterStory implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;
  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public FilterStory(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {

    String result = wimRepository.getWorkItems().values().stream()
        .filter(workItems -> workItems.getClass().getSimpleName()
        .replace("Impl", "")
        .equals("Story"))
        .map(WorkItems::getTitle)
        .collect(Collectors.joining(String.format("%n")));

    if (result.isEmpty()) {
      throw new IllegalArgumentException(NO_STORY_CREATED);
    }
    return result;
  }
}
