package com.telerikacademy.workitemmanagement.commands.filter;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class FilterByAssignee implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
  private static final String NO_EXISTING_PEOPLE = "No Existing People.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public FilterByAssignee(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String assignee = parameters.get(0);

    String result = wimRepository.getAllPeople().get(assignee)
        .listWorkItems().stream()
        .map(WorkItems::toString)
        .collect(Collectors.joining(String.format("%n")));

    if (result.isEmpty()) {
      throw new IllegalArgumentException(NO_WORKITEM_CREATED);
    }
    return result;
  }
}

