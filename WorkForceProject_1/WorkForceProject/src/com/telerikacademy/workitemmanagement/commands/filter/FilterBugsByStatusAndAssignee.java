package com.telerikacademy.workitemmanagement.commands.filter;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;

import java.util.List;
import java.util.stream.Collectors;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.workitemmanagement.commands.CommandConstants.NO_BUG_CREATED;

public class FilterBugsByStatusAndAssignee implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
  private static final String NO_EXISTING_PEOPLE = "No Existing People.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public FilterBugsByStatusAndAssignee(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);
    if (wimRepository.getWorkItems().size() == 0) {
      throw new IllegalArgumentException(NO_EXISTING_PEOPLE);
    }

    String assignee = parameters.get(0);
    BugStatusType bugStatusType = BugStatusType.valueOf(parameters.get(1).toUpperCase());

    String result = wimRepository.getAllPeople().get(assignee)
        .listWorkItems()
        .stream()
        .filter(workItems -> workItems.getClass().getSimpleName()
        .replace("Impl", "")
        .equals("Bug"))
        .filter(s -> s.getStatus().equals(bugStatusType.toString()))
        .map(workItems -> workItems.getTitle())
        .collect(Collectors.joining(String.format("%n")));
    if (result.isEmpty()) {
      throw new IllegalArgumentException(NO_BUG_CREATED);
    }
    return result;
  }
}

