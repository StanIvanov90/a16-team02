package com.telerikacademy.workitemmanagement.commands.create;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class CreatePerson implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public CreatePerson(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(), CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
    String personName = parameters.get(0);
    return createPerson(personName);
  }

  private String createPerson(String personName) {

    if (wimRepository.getAllPeople().containsKey(personName)) {
      return String.format(PERSON_ALREADY_EXISTS, personName);
    }

    Person person = wimFactory.createPerson(personName);
    wimRepository.addPerson(personName, person);

    return String.format(PERSON_CREATED, personName);
  }
}
