package com.telerikacademy.workitemmanagement.commands.create;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class CreateBoard implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public CreateBoard(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String boardName = parameters.get(0);
    String personNameToAddActivity = parameters.get(1);
    String teamNameToAddActivity = parameters.get(2);
    return createBoard(boardName, personNameToAddActivity, teamNameToAddActivity);
  }

  private String createBoard(String boardName,
                             String personNameToAddActivity,
                             String teamNameToAddActivity) {

    if (!wimRepository.getTeams().containsKey(teamNameToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamNameToAddActivity));
    }


    Board board = wimFactory.createBoard(boardName);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    Team teamToAddActivity = wimRepository.getTeams().get(teamNameToAddActivity);


    Validation.checkIfPersonBelongsToTeam(teamToAddActivity, personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, teamNameToAddActivity));
    teamToAddActivity.addActivityHistoryToTeam(
        String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName, personNameToAddActivity));
    personToAddActivity.addActivityHistory(
        String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName, personNameToAddActivity));

    teamToAddActivity.addBoard(board);

    return String.format(BOARD_CREATED_SUCCESS_MESSAGE, boardName, personNameToAddActivity);
  }
}
