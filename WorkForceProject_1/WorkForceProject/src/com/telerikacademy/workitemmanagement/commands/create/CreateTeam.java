package com.telerikacademy.workitemmanagement.commands.create;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;
import static com.telerikacademy.workitemmanagement.commands.CommandConstants.TEAM_CREATED;

public class CreateTeam implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public CreateTeam(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String teamName = parameters.get(0);
    String personNameToAddActivity = parameters.get(1);

    return createTeam(teamName, personNameToAddActivity);
  }

  private String createTeam(String name, String personNameToAddActivity) {

    Team team = wimFactory.createTeam(name);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    wimRepository.addTeam(name, team);
    team.addMember(personToAddActivity);
    personToAddActivity.addActivityHistory(
        String.format(TEAM_CREATED, name, personNameToAddActivity));

    return String.format(TEAM_CREATED, name, personNameToAddActivity);
  }
}


