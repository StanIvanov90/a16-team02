package com.telerikacademy.workitemmanagement.commands.show;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;
import static com.telerikacademy.workitemmanagement.commands.Validation.CheckIfActivityListIsEmpty;
import static com.telerikacademy.workitemmanagement.commands.Validation.CheckIfContainsKey;

public class ShowActivity implements Command {
    public static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;
    private final WimFactory wimFactory;
    private final WimRepository wimRepository;

    public ShowActivity(WimFactory wimFactory, WimRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        Validation.checkParameterSize(parameters.size(),
                CORRECT_NUMBER_OF_ARGUMENTS,
                INVALID_NUMBER_OF_ARGUMENTS);
        String boardToShowActivity = parameters.get(0);
        String typeOfTheActivity = parameters.get(1).toUpperCase();

        switch (typeOfTheActivity) {
//      case "BOARD":
//        CheckIfContainsKey(boardToShowActivity,
//            wimRepository.getBoards().containsKey(boardToShowActivity), BOARD_NOT_FOUND);
            // return showAllBoardActivity(boardToShowActivity, typeOfTheActivity);
            case "MEMBER":
                CheckIfContainsKey(boardToShowActivity,
                        wimRepository.getAllPeople().containsKey(boardToShowActivity), PERSON_NOT_FOUND);
                return showAllBoardActivity(boardToShowActivity, typeOfTheActivity);
            case "TEAM":
                CheckIfContainsKey(boardToShowActivity,
                        wimRepository.getTeams().containsKey(boardToShowActivity), NO_TEAM_FOUND);
                return showAllBoardActivity(boardToShowActivity, typeOfTheActivity);
            default:
                throw new IllegalArgumentException(SHOW_COMMAND_ERROR_MESSAGE);
        }
    }

    private String showAllBoardActivity(String showActivity, String typeOfTheActivity) {
        switch (typeOfTheActivity) {
//            case "BOARD":
//                Board board = wimRepository.getBoards().get(showActivity);
//                return updatedActivityList(showActivity, board.showActivity(), NO_BOARD_ACTIVITY);
            case "MEMBER":
                Person person = wimRepository.getAllPeople().get(showActivity);
                return updatedActivityList(showActivity, person.showActivity(), NO_PERSON_ACTIVITY);
            case "TEAM":
                Team team = wimRepository.getTeams().get(showActivity);
                return updatedActivityList(showActivity, team.teamActivity(), NO_ACTIVITY_IN_TEAM);
            default:
                throw new IllegalArgumentException(SHOW_COMMAND_ERROR_MESSAGE);
        }
    }

    private String updatedActivityList(String instruction,
                                       List<String> showActivity,
                                       String errorMessage) {
        List<String> activityList = new ArrayList<>(showActivity);
        CheckIfActivityListIsEmpty(instruction, activityList.isEmpty(), errorMessage);
        return activityList.toString().replace("[", "").replace("]", "");
    }
}
