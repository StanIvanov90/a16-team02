package com.telerikacademy.workitemmanagement.commands.show;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllBoardsInATeam implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

    private final WimFactory wimFactory;
    private final WimRepository wimRepository;

    public ShowAllBoardsInATeam(WimFactory wimFactory, WimRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }

        String teamToShowBoard = parameters.get(0);
        return showCompanyCatalog(teamToShowBoard);
    }

        private String showCompanyCatalog(String teamToShowBoard) {



            Team team = wimRepository.getTeams().get(teamToShowBoard);
            List<Board> boardList = new ArrayList<>(team.showBoard());
            List<String> boards = new ArrayList<>();
            for (Board b : boardList) {
                boards.add(b.getName());
            }

            return boards.toString().replace("[", "").replace("]", "");
        }
    }

