package com.telerikacademy.workitemmanagement.commands.show;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.BOARD_NOT_PART_OF_TEAM_ERROR;
import static com.telerikacademy.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowBoardActivity implements Command {

    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

    private final WimFactory wimFactory;
    private final WimRepository wimRepository;

    public ShowBoardActivity(WimFactory wimFactory, WimRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
            throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
        }
        String boardToShowActivity = parameters.get(0);
        String teamToShowBoard = parameters.get(1);
        return showCompanyCatalog(boardToShowActivity, teamToShowBoard);
    }

    private String showCompanyCatalog(String boardToShowActivity, String teamToShowBoard) {

        Team team = wimRepository.getTeams().get(teamToShowBoard);
        Board board = team.showBoard().get(
                Validation.checkIndexOfBoard(team, teamToShowBoard));

        Validation.checkIfBoardBelongsToTeam(team, boardToShowActivity,
                String.format(BOARD_NOT_PART_OF_TEAM_ERROR,
                        boardToShowActivity,
                        teamToShowBoard));

        return board.showActivity().toString().replace("[","").replace("]","");
    }
}
