package com.telerikacademy.workitemmanagement.commands.show;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class ShowAll implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ShowAll(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS, INVALID_NUMBER_OF_ARGUMENTS);
    String typeOfTheActivity = parameters.get(0).toUpperCase();
    switch (typeOfTheActivity) {
//      case "BOARD":
//        return checkIfSizeIsDifferentThanZero(typeOfTheActivity,
//            wimRepository.getBoards().size(), NO_EXISTING_BOARDS,"BOARD");
      case "MEMBER":
        return checkIfSizeIsDifferentThanZero(typeOfTheActivity,
            wimRepository.getAllPeople().size(), NO_EXISTING_PEOPLE, "MEMBER");
      case "TEAM":
        return checkIfSizeIsDifferentThanZero(typeOfTheActivity,
            wimRepository.getTeams().size(), NO_EXISTING_TEAMS, "TEAM");
      default:
        throw new IllegalArgumentException(SHOW_COMMAND_ERROR_MESSAGE);
    }
  }

  private String showAllActivity(String teamToShowMembers, String typeOfTheActivity) {
    switch (typeOfTheActivity) {
      case "TEAM" :
        return showAllMethod(wimRepository.getTeams().keySet());
//      case "BOARD":
//        return showAllMethod(wimRepository.getBoards().keySet());
      case "MEMBER":
        return showAllMethod(wimRepository.getAllPeople().keySet());
      default:
        throw new IllegalArgumentException(SHOW_COMMAND_ERROR_MESSAGE);
    }
  }

  private String checkIfSizeIsDifferentThanZero(String typeOfTheActivity,
                                                int size,
                                                String message,
                                                String type) {
    if (size == 0) {
      throw new IllegalArgumentException(message);
    }
    return showAllActivity(typeOfTheActivity, type);
  }

  private String showAllMethod(Set<String> strings) {
    List<String> showAllList = new ArrayList<>(strings);
    return showAllList.toString().replace("[", "").replace("]", "");
  }
}

