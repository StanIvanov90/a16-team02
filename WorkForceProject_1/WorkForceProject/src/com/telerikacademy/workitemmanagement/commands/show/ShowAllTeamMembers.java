package com.telerikacademy.workitemmanagement.commands.show;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.ArrayList;
import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.INVALID_NUMBER_OF_ARGUMENTS;

public class ShowAllTeamMembers implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 1;
  private static final String NO_TEAM_FOUND = "No Team Found.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ShowAllTeamMembers(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String teamToShowMembers = parameters.get(0);

    if (!wimRepository.getTeams().containsKey(teamToShowMembers)) {
      throw new IllegalArgumentException(NO_TEAM_FOUND);
    }
    return showAllTeamMembers(teamToShowMembers);
  }

  private String showAllTeamMembers(String teamToShowMembers) {

    Team team = wimRepository.getTeams().get(teamToShowMembers);
    List<Person> personList = new ArrayList<>(team.showTeamMembers());

    return personList.toString().replace("[", "").replace("]", "");
  }
}
