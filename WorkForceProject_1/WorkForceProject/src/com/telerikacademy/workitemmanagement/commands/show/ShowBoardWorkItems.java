package com.telerikacademy.workitemmanagement.commands.show;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class ShowBoardWorkItems implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ShowBoardWorkItems(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    if (parameters.size() != CORRECT_NUMBER_OF_ARGUMENTS) {
      throw new IllegalArgumentException(INVALID_NUMBER_OF_ARGUMENTS);
    }
    String boardToShowItems = parameters.get(0);
    String teamToShowBoard = parameters.get(1);
    return showCompanyCatalog(boardToShowItems, teamToShowBoard);
  }

  private String showCompanyCatalog(String boardToShowItems, String teamToShowBoard) {

    Team team = wimRepository.getTeams().get(teamToShowBoard);
    Board board = team.showBoard().get(
        Validation.checkIndexOfBoard(team, boardToShowItems));

    Validation.checkIfBoardBelongsToTeam(team, boardToShowItems,
        String.format(BOARD_NOT_PART_OF_TEAM_ERROR,
            boardToShowItems,
            teamToShowBoard));

    return board.workItemsDetails();
  }
}
