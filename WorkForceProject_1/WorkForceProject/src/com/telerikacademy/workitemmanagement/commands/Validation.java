package com.telerikacademy.workitemmanagement.commands;

import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.List;

public class Validation {

  public static void CheckIfContainsKey(String boardToShowActivity,
                                        boolean doesNotContainsKey,
                                        String boardNotFound) {
    if (!doesNotContainsKey) {
      throw new IllegalArgumentException(
        String.format(boardNotFound, boardToShowActivity));
    }
  }

  public static void CheckIfActivityListIsEmpty(String showActivity,
                                                boolean empty,
                                                String noActivity) {
    if (empty) {
      throw new IllegalArgumentException(
        String.format(noActivity, showActivity));
    }
  }

  public static void checkParameterSize(int parametersSize,
                                        int numbersOfArgs,
                                        String message) {
    if (parametersSize != numbersOfArgs) {
      throw new IllegalArgumentException(message);
    }
  }

  public static void checkIfPersonBelongsToTeam(Team team,
                                                Person person,
                                                String errorMessage) {
    if (!team.showTeamMembers().contains(person)) {
      throw new IllegalArgumentException(errorMessage);
    }
  }


  public static void checkIfWorkItemBelongsToBoard(Board board,
                                                   String name,
                                                   String errorMessage) {
    boolean contained = false;
    List<WorkItems> boardList = board.listWorkItems();
    for (WorkItems i : boardList) {
      if (i.getTitle().equals(name)) {
        contained = true;
        break;
      }
    }
    if (!contained) {
      throw new IllegalArgumentException(errorMessage);
    }
  }

  public static int checkIndexOfBoard(Team team, String name) {
    int index = 0;
    List<Board> boardList = team.showBoard();
    for (Board i : boardList) {
      if (i.getName().equals(name)) {
        index = boardList.indexOf(i);
        break;
      }
    }
    return index;
  }

  public static void checkIfBoardBelongsToTeam(Team team,
                                               String name,
                                               String errorMessage) {
    boolean contained = false;
    List<Board> boardList = team.showBoard();
    for (Board i : boardList) {
      if (i.getName().equals(name)) {
        contained = true;
        break;
      }
    }
    if (!contained) {
      throw new IllegalArgumentException(errorMessage);
    }
  }

}