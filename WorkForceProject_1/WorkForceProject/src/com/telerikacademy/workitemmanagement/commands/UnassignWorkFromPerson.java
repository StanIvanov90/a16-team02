package com.telerikacademy.workitemmanagement.commands;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class UnassignWorkFromPerson implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public UnassignWorkFromPerson(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String workItemToBeRemoved = parameters.get(0);
    String personToRemoveToName = parameters.get(1);

    if (!wimRepository.getAllPeople().containsKey(personToRemoveToName)) {
      throw new IllegalArgumentException(
          String.format(PERSON_NOT_FOUND, personToRemoveToName));
    }

    if (!wimRepository.getWorkItems().containsKey(workItemToBeRemoved)) {
      throw new IllegalArgumentException(
          String.format(WORK_ITEM_NOT_FOUND, workItemToBeRemoved));
    }

    WorkItems workItems = wimRepository.getWorkItems().get(workItemToBeRemoved);
    Person person = wimRepository.getAllPeople().get(personToRemoveToName);

    person.unassignWork(workItems);
    person.addActivityHistory(String.format(WORK_ITEM_UNASSIGNED_FROM_PERSON,
            workItemToBeRemoved,
            personToRemoveToName));

    return String.format(WORK_ITEM_UNASSIGNED_FROM_PERSON,
      workItemToBeRemoved,
      personToRemoveToName);
  }
}
