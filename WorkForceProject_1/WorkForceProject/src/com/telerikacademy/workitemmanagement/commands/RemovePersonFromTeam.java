package com.telerikacademy.workitemmanagement.commands;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;
import static com.telerikacademy.workitemmanagement.commands.CommandConstants.TEAM_NOT_FOUND_ERROR_MESSAGE;

public class RemovePersonFromTeam implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public RemovePersonFromTeam(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String personToBeRemovedName = parameters.get(0);
    String teamToRemoveToName = parameters.get(1);
    String personNameToAddActivity = parameters.get(2);

    if (!wimRepository.getAllPeople().containsKey(personToBeRemovedName)) {
      throw new IllegalArgumentException(String.format(PERSON_NOT_FOUND, personToBeRemovedName));
    }

    if (!wimRepository.getTeams().containsKey(teamToRemoveToName)) {
      throw new IllegalArgumentException(
        String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToRemoveToName));
    }
    Team team = wimRepository.getTeams().get(teamToRemoveToName);
    Person person = wimRepository.getAllPeople().get(personToBeRemovedName);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    Validation.checkIfPersonBelongsToTeam(team, personToAddActivity,
            String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, team));
    team.addActivityHistoryToTeam(
            String.format(PERSON_REMOVED_FROM_TEAM_SUCCESS_MESSAGE,
                    personToBeRemovedName,
                    teamToRemoveToName,
                    personNameToAddActivity));
    personToAddActivity.addActivityHistory(
            String.format(PERSON_REMOVED_FROM_TEAM_SUCCESS_MESSAGE,
                    personToBeRemovedName,
                    teamToRemoveToName,
                    personNameToAddActivity));
    team.removeMember(person);

    return String.format(PERSON_REMOVED_FROM_TEAM_SUCCESS_MESSAGE,
      personToBeRemovedName,
      teamToRemoveToName,personNameToAddActivity);
  }
}


