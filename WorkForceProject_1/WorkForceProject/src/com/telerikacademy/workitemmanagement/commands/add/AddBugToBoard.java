package com.telerikacademy.workitemmanagement.commands.add;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SeverityType;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AddBugToBoard implements Command {
    private static final int CORRECT_NUMBER_OF_ARGUMENTS = 8;

    private final WimFactory wimFactory;
    private final WimRepository wimRepository;

    public AddBugToBoard(WimFactory wimFactory, WimRepository wimRepository) {
        this.wimFactory = wimFactory;
        this.wimRepository = wimRepository;
    }

    @Override
    public String execute(List<String> parameters) {
        Validation.checkParameterSize(parameters.size(),
                CORRECT_NUMBER_OF_ARGUMENTS,
                INVALID_NUMBER_OF_ARGUMENTS);

        String bugDescription = parameters.get(0);
        String bugTitle = parameters.get(1);

        PriorityType bugPriorityType = PriorityType.valueOf(parameters.get(2).toUpperCase());
        SeverityType bugSeverityType = SeverityType.valueOf(parameters.get(3).toUpperCase());
        BugStatusType bugStatusType = BugStatusType.valueOf(parameters.get(4).toUpperCase());

        String personNameToAddActivity = parameters.get(5);
        String boardToAddToName = parameters.get(6);
        String teamNameToAddActivity = parameters.get(7);

        return createBug(bugDescription,
                bugTitle,
                bugPriorityType,
                bugSeverityType,
                bugStatusType,
                personNameToAddActivity,
                boardToAddToName,
                teamNameToAddActivity);
    }

    private String createBug(String bugDescription,
                             String bugTitle,
                             PriorityType bugPriorityType,
                             SeverityType bugSeverityType,
                             BugStatusType bugStatusType,
                             String personNameToAddActivity,
                             String boardToAddToName,
                             String teamNameToAddActivity) {

        if (!wimRepository.getTeams().containsKey(teamNameToAddActivity)) {
            throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamNameToAddActivity));
        }

        if (wimRepository.getWorkItems().containsKey(bugTitle)) {
            throw new IllegalArgumentException(String.format(WORK_ITEM_ALREADY_EXISTS, bugTitle));
        }

        Bug bug = wimFactory.createBug(
                bugDescription, bugTitle, bugPriorityType, bugSeverityType, bugStatusType);
        Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
        Team teamToAddActivity = wimRepository.getTeams().get(teamNameToAddActivity);
        Board board = teamToAddActivity.showBoard().get(
                Validation.checkIndexOfBoard(teamToAddActivity, boardToAddToName));

        Validation.checkIfPersonBelongsToTeam(teamToAddActivity, personToAddActivity,
                String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, teamNameToAddActivity));
        Validation.checkIfBoardBelongsToTeam(teamToAddActivity, boardToAddToName,
                String.format(BOARD_NOT_PART_OF_TEAM_ERROR, boardToAddToName, teamNameToAddActivity));
        Validation.checkIfBoardBelongsToTeam(teamToAddActivity, boardToAddToName,
                String.format(BOARD_NOT_PART_OF_TEAM_ERROR, boardToAddToName, teamNameToAddActivity));

        wimRepository.addWorkItem(bugTitle, bug);
        board.addBug(bug);
        bug.addWorkItemHistory(String.format(BUG_CREATED,
                bugTitle,
                personNameToAddActivity));
        board.addActivityHistoryToBoard(String.format(BUG_CREATED,
                bugTitle,
                personNameToAddActivity));
        teamToAddActivity.addActivityHistoryToTeam(String.format(BUG_CREATED,
                bugTitle,
                personNameToAddActivity));
        personToAddActivity.addActivityHistory(String.format(BUG_CREATED,
                bugTitle,
                personNameToAddActivity));

        return String.format(BUG_CREATED, bugTitle, personNameToAddActivity);
    }
}
