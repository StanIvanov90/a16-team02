package com.telerikacademy.workitemmanagement.commands.add;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AddRepoStepsToBug implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public AddRepoStepsToBug(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);
    String bugToAddToName = parameters.get(0);
    String repoStepToBeAddedToBug = parameters.get(1);
    String personName = parameters.get(2);
    String boardToAddToName = parameters.get(3);
    String teamNameToAddActivity = parameters.get(4);

    if (!wimRepository.getTeams().containsKey(teamNameToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamNameToAddActivity));
    }

    if (!wimRepository.getWorkItems().containsKey(bugToAddToName)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND, bugToAddToName));
    }

    Person person = wimRepository.getAllPeople().get(personName);
    Board board = wimRepository.getBoards().get(boardToAddToName);
    Team teamToAddActivity = wimRepository.getTeams().get(teamNameToAddActivity);

    Validation.checkIfPersonBelongsToTeam(teamToAddActivity, person,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, person, teamToAddActivity));
    Validation.checkIfBoardBelongsToTeam(teamToAddActivity, boardToAddToName,
        String.format(BOARD_NOT_PART_OF_TEAM_ERROR, board, teamToAddActivity));
    Bug bug = (Bug) wimRepository.getWorkItems().get(bugToAddToName);
    bug.addWorkItemHistory(
        String.format(REPO_STEP_ADDED_TO_BUG, repoStepToBeAddedToBug, bugToAddToName, personName));
    board.addActivityHistoryToBoard(
        String.format(REPO_STEP_ADDED_TO_BUG, repoStepToBeAddedToBug, bugToAddToName, personName));
    teamToAddActivity.addActivityHistoryToTeam(
        String.format(REPO_STEP_ADDED_TO_BUG, repoStepToBeAddedToBug, bugToAddToName, personName));
    person.addActivityHistory(
        String.format(REPO_STEP_ADDED_TO_BUG, repoStepToBeAddedToBug, bugToAddToName, personName));
    bug.addStepToRepo(repoStepToBeAddedToBug);

    return String.format(REPO_STEP_ADDED_TO_BUG,
        repoStepToBeAddedToBug,
        bugToAddToName,
        personName);
  }
}
