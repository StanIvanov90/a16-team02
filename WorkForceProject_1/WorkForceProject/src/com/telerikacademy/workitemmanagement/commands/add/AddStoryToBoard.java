package com.telerikacademy.workitemmanagement.commands.add;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Story;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.PriorityType;
import com.telerikacademy.workitemmanagement.models.enums.SizeType;
import com.telerikacademy.workitemmanagement.models.enums.StoryStatusType;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AddStoryToBoard implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 8;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public AddStoryToBoard(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String storyDescription = parameters.get(0);
    String storyTitle = parameters.get(1);

    PriorityType storyPriorityType = PriorityType.valueOf(parameters.get(2).toUpperCase());
    SizeType storySizeType = SizeType.valueOf(parameters.get(3).toUpperCase());

    StoryStatusType storyStatusType = StoryStatusType.valueOf(parameters.get(4).toUpperCase());

    String personNameToAddActivity = parameters.get(5);
    String boardToAddToName = parameters.get(6);
    String teamNameToAddActivity = parameters.get(7);

    return createStory(storyDescription,
      storyTitle,
      storyPriorityType,
      storySizeType,
      storyStatusType,
      personNameToAddActivity,
      boardToAddToName,
      teamNameToAddActivity);
  }

  private String createStory(String storyDescription,
                             String storyTitle,
                             PriorityType storyPriorityType,
                             SizeType storySizeType,
                             StoryStatusType storyStatusType,
                             String personNameToAddActivity,
                             String boardToAddToName,
                             String teamNameToAddActivity) {

    if (!wimRepository.getTeams().containsKey(teamNameToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamNameToAddActivity));
    }

    if (wimRepository.getWorkItems().containsKey(storyTitle)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_ALREADY_EXISTS, storyTitle));
    }

    Story story = wimFactory.createStory(
        storyDescription, storyTitle, storyPriorityType, storySizeType, storyStatusType);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    Team teamToAddActivity = wimRepository.getTeams().get(teamNameToAddActivity);
    Board board = teamToAddActivity.showBoard().get(
            Validation.checkIndexOfBoard(teamToAddActivity, boardToAddToName));
    Validation.checkIfPersonBelongsToTeam(teamToAddActivity, personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, teamNameToAddActivity));
    Validation.checkIfBoardBelongsToTeam(teamToAddActivity, boardToAddToName,
        String.format(BOARD_NOT_PART_OF_TEAM_ERROR, boardToAddToName, teamNameToAddActivity));

    wimRepository.addWorkItem(storyTitle, story);
    board.addStory(story);
    story.addWorkItemHistory(
        String.format(STORY_CREATED, storyTitle, personNameToAddActivity));
    board.addActivityHistoryToBoard(
        String.format(STORY_CREATED, storyTitle, personNameToAddActivity));
    teamToAddActivity.addActivityHistoryToTeam(
        String.format(STORY_CREATED, storyTitle, personNameToAddActivity));
    personToAddActivity.addActivityHistory(
        String.format(STORY_CREATED, storyTitle, personNameToAddActivity));

    return String.format(STORY_CREATED, storyTitle, personNameToAddActivity);
  }

}
