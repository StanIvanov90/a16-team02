package com.telerikacademy.workitemmanagement.commands.add;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AddFeedbackToBoard implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 7;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public AddFeedbackToBoard(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String feedbackDescription = parameters.get(0);
    String feedbackTitle = parameters.get(1);
    FeedbackStatusType feedbackStatusType = FeedbackStatusType.valueOf(parameters.get(2).toUpperCase());
    int feedbackRating = Integer.parseInt(parameters.get(3));
    String personNameToAddActivity = parameters.get(4);
    String boardToAddToName = parameters.get(5);
    String teamNameToAddActivity = parameters.get(6);

    return createFeedBack(feedbackDescription,
      feedbackTitle,
      feedbackStatusType,
      feedbackRating,
      personNameToAddActivity,
      boardToAddToName,
      teamNameToAddActivity);
  }

  private String createFeedBack(String feedbackDescription,
                                String feedbackTitle,
                                FeedbackStatusType feedbackStatusType,
                                int feedbackRating,
                                String personNameToAddActivity,
                                String boardToAddToName,
                                String teamNameToAddActivity) {

    if (!wimRepository.getTeams().containsKey(teamNameToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamNameToAddActivity));
    }

    if (wimRepository.getWorkItems().containsKey(feedbackTitle)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_ALREADY_EXISTS, feedbackTitle));
    }

    Feedback feedback = wimFactory.createFeedBack(
        feedbackDescription, feedbackTitle, feedbackStatusType, feedbackRating);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    Team teamToAddActivity = wimRepository.getTeams().get(teamNameToAddActivity);
    Board board = teamToAddActivity.showBoard().get(
            Validation.checkIndexOfBoard(teamToAddActivity, boardToAddToName));
    Validation.checkIfPersonBelongsToTeam(teamToAddActivity, personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, teamNameToAddActivity));
    Validation.checkIfBoardBelongsToTeam(teamToAddActivity, boardToAddToName,
        String.format(BOARD_NOT_PART_OF_TEAM_ERROR, boardToAddToName, teamNameToAddActivity));

    wimRepository.addWorkItem(feedbackTitle, feedback);
    board.addFeedback(feedback);
    feedback.addWorkItemHistory(
        String.format(FEEDBACK_CREATED, feedbackTitle, personNameToAddActivity));
    board.addActivityHistoryToBoard(
        String.format(FEEDBACK_CREATED, feedbackTitle, personNameToAddActivity));
    teamToAddActivity.addActivityHistoryToTeam(
        String.format(FEEDBACK_CREATED, feedbackTitle, personNameToAddActivity));
    personToAddActivity.addActivityHistory(
        String.format(FEEDBACK_CREATED, feedbackTitle, personNameToAddActivity));

    return String.format(FEEDBACK_CREATED, feedbackTitle, personNameToAddActivity);
  }
}
