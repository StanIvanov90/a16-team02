package com.telerikacademy.workitemmanagement.commands.add;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AddPersonToTeam implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 3;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public AddPersonToTeam(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String personToBeAddedName = parameters.get(0);
    String teamToAddToName = parameters.get(1);
    String personNameToAddActivity = parameters.get(2);

    if (!wimRepository.getAllPeople().containsKey(personToBeAddedName)) {
      throw new IllegalArgumentException(String.format(PERSON_NOT_FOUND, personToBeAddedName));
    }

    if (!wimRepository.getTeams().containsKey(teamToAddToName)) {
      throw new IllegalArgumentException(
        String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddToName));
    }

    Team team = wimRepository.getTeams().get(teamToAddToName);
    Person personToAdd = wimRepository.getAllPeople().get(personToBeAddedName);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    Validation.checkIfPersonBelongsToTeam(team, personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, team));
    team.addActivityHistoryToTeam(
        String.format(PERSON_ADDED_TO_TEAM_SUCCESS_MESSAGE,
           personToBeAddedName,
           teamToAddToName,
           personNameToAddActivity));
    personToAddActivity.addActivityHistory(
        String.format(PERSON_ADDED_TO_TEAM_SUCCESS_MESSAGE,
           personToBeAddedName,
           teamToAddToName,
           personNameToAddActivity));
    team.addMember(personToAdd);

    return String.format(PERSON_ADDED_TO_TEAM_SUCCESS_MESSAGE,
        personToBeAddedName,
        teamToAddToName,
        personNameToAddActivity);
  }
}
