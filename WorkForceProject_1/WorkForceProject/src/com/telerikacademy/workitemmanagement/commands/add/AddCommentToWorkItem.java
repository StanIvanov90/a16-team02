package com.telerikacademy.workitemmanagement.commands.add;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AddCommentToWorkItem implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public AddCommentToWorkItem(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String workItemToAddToName = parameters.get(0);
    String commentItemToBeAdded = parameters.get(1);
    String personName = parameters.get(2);
    String boardToAddToName = parameters.get(3);
    String teamNameToAddActivity = parameters.get(4);

    if (!wimRepository.getTeams().containsKey(teamNameToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamNameToAddActivity));
    }

    if (!wimRepository.getWorkItems().containsKey(workItemToAddToName)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND, workItemToAddToName));
    }

    WorkItems workItems = wimRepository.getWorkItems().get(workItemToAddToName);
    Person person = wimRepository.getAllPeople().get(personName);
    Board board = wimRepository.getBoards().get(boardToAddToName);
    Team teamToAddActivity = wimRepository.getTeams().get(teamNameToAddActivity);

    Validation.checkIfPersonBelongsToTeam(teamToAddActivity, person,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, person, teamToAddActivity));
    Validation.checkIfBoardBelongsToTeam(teamToAddActivity, boardToAddToName,
        String.format(BOARD_NOT_PART_OF_TEAM_ERROR, board, teamToAddActivity));

    workItems.addComment(commentItemToBeAdded);
    board.addActivityHistoryToBoard(String.format(COMMENT_ADDED_TO_WORKITEM,
        commentItemToBeAdded, workItemToAddToName, personName));
    teamToAddActivity.addActivityHistoryToTeam(String.format(COMMENT_ADDED_TO_WORKITEM,
        commentItemToBeAdded, workItemToAddToName, personName));
    person.addActivityHistory(String.format(COMMENT_ADDED_TO_WORKITEM,
        commentItemToBeAdded, workItemToAddToName, personName));

    return String.format(COMMENT_ADDED_TO_WORKITEM,
      commentItemToBeAdded,
      workItemToAddToName,
      personName);
  }
}
