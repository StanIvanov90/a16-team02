package com.telerikacademy.workitemmanagement.commands.sort;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Story;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortBySize implements Command {
  private static final String NO_EXISTING_STORIES = "No Existing Stories";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public SortBySize(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {

    String result = wimRepository.getWorkItems()
        .values().stream()
        .filter(workItems -> workItems.getClass().getSimpleName()
        .replace("Impl", "")
        .equals("Story"))
        .map(workItem -> (Story) workItem)
        .sorted(Comparator.comparing(Story::getSize))
        .map(story -> story.getTitle() + " " + story.getSize().toString())
        .collect(Collectors.joining(String.format("%n")));
    if(result.isEmpty()){
      throw new IllegalArgumentException(NO_EXISTING_STORIES);
    }
    return result;
  }
}