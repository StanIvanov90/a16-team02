package com.telerikacademy.workitemmanagement.commands.sort;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortByRating implements Command {
  private static final String NO_EXISTING_FEEDBACK = "No Existing FeedBacks.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;


  public SortByRating(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {

    String result = wimRepository.getWorkItems().values().stream().filter(
        workItems -> workItems.getClass().getSimpleName()
         .replace("Impl", "")
         .equals("FeedBack"))
         .map(workItem -> (Feedback) workItem)
         .sorted(Comparator.comparingInt(Feedback::getRating))
         .map(feedback -> feedback.getTitle() + " " + feedback.getRating())
         .collect(Collectors.joining(String.format("%n")));
    if (result.isEmpty()) {
      throw new IllegalArgumentException(NO_EXISTING_FEEDBACK);
    }
    return result;
  }
}