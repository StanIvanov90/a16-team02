package com.telerikacademy.workitemmanagement.commands.sort;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class SortBugsByPriority implements Command {
  private static final String NO_EXISTING_BUGS = "No Existing Bugs.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public SortBugsByPriority(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {

    String result = wimRepository.getWorkItems()
        .values().stream()
        .filter(workItems -> workItems.getClass().getSimpleName()
        .replace("Impl", "")
        .equals("Bug"))
        .map(workItem -> (Bug) workItem)
        .sorted(Comparator.comparing(Bug::getBugPriority))
        .map(bug -> bug.getTitle() + " " + bug.getBugPriority().toString())
        .collect(Collectors.joining(String.format("%n")));
    if (result.isEmpty()) {
      throw new IllegalArgumentException(NO_EXISTING_BUGS);
    }
    return result;
  }
}