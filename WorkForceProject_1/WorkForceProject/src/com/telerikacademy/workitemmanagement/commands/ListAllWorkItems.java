package com.telerikacademy.workitemmanagement.commands;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;

import java.util.ArrayList;
import java.util.List;

public class ListAllWorkItems implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 0;
  private static final String NO_EXISTING_TEAMS = "No Existing Teams.";

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ListAllWorkItems(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {

    if (wimRepository.getWorkItems().size() == 0) {
      throw new IllegalArgumentException(NO_EXISTING_TEAMS);
    }

    List<String> allWorkItems = new ArrayList<>(wimRepository.getWorkItems().keySet());
    return allWorkItems.toString().replace("[","").replace("]","");
  }
}
