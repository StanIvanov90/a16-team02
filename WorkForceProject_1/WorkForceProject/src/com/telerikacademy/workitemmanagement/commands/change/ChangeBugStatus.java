package com.telerikacademy.workitemmanagement.commands.change;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Bug;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.BugStatusType;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class ChangeBugStatus implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ChangeBugStatus(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String bugStatusTitle = parameters.get(0);
    BugStatusType changeBugStatusType = BugStatusType.valueOf(parameters.get(1).toUpperCase());
    String personNameToAddActivity = parameters.get(2);
    String boardToAddActivity = parameters.get(3);
    String teamToAddActivity = parameters.get(4);



    return changeBugStatus(bugStatusTitle,
            changeBugStatusType ,
            personNameToAddActivity,
            boardToAddActivity,
            teamToAddActivity);
  }

  private String changeBugStatus(String bugStatusTitle,
                                 BugStatusType changeBugStatusType,
                                 String personNameToAddActivity,
                                 String boardToAddActivity,
                                 String teamToAddActivity) {

    if (!wimRepository.getTeams().containsKey(teamToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddActivity));
    }

    if (!wimRepository.getWorkItems().containsKey(bugStatusTitle)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND, bugStatusTitle));
    }

    Bug changeBugStatus =(Bug) wimRepository.getWorkItems().get(bugStatusTitle);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    changeBugStatus.setBugStatusType(changeBugStatusType);
    Team team = wimRepository.getTeams().get(teamToAddActivity);
    Board board = team.showBoard().get(
            Validation.checkIndexOfBoard(team, boardToAddActivity));

    Validation.checkIfPersonBelongsToTeam(team,personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR,personNameToAddActivity,teamToAddActivity));
    Validation.checkIfWorkItemBelongsToBoard(board,bugStatusTitle,
        String.format(WORKITEM_NOT_PART_OF_BOARD_ERROR,bugStatusTitle,boardToAddActivity));

    personToAddActivity.addActivityHistory(String.format(CHANGED_BUG_STATUS, bugStatusTitle, changeBugStatusType, personNameToAddActivity));
    board.addActivityHistoryToBoard(String.format(CHANGED_BUG_STATUS, bugStatusTitle, changeBugStatusType, personNameToAddActivity));
    team.addActivityHistoryToTeam(String.format(CHANGED_BUG_STATUS, bugStatusTitle, changeBugStatusType, personNameToAddActivity));
    return String.format(CHANGED_BUG_STATUS, bugStatusTitle, changeBugStatusType, personNameToAddActivity);
  }
}
