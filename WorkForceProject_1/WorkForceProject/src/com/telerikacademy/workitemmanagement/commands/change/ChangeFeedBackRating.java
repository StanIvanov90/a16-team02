package com.telerikacademy.workitemmanagement.commands.change;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class ChangeFeedBackRating implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ChangeFeedBackRating(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String feeedbackRatingTitle = parameters.get(0);
    int feedbackRating = Integer.parseInt(parameters.get(1));
    String personNameToAddActivity = parameters.get(2);
    String boardToAddActivity = parameters.get(3);
    String teamToAddActivity = parameters.get(4);

    return changeFeedbackRating(feeedbackRatingTitle,
      feedbackRating,
      personNameToAddActivity,
      boardToAddActivity,
      teamToAddActivity);
  }

  private String changeFeedbackRating(String feedbackRatingTitle,
                                      int feedbackRating,
                                      String personNameToAddActivity,
                                      String boardToAddActivity,
                                      String teamToAddActivity) {

    if (!wimRepository.getTeams().containsKey(teamToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddActivity));
    }

    if (!wimRepository.getWorkItems().containsKey(feedbackRatingTitle)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND, feedbackRatingTitle));
    }

    Feedback changeFeedbackRating = (Feedback) wimRepository.getWorkItems().get(feedbackRatingTitle);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    changeFeedbackRating.setIntRating(feedbackRating);
    Team team = wimRepository.getTeams().get(teamToAddActivity);
    Board board = team.showBoard().get(
        Validation.checkIndexOfBoard(team, boardToAddActivity));


    Validation.checkIfPersonBelongsToTeam(team, personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, teamToAddActivity));
    Validation.checkIfWorkItemBelongsToBoard(board,feedbackRatingTitle,
        String.format(WORKITEM_NOT_PART_OF_BOARD_ERROR, feedbackRatingTitle, boardToAddActivity));

    personToAddActivity.addActivityHistory(
        String.format(CHANGED_FEEDBACK_RATING, feedbackRatingTitle, feedbackRating, personNameToAddActivity));
    board.addActivityHistoryToBoard(
        String.format(CHANGED_FEEDBACK_RATING, feedbackRatingTitle, feedbackRating, personNameToAddActivity));
    team.addActivityHistoryToTeam(
        String.format(CHANGED_FEEDBACK_RATING, feedbackRatingTitle, feedbackRating, personNameToAddActivity));

    return String.format(CHANGED_FEEDBACK_RATING, feedbackRatingTitle, feedbackRating, personNameToAddActivity);
  }
}
