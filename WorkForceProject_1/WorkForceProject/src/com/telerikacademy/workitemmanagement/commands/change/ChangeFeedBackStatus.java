package com.telerikacademy.workitemmanagement.commands.change;

import com.telerikacademy.workitemmanagement.commands.Validation;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Board;
import com.telerikacademy.workitemmanagement.models.contracts.Feedback;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.Team;
import com.telerikacademy.workitemmanagement.models.enums.FeedbackStatusType;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class ChangeFeedBackStatus implements Command {
  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 5;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public ChangeFeedBackStatus(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String feedbackStatusTitle = parameters.get(0);
    FeedbackStatusType changeFeedbackStatusType = FeedbackStatusType.valueOf(parameters.get(1).toUpperCase());
    String personNameToAddActivity = parameters.get(2);
    String boardToAddActivity = parameters.get(3);
    String teamToAddActivity = parameters.get(4);

    return changeFeedBackStatus(feedbackStatusTitle,
      changeFeedbackStatusType,
      personNameToAddActivity,
      boardToAddActivity,
      teamToAddActivity);
  }

  private String changeFeedBackStatus(String feedbackStatusTitle,
                                      FeedbackStatusType changeBugFeedBackType,
                                      String personNameToAddActivity,
                                      String boardToAddActivity,
                                      String teamToAddActivity) {

    if (!wimRepository.getTeams().containsKey(teamToAddActivity)) {
      throw new IllegalArgumentException(String.format(TEAM_NOT_FOUND_ERROR_MESSAGE, teamToAddActivity));
    }

    if (!wimRepository.getWorkItems().containsKey(feedbackStatusTitle)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND, feedbackStatusTitle));
    }

    Feedback feedbackStatusType = (Feedback) wimRepository.getWorkItems().get(feedbackStatusTitle);
    Person personToAddActivity = wimRepository.getAllPeople().get(personNameToAddActivity);
    feedbackStatusType.setFeedback(changeBugFeedBackType);
    personToAddActivity.addActivityHistory(
        String.format(CHANGED_FEEDBACK_STATUS, feedbackStatusTitle, changeBugFeedBackType, personNameToAddActivity));
    Team team = wimRepository.getTeams().get(teamToAddActivity);
    Board board = team.showBoard().get(
        Validation.checkIndexOfBoard(team, boardToAddActivity));

    Validation.checkIfPersonBelongsToTeam(team, personToAddActivity,
        String.format(PERSON_NOT_PART_OF_TEAM_ERROR, personNameToAddActivity, teamToAddActivity));
    Validation.checkIfWorkItemBelongsToBoard(board,feedbackStatusTitle,
        String.format(WORKITEM_NOT_PART_OF_BOARD_ERROR, feedbackStatusTitle, boardToAddActivity));

    board.addActivityHistoryToBoard(
        String.format(CHANGED_FEEDBACK_STATUS, feedbackStatusTitle, changeBugFeedBackType, personNameToAddActivity));
    team.addActivityHistoryToTeam(
        String.format(CHANGED_FEEDBACK_STATUS, feedbackStatusTitle, changeBugFeedBackType, personNameToAddActivity));

    return String.format(CHANGED_FEEDBACK_STATUS, feedbackStatusTitle, changeBugFeedBackType, personNameToAddActivity);
  }
}


