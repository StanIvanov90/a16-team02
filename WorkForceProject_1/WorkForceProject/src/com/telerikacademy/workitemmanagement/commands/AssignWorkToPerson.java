package com.telerikacademy.workitemmanagement.commands;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.Person;
import com.telerikacademy.workitemmanagement.models.contracts.WorkItems;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.*;

public class AssignWorkToPerson implements Command {

  private static final int CORRECT_NUMBER_OF_ARGUMENTS = 2;

  private final WimFactory wimFactory;
  private final WimRepository wimRepository;

  public AssignWorkToPerson(WimFactory wimFactory, WimRepository wimRepository) {
    this.wimFactory = wimFactory;
    this.wimRepository = wimRepository;
  }

  @Override
  public String execute(List<String> parameters) {
    Validation.checkParameterSize(parameters.size(),
        CORRECT_NUMBER_OF_ARGUMENTS,
        INVALID_NUMBER_OF_ARGUMENTS);

    String workItemToBeAdded = parameters.get(0);
    String personToAddToName = parameters.get(1);

    if (!wimRepository.getAllPeople().containsKey(personToAddToName)) {
      throw new IllegalArgumentException(String.format(PERSON_NOT_FOUND, personToAddToName));
    }

    if (!wimRepository.getWorkItems().containsKey(workItemToBeAdded)) {
      throw new IllegalArgumentException(String.format(WORK_ITEM_NOT_FOUND, workItemToBeAdded));
    }

    WorkItems workItems = wimRepository.getWorkItems().get(workItemToBeAdded);
    Person person = wimRepository.getAllPeople().get(personToAddToName);

    person.assignWork(workItems);
    person.addActivityHistory(String.format(WORK_ITEM_ASSIGNED_TO_PERSON,
        workItemToBeAdded,
        personToAddToName));

    return String.format(WORK_ITEM_ASSIGNED_TO_PERSON, workItemToBeAdded, personToAddToName);
  }
}

