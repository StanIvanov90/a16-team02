package com.telerikacademy.workitemmanagement.commands;

public class CommandConstants {

  public static final String INVALID_NUMBER_OF_ARGUMENTS = "Invalid number of arguments.";
  public static final String TEAM_NOT_FOUND_ERROR_MESSAGE = "Team %s not found";
  public static final String BOARD_CREATED_SUCCESS_MESSAGE = "Board with name %s was created by %s";
  public static final String BUG_CREATED = "Bug with name %s was created by %s.";
  public static final String FEEDBACK_CREATED = "Feedback with name %s was created by %s.";
  public static final String PERSON_CREATED = "Member with name %s was created.";
  public static final String STORY_CREATED = "Story with name %s was created by %s.";
  public static final String TEAM_CREATED = "Team with name %s was created by %s.";
  public static final String PERSON_ADDED_TO_TEAM_SUCCESS_MESSAGE = "Person %s added to Team %s by %s";
  public static final String CHANGED_BUG_STATUS = "Change bug with name %s to have status %s by %s";
  public static final String CHANGED_BUG_SEVERITY = "Change bug with name %s to have severity %s by %s";
  public static final String CHANGED_BUG_PRIORITY = "Change bug with name %s to have priority %s by %s";
  public static final String CHANGED_FEEDBACK_STATUS = "Change feedback with name %s to have status %s by %s";
  public static final String CHANGED_FEEDBACK_RATING = "Change feedback with name %s to have %d rating by %s";
  public static final String CHANGED_STORY_SIZE = "Change story with name %s to have size %s by %s";
  public static final String CHANGED_STORY_STATUS = "Change story with name %s to have status %s by %s";
  public static final String CHANGED_STORY_PRIORITY = "Change story with name %s to have %s priority by %s";
  public static final String NO_FEEDBACK_CREATED = "No Feedback Created.";
  public static final String NO_BUG_CREATED = "No Bugs Created.";
  public static final String NO_STORY_CREATED = "No Stories Created.";
  public static final String WORK_ITEM_NOT_FOUND = "WorkItem %s not found";
  public static final String NO_WORKITEM_CREATED = "No WorkItems Created.";
  public static final String COMMENT_ADDED_TO_WORKITEM = "Comment: %s\n has been added to %s by %s";
  public static final String REPO_STEP_ADDED_TO_BUG = "Repo Step: %s\n has been added to %s by %s";
  public static final String BOARD_NOT_FOUND = "Board with name %s not found";
  public static final String PERSON_NOT_FOUND = "Person with name %s not found";
  public static final String BOARD_ALREADY_EXISTS = "Board with name %s already exists";
  public static final String PERSON_ALREADY_EXISTS = "Person with name %s already exists";
  public static final String WORK_ITEM_ALREADY_EXISTS = "Work item with name %s already exists";
  public static final String PERSON_NOT_PART_OF_TEAM_ERROR = "%s is not part of %s.";
  public static final String BOARD_NOT_PART_OF_TEAM_ERROR = "%s is not part of %s.";
  public static final String WORKITEM_NOT_PART_OF_BOARD_ERROR = "%s is not part of %s.";
  public static final String NO_BOARD_ACTIVITY = "%s has no activity.";
  public static final String NO_TEAM_FOUND = "No Team Found.";
  public static final String NO_PERSON_ACTIVITY = "%s has no activity.";
  public static final String NO_ACTIVITY_IN_TEAM = "The Team %s has no activity.";
  public static final String NO_EXISTING_TEAMS = "No Existing Teams.";
  public static final String NO_EXISTING_PEOPLE = "No Existing People.";
  public static final String NO_EXISTING_BOARDS = "No Existing Boards.";
  public static final String WRONG_BUG_STATUS_TYPE = "Wrong bug status type";
  public static final String WRONG_FEEDBACK_STATUS_TYPE = "Wrong feedback status type";
  public static final String WRONG_STORY_STATUS_TYPE = "Wrong story status type";
  public static final String WRONG_PRIORITY_TYPE = "Wrong priority type";
  public static final String WRONG_SEVERITY_TYPE = "Wrong severity type";
  public static final String WRONG_SIZE_TYPE = "Wrong size type";
  public static final String SHOW_COMMAND_ERROR_MESSAGE = "This command for do not exist! It must be \"member\",\"board\" or \"team\".";
  public static final String COMMAND_CANNOT_BE_NULL_OR_EMPTY = "Command cannot be null or empty.";
  public static final String RATING_CANNOT_BE_NEGATIVE = "Rating cannot be negative";
  public static final String OBJECT_CANNOT_BE_NULL = "Object cannot be null";
  static final String WORK_ITEM_ASSIGNED_TO_PERSON = "WorkItem %s has been assigned to %s";
  static final String WORK_ITEM_UNASSIGNED_FROM_PERSON = "WorkItem %s has been unassigned from %s";
  static final String PERSON_REMOVED_FROM_TEAM_SUCCESS_MESSAGE = "Person %s removed to Team %s by %s";
}

