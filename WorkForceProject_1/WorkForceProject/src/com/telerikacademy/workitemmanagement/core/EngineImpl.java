package com.telerikacademy.workitemmanagement.core;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.core.contracts.*;
import com.telerikacademy.workitemmanagement.core.factories.CommandFactoryImpl;
import com.telerikacademy.workitemmanagement.core.factories.WimFactoryImpl;
import com.telerikacademy.workitemmanagement.core.providers.CommandParserImpl;
import com.telerikacademy.workitemmanagement.core.providers.ConsoleReader;
import com.telerikacademy.workitemmanagement.core.providers.ConsoleWriter;

import java.util.List;

import static com.telerikacademy.workitemmanagement.commands.CommandConstants.COMMAND_CANNOT_BE_NULL_OR_EMPTY;

public class EngineImpl implements Engine {

  private static final String TERMINATION_COMMAND = "Exit";

  private WimFactory wimFactory;
  private CommandParser commandParser;
  private WimRepository wimRepository;
  private Writer writer;
  private Reader reader;
  private CommandFactory commandFactory;

  public EngineImpl() {
    writer = new ConsoleWriter();
    reader = new ConsoleReader();
    wimFactory = new WimFactoryImpl();
    commandParser = new CommandParserImpl();
    commandFactory = new CommandFactoryImpl();
    wimRepository = new WimRepositoryImpl();
  }

  @Override
  public void start() {
    while (true) {
      try {
        String commandAsString = reader.readLine();
        if (commandAsString.equalsIgnoreCase(TERMINATION_COMMAND)) {
          break;
        }
        processCommand(commandAsString);

      } catch (Exception ex) {
        writer.writeLine(ex.getMessage() != null
          && !ex.getMessage().isEmpty() ? ex.getMessage() : ex.toString());
      }
    }
  }

  private void processCommand(String commandAsString) {
    if (commandAsString == null || commandAsString.trim().equals("")) {
      throw new IllegalArgumentException(COMMAND_CANNOT_BE_NULL_OR_EMPTY);
    }

    String commandName = commandParser.parseCommand(commandAsString);
    Command command = commandFactory.createCommand(commandName, wimFactory, wimRepository);
    List<String> parameters = commandParser.parseParameters(commandAsString);
    String executionResult = command.execute(parameters);
    writer.writeLine(executionResult);
  }
}
