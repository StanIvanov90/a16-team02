package com.telerikacademy.workitemmanagement.core;

import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;
import com.telerikacademy.workitemmanagement.models.contracts.*;

import java.util.HashMap;
import java.util.Map;

public class WimRepositoryImpl implements WimRepository {

  private Map<String, Team> teams;
  private Map<String, Board> boards;
  private Map<String, WorkItems> workitems;
  private Map<String, Person> people;

  public WimRepositoryImpl() {
    this.teams = new HashMap<>();
    this.boards = new HashMap<>();
    this.workitems = new HashMap<>();
    this.people = new HashMap<>();
  }

  @Override
  public Map<String, Team> getTeams() {
    return new HashMap<>(teams);
  }

  @Override
  public Map<String, Board> getBoards() {
    return new HashMap<>(boards);
  }

  @Override
  public Map<String, WorkItems> getWorkItems() {
    return new HashMap<>(workitems);
  }

  @Override
  public Map<String, Person> getAllPeople() {
    return new HashMap<>(people);
  }

  @Override
  public void addPerson(String name, Person person) {
    this.people.put(name, person);
  }

  @Override
  public void addTeam(String name, Team team) {
    this.teams.put(name, team);
  }

  @Override
  public void addBoardToBoards(String name, Board board) {
    this.boards.put(name, board);
  }

  public void addWorkItem(String name, WorkItems workItems) {
    this.workitems.put(name, workItems);
  }

}
