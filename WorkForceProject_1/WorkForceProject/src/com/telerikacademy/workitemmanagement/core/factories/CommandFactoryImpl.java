package com.telerikacademy.workitemmanagement.core.factories;

import com.telerikacademy.workitemmanagement.commands.*;
import com.telerikacademy.workitemmanagement.commands.add.*;
import com.telerikacademy.workitemmanagement.commands.change.*;
import com.telerikacademy.workitemmanagement.commands.contracts.Command;
import com.telerikacademy.workitemmanagement.commands.create.*;
import com.telerikacademy.workitemmanagement.commands.enums.CommandType;
import com.telerikacademy.workitemmanagement.commands.filter.*;
import com.telerikacademy.workitemmanagement.commands.show.*;
import com.telerikacademy.workitemmanagement.commands.sort.*;
import com.telerikacademy.workitemmanagement.core.contracts.CommandFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.core.contracts.WimRepository;

public class CommandFactoryImpl implements CommandFactory {

    private static final String INVALID_COMMAND = "Invalid command name %s.";

    public Command createCommand(String commandName, WimFactory wimFactory, WimRepository wimRepository) {
        CommandType commandType = CommandType.valueOf(commandName.toUpperCase());
        switch (commandType) {
            case CREATEPERSON:
                return new CreatePerson(wimFactory, wimRepository);
            case CREATEBOARD:
                return new CreateBoard(wimFactory, wimRepository);
            case CREATEBUG:
                return new AddBugToBoard(wimFactory, wimRepository);
            case CREATEFEEDBACK:
                return new AddFeedbackToBoard(wimFactory, wimRepository);
            case CREATESTORY:
                return new AddStoryToBoard(wimFactory, wimRepository);
            case CREATETEAM:
                return new CreateTeam(wimFactory, wimRepository);
            case ADDPERSONTOTEAM:
                return new AddPersonToTeam(wimFactory, wimRepository);
            case ADDFEEDBACKTOBOARD:
                return new AddFeedbackToBoard(wimFactory, wimRepository);
            case ADDBUGTOBOARD:
                return new AddBugToBoard(wimFactory, wimRepository);
            case ADDSTORYTOBOARD:
                return new AddStoryToBoard(wimFactory, wimRepository);
            case ASSIGNWORKTOPERSON:
                return new AssignWorkToPerson(wimFactory, wimRepository);
            case ADDCOMMENTTOWORKITEM:
                return new AddCommentToWorkItem(wimFactory, wimRepository);
            case ADDREPOSTEPSTOBUG:
                return new AddRepoStepsToBug(wimFactory,wimRepository);
            case SHOWALL:
                return new ShowAll(wimFactory, wimRepository);
            case SHOWACTIVITY:
                return new ShowActivity(wimFactory, wimRepository);
            case SHOWALLTEAMMEMBERS:
                return new ShowAllTeamMembers(wimFactory, wimRepository);
            case CHANGEBUGSEVERITY:
                return new ChangeBugSeverity(wimFactory, wimRepository);
            case CHANGEBUGPRIORITY:
                return new ChangeBugPriority(wimFactory, wimRepository);
            case CHANGEBUGSTATUS:
                return new ChangeBugStatus(wimFactory, wimRepository);
            case CHANGESTORYSTATUS:
                return new ChangeStoryStatus(wimFactory, wimRepository);
            case CHANGESTORYSIZE:
                return new ChangeStorySize(wimFactory, wimRepository);
            case CHANGESTORYPRIORITY:
                return new ChangeStoryPriority(wimFactory, wimRepository);
            case CHANGEFEEDBACKSTATUS:
                return new ChangeFeedBackStatus(wimFactory, wimRepository);
            case CHANGEFEEDBACKRATING:
                return new ChangeFeedBackRating(wimFactory, wimRepository);
            case UNASSIGNWORKFROMPERSON:
                return new UnassignWorkFromPerson(wimFactory, wimRepository);
            case REMOVEPERSONFROMTEAM:
                return new RemovePersonFromTeam(wimFactory, wimRepository);
            case LISTALLWORKITEMS:
                return new ListAllWorkItems(wimFactory, wimRepository);
            case FILTERBUGS:
                return new FilterBugs(wimFactory, wimRepository);
            case FILTERSTORY:
                return new FilterStory(wimFactory, wimRepository);
            case FILTERFEEDBACK:
                return new FilterFeedBack(wimFactory, wimRepository);
            case FILTERBUGBYSTATUS:
                return new FilterBugByStatus(wimFactory, wimRepository);
            case FILTERSTORYBYSTATUS:
                return new FilterStoryByStatus(wimFactory, wimRepository);
            case FILTERFEEDBACKBYSTATUS:
                return new FilterFeedBackByStatus(wimFactory, wimRepository);
            case FILTERBYASSIGNEE:
                return new FilterByAssignee(wimFactory, wimRepository);
            case FILTERBUGSBYSTATUSANDASSIGNEE:
                return new FilterBugsByStatusAndAssignee(wimFactory,wimRepository);
            case FILTERSTORYBYSTATUSANDASSIGNEE:
                return new FilterStoryByStatusAndAssignee(wimFactory, wimRepository);
            case FILTERFEEDBACKBYSTATUSANDASSIGNEE:
                return new FilterFeedBackByStatusAndAssignee(wimFactory, wimRepository);
            case FILTERBYTITLE:
                return new FilterByTitle(wimFactory, wimRepository);
            case SORTBYSIZE:
                return new SortBySize(wimFactory, wimRepository);
            case SORTBYRATING:
                return new SortByRating(wimFactory, wimRepository);
            case SORTBUGSBYPRIORITY:
                return new SortBugsByPriority(wimFactory, wimRepository);
            case SORTSTORYBYPRIORITY:
                return new SortStoryByPriority(wimFactory, wimRepository);
            case SORTBYSEVERITY:
                return new SortBySeverity(wimFactory, wimRepository);
            case SHOWBOARDWORKITEMS:
                return new ShowBoardWorkItems(wimFactory,wimRepository);
            case SHOWALLBOARDSINATEAM:
                return new ShowAllBoardsInATeam(wimFactory,wimRepository);
            case SHOWBOARDACTIVITY:
                return new ShowBoardActivity(wimFactory,wimRepository);
        }
        throw new IllegalArgumentException(String.format(INVALID_COMMAND, commandName));
    }
}
