package com.telerikacademy.workitemmanagement.core.factories;

import com.telerikacademy.workitemmanagement.core.contracts.WimFactory;
import com.telerikacademy.workitemmanagement.models.*;
import com.telerikacademy.workitemmanagement.models.contracts.*;
import com.telerikacademy.workitemmanagement.models.enums.*;

public class WimFactoryImpl implements WimFactory {
  @Override
  public Board createBoard(String boardName) {
    return new BoardsImpl(boardName);
  }

  @Override
  public Team createTeam(String nameTeam) {
    return new TeamsImpl(nameTeam);
  }

  @Override
  public Bug createBug(String bugDescription,
                       String bugTitle,
                       PriorityType priorityType,
                       SeverityType severityType,
                       BugStatusType bugStatusType) {
    return new BugImpl(bugDescription, bugTitle, priorityType, severityType, bugStatusType);
  }

  @Override
  public Feedback createFeedBack(String feedbackDescription,
                                 String feedbackTitle,
                                 FeedbackStatusType feedbackStatusType,
                                 int feedbackRating) {
    return new FeedBackImpl(feedbackDescription, feedbackTitle, feedbackStatusType, feedbackRating);
  }

  @Override
  public Story createStory(String storyDescription,
                           String storyTitle,
                           PriorityType storyPriorityType,
                           SizeType storySizeType,
                           StoryStatusType storyStatusType) {
    return new StoryImpl(storyDescription, storyTitle, storyPriorityType, storySizeType, storyStatusType);
  }

  @Override
  public Person createPerson(String personName) {
    return new PersonImpl(personName);
  }
}
