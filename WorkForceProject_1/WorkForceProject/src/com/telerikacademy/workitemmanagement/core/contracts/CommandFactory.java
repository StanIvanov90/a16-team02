package com.telerikacademy.workitemmanagement.core.contracts;

import com.telerikacademy.workitemmanagement.commands.contracts.Command;

public interface CommandFactory {
    Command createCommand(String commandTypeAsString, WimFactory factory, WimRepository wimRepository);

}
