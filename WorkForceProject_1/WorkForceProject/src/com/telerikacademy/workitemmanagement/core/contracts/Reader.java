package com.telerikacademy.workitemmanagement.core.contracts;

public interface Reader {
    String readLine();
}
