package com.telerikacademy.workitemmanagement.core.contracts;

import com.telerikacademy.workitemmanagement.models.contracts.*;
import com.telerikacademy.workitemmanagement.models.enums.*;

public interface WimFactory {


  Team createTeam(String nameTeam);

  Bug createBug(String bugTitle,
                String bugDescription,
                PriorityType priorityType,
                SeverityType severityType,
                BugStatusType bugStatusType);

  Feedback createFeedBack(String feedbackTitle,
                          String feedbackDescription,
                          FeedbackStatusType feedbackStatusType,
                          int feedbackRating);

  Story createStory(String storyTitle,
                    String storyDescription,
                    PriorityType storyPriorityType,
                    SizeType storySizeType,
                    StoryStatusType storyStatusType);

  Person createPerson(String personName);

  Board createBoard(String boardName);


  }
