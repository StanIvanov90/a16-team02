package com.telerikacademy.workitemmanagement.core.contracts;

import com.telerikacademy.workitemmanagement.models.contracts.*;

import java.util.Map;

public interface WimRepository {
  Map<String, Team> getTeams();

  Map<String, Board> getBoards();

  Map<String, WorkItems> getWorkItems();

  Map<String, Person> getAllPeople();

  void addTeam(String name, Team team);

  void addWorkItem(String name, WorkItems workItems);

  void addPerson(String name, Person person);

  void addBoardToBoards(String name, Board board);

}
