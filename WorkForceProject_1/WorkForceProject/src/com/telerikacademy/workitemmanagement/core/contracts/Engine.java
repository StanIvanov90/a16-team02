package com.telerikacademy.workitemmanagement.core.contracts;

public interface Engine {
    void start();
}
