Please join our Trello Team by clicking on the link below:
https://trello.com/invite/a16team02/66a98ad731156c0b907926a7ff2e8f06

Project Description:

Design and implement a Work Item Management (WIM) Console Application.

Functional Requirements:

Application should support multiple teams. Each team has name, members and boards.

Member has name, list of work items and activity history.

- Name should be unique in the application

- Name is a string between 5 and 15 symbols.

Board has name, list of work items and activity history.

- Name should be unique in the team

- Name is a string between 5 and 10 symbols.

There are 3 types of work items: bug, story and feedback.

Bug:

Bug has ID, title, description, steps to reproduce, priority, severity, status, assignee, comments and history.

- Title is a string between 10 and 50 symbols.

- Description is a string between 10 and 500 symbols.

- Steps to reproduce is a list of strings.

- Priority is one of the following: High, Medium, Low

- Severity is one of the following: Critical, Major, Minor  Status is one of the following: Active, Fixed  Assignee is a member from the team.

- Comments is a list of comments (string messages with author).

- History is a list of all changes (string messages) that were done to the bug.

Story:

Story has ID, title, description, priority, size, status, assignee, comments and history.

- Title is a string between 10 and 50 symbols.

- Description is a string between 10 and 500 symbols.

- Priority is one of the following: High, Medium, Low

- Size is one of the following: Large, Medium, Small  Status is one of the following: NotDone, InProgress, Done  Assignee is a member from the team.

- Comments is a list of comments (string messages with author).

- History is a list of all changes (string messages) that were done to the story.

Feedback:

Feedback has ID, title, description, rating, status, comments and history.

- Title is a string between 10 and 50 symbols.

- Description is a string between 10 and 500 symbols.

- Rating is an integer.

- Status is one of the following: New, Unscheduled, Scheduled, Done  Comments is a list of comments (string messages with author).

- History is a list of all changes (string messages) that were done to the feedback.

Note: IDs of work items should be unique in the application i.e. if we have a bug with ID X then we can't have Story of Feedback with ID X.

Operations:

Application should support the following operations:

- Create a new person

- Show all people

- Show person's activity

- Create a new team

- Show all teams

- Show team's activity

- Add person to team

- Show all team members

- Create a new board in a team

- Show all team boards

- Show board's activity

- Create a new Bug/Story/Feedback in a board

- Change Priority/Severity/Status of a bug

- Change Priority/Size/Status of a story

- Change Rating/Status of a feedback

- Assign/Unassign work item to a person  Add comment to a work item

- List work items with options:

- List all

- Filter bugs/stories/feedback only

- Filter by status and/or asignee

- Sort by title/priority/severity/size/rating

General Requirements:

- Follow the OOP best practices:

- Use data encapsulation

- Proper use inheritance and polymorphism o Proper use interfaces and abstract classes o Proper use static/final members

- Proper use enums o Follow the principles of strong cohesion and loose coupling

- Use Streaming API

- Implement proper user input validation and display meaningful user messages

- Implement proper exception handling

- Cover functionality with unit tests (80% code coverage)


The Project has been tested with the following input:

CreatePerson Pesho
CreateTeam Team1 Pesho
CreateBoard Board1 Pesho Team1
CreateBoard Board2 Pesho Team1
CreateBoard Board3 Pesho Team1
CreatePerson Gosho
AddPersonToTeam Gosho Team1 Pesho
AddBugToBoard {{Bug description1}} BugTitleInput Low Minor Active Pesho Board1 Team1
ChangeBugPriority BugTitleInput High Pesho Board1 Team1
AddStoryToBoard {{Story Description1}} StoryTitle123455 Low Small InProgress Pesho Board3 Team1
AddFeedBackToBoard {{Feedback Description1}} FeedbackTitleTest Done 3 Pesho Board1 Team1
AddStoryToBoard {{StoryDescription test123}} StoryTitle12345532 LoW Small InProgress Pesho Board1 Team1
AddBugToBoard {{BugDescreption test}} BugTitleTesting LoW Minor Fixed Pesho Board2 Team1
AddBugToBoard {{BugTestDescr test a1}} BugTitle1234 LoW Major Fixed Pesho Board1 Team1
AddBugToBoard {{Description of a bug}} BugTitle4532 LoW Critical Active Pesho Board1 Team1
AddFeedBackToBoard {{Feedback description typed here}} FeedBackTitle12345 New 3 Pesho Board1 Team1
AddCommentToWorkItem StoryTitle12345532 testComment Pesho Board1 Team1
AddRepoStepsToBug BugTitleInput testRepoStep Gosho Board1 Team1
ChangeBugPriority BugTitleInput High Pesho Board1 Team1
ChangeBugSeverity BugTitleInput Major Pesho Board1 Team1
ChangeBugStatus BugTitleInput Fixed Pesho Board1 Team1
ChangeFeedBackRating FeedBackTitle12345 100 Pesho Board1 Team1
ChangeFeedBackStatus FeedBackTitle12345 Scheduled Pesho Board1 Team1
ChangeStoryPriority StoryTitle123455 Medium Pesho Board1 Team1
ChangeStorySize StoryTitle123455 Large Gosho Board1 Team1
ChangeStoryStatus StoryTitle123455 Done Gosho Board1 Team1
AssignWorkToPerson BugTitleTesting Pesho
AssignWorkToPerson BugTitle4532 Pesho
AssignWorkToPerson FeedbackTitleTest Pesho
AssignWorkToperson FeedBackTitle12345 Pesho
AssignWorkToPerson StoryTitle12345532 Pesho
AssignWorkToPerson StoryTitle123455 Pesho
FilterBugByStatus Active
FilterBugs
FilterBugsByStatusAndAssignee Pesho Active
FilterByAssignee Pesho
FilterByTitle
FilterFeedBack
FilterFeedBackByStatus Done
FilterFeedBackByStatusAndAssignee Pesho Done
FilterStory
FilterStoryByStatus Done
FilterStoryByStatusAndAssignee Pesho Done
ShowAllTeamMembers Team1
ShowBoardWorkItems Board1 Team1
ShowAllBoardsInATeam Team1
ShowBoardActivity Board1 Team1
ShowActivity Team1 Team
ShowActivity Pesho Member
ShowAll Team
ShowAll Member
SortBugsByPriority
SortByRating
SortBySeverity
SortBySize
SortStoryByPriority
ListAllWorkItems
RemovePersonFromTeam Gosho Team1 Pesho
UnassignWorkFromPerson BugTitleTesting Pesho


The following output has been shown on the console:

Member with name Pesho was created.
Team with name Team1 was created by Pesho.
Board with name Board1 was created by Pesho
Board with name Board2 was created by Pesho
Board with name Board3 was created by Pesho
Member with name Gosho was created.
Person Gosho added to Team Team1 by Pesho
Bug with name BugTitleInput was created by Pesho.
Change bug with name BugTitleInput to have priority High by Pesho
Story with name StoryTitle123455 was created by Pesho.
Feedback with name FeedbackTitleTest was created by Pesho.
Story with name StoryTitle12345532 was created by Pesho.
Bug with name BugTitleTesting was created by Pesho.
Bug with name BugTitle1234 was created by Pesho.
Bug with name BugTitle4532 was created by Pesho.
Feedback with name FeedBackTitle12345 was created by Pesho.
java.lang.NullPointerException
java.lang.NullPointerException
Change bug with name BugTitleInput to have priority High by Pesho
Change bug with name BugTitleInput to have severity Major by Pesho
Change bug with name BugTitleInput to have status Fixed by Pesho
Change feedback with name FeedBackTitle12345 to have 100 rating by Pesho
Change feedback with name FeedBackTitle12345 to have status Scheduled by Pesho
Change story with name StoryTitle123455 to have Medium priority by Pesho
Change story with name StoryTitle123455 to have size Large by Gosho
Change story with name StoryTitle123455 to have status Done by Gosho
WorkItem BugTitleTesting has been assigned to Pesho
WorkItem BugTitle4532 has been assigned to Pesho
WorkItem FeedbackTitleTest has been assigned to Pesho
WorkItem FeedBackTitle12345 has been assigned to Pesho
WorkItem StoryTitle12345532 has been assigned to Pesho
WorkItem StoryTitle123455 has been assigned to Pesho
BugTitle4532
BugTitle4532
BugTitleInput
BugTitle1234
BugTitleTesting
BugTitle4532
Bug: with the following details:
id: 5
title: BugTitleTesting
description: BugDescreption test
history: Bug with name BugTitleTesting was created by Pesho.
Bug Status: Fixed
Severity Type: Minor
Priority Type: Low
Bug: with the following details:
id: 7
title: BugTitle4532
description: Description of a bug
history: Bug with name BugTitle4532 was created by Pesho.
Bug Status: Active
Severity Type: Critical
Priority Type: Low
FeedBack: with the following details:
id: 3
title: FeedbackTitleTest
description: Feedback Description1
history: Feedback with name FeedbackTitleTest was created by Pesho.
FeedBack Status: Done
Rating: 3
FeedBack: with the following details:
id: 8
title: FeedBackTitle12345
description: Feedback description typed here
history: Feedback with name FeedBackTitle12345 was created by Pesho.
FeedBack Status: Scheduled
Rating: 100
Story: with the following details:
id: 4
title: StoryTitle12345532
description: StoryDescription test123
history: Story with name StoryTitle12345532 was created by Pesho.
Comments: testComment
Story Size: Small
Priority Type: Low
Story Status: InProgress
Story: with the following details:
id: 2
title: StoryTitle123455
description: Story Description1
history: Story with name StoryTitle123455 was created by Pesho.
Story Size: Large
Priority Type: Low
Story Status: Done
Invalid number of arguments.
FeedbackTitleTest
FeedBackTitle12345
FeedbackTitleTest
FeedbackTitleTest
StoryTitle12345532
StoryTitle123455
StoryTitle123455
StoryTitle123455
Pesho, Gosho
Board with name Board1 consists of the following items:
Bug: with the following details:
id: 1
title: BugTitleInput
description: Bug description1
history: Bug with name BugTitleInput was created by Pesho., Repo Step: testRepoStep
 has been added to BugTitleInput by Gosho
Bug Status: Fixed
Severity Type: Major
Priority Type: Low
----------------
Story: with the following details:
id: 2
title: StoryTitle123455
description: Story Description1
history: Story with name StoryTitle123455 was created by Pesho.
Story Size: Large
Priority Type: Low
Story Status: Done
----------------
FeedBack: with the following details:
id: 3
title: FeedbackTitleTest
description: Feedback Description1
history: Feedback with name FeedbackTitleTest was created by Pesho.
FeedBack Status: Done
Rating: 3
----------------
Story: with the following details:
id: 4
title: StoryTitle12345532
description: StoryDescription test123
history: Story with name StoryTitle12345532 was created by Pesho.
Comments: testComment
Story Size: Small
Priority Type: Low
Story Status: InProgress
----------------
Bug: with the following details:
id: 5
title: BugTitleTesting
description: BugDescreption test
history: Bug with name BugTitleTesting was created by Pesho.
Bug Status: Fixed
Severity Type: Minor
Priority Type: Low
----------------
Bug: with the following details:
id: 6
title: BugTitle1234
description: BugTestDescr test a1
history: Bug with name BugTitle1234 was created by Pesho.
Bug Status: Fixed
Severity Type: Major
Priority Type: Low
----------------
Bug: with the following details:
id: 7
title: BugTitle4532
description: Description of a bug
history: Bug with name BugTitle4532 was created by Pesho.
Bug Status: Active
Severity Type: Critical
Priority Type: Low
----------------
FeedBack: with the following details:
id: 8
title: FeedBackTitle12345
description: Feedback description typed here
history: Feedback with name FeedBackTitle12345 was created by Pesho.
FeedBack Status: Scheduled
Rating: 100
----------------
Board1, Board2, Board3
Bug with name BugTitleInput was created by Pesho., Change bug with name BugTitleInput to have priority High by Pesho, Story with name StoryTitle123455 was created by Pesho., Feedback with name FeedbackTitleTest was created by Pesho., Story with name StoryTitle12345532 was created by Pesho., Bug with name BugTitleTesting was created by Pesho., Bug with name BugTitle1234 was created by Pesho., Bug with name BugTitle4532 was created by Pesho., Feedback with name FeedBackTitle12345 was created by Pesho., Change bug with name BugTitleInput to have priority High by Pesho, Change bug with name BugTitleInput to have severity Major by Pesho, Change bug with name BugTitleInput to have status Fixed by Pesho, Change feedback with name FeedBackTitle12345 to have 100 rating by Pesho, Change feedback with name FeedBackTitle12345 to have status Scheduled by Pesho, Change story with name StoryTitle123455 to have Medium priority by Pesho, Change story with name StoryTitle123455 to have size Large by Gosho, Change story with name StoryTitle123455 to have status Done by Gosho
Board with name Board1 was created by Pesho, Board with name Board2 was created by Pesho, Board with name Board3 was created by Pesho, Person Gosho added to Team Team1 by Pesho, Bug with name BugTitleInput was created by Pesho., Change bug with name BugTitleInput to have priority High by Pesho, Story with name StoryTitle123455 was created by Pesho., Feedback with name FeedbackTitleTest was created by Pesho., Story with name StoryTitle12345532 was created by Pesho., Bug with name BugTitleTesting was created by Pesho., Bug with name BugTitle1234 was created by Pesho., Bug with name BugTitle4532 was created by Pesho., Feedback with name FeedBackTitle12345 was created by Pesho., Change bug with name BugTitleInput to have priority High by Pesho, Change bug with name BugTitleInput to have severity Major by Pesho, Change bug with name BugTitleInput to have status Fixed by Pesho, Change feedback with name FeedBackTitle12345 to have 100 rating by Pesho, Change feedback with name FeedBackTitle12345 to have status Scheduled by Pesho, Change story with name StoryTitle123455 to have Medium priority by Pesho, Change story with name StoryTitle123455 to have size Large by Gosho, Change story with name StoryTitle123455 to have status Done by Gosho
Team with name Team1 was created by Pesho., Board with name Board1 was created by Pesho, Board with name Board2 was created by Pesho, Board with name Board3 was created by Pesho, Person Gosho added to Team Team1 by Pesho, Bug with name BugTitleInput was created by Pesho., Change bug with name BugTitleInput to have priority High by Pesho, Story with name StoryTitle123455 was created by Pesho., Feedback with name FeedbackTitleTest was created by Pesho., Story with name StoryTitle12345532 was created by Pesho., Bug with name BugTitleTesting was created by Pesho., Bug with name BugTitle1234 was created by Pesho., Bug with name BugTitle4532 was created by Pesho., Feedback with name FeedBackTitle12345 was created by Pesho., Change bug with name BugTitleInput to have priority High by Pesho, Change bug with name BugTitleInput to have severity Major by Pesho, Change bug with name BugTitleInput to have status Fixed by Pesho, Change feedback with name FeedBackTitle12345 to have 100 rating by Pesho, Change feedback with name FeedBackTitle12345 to have status Scheduled by Pesho, Change story with name StoryTitle123455 to have Medium priority by Pesho, WorkItem BugTitleTesting has been assigned to Pesho, WorkItem BugTitle4532 has been assigned to Pesho, WorkItem FeedbackTitleTest has been assigned to Pesho, WorkItem FeedBackTitle12345 has been assigned to Pesho, WorkItem StoryTitle12345532 has been assigned to Pesho, WorkItem StoryTitle123455 has been assigned to Pesho
Team1
Gosho, Pesho
BugTitle4532 Low
BugTitle1234 Low
BugTitleTesting Low
BugTitleInput High
FeedbackTitleTest 3
FeedBackTitle12345 100
BugTitle4532 Critical
BugTitleInput Major
BugTitle1234 Major
BugTitleTesting Minor
StoryTitle123455 Large
StoryTitle12345532 Small
StoryTitle12345532 Low
StoryTitle123455 Medium
BugTitle4532, BugTitleInput, BugTitle1234, FeedbackTitleTest, BugTitleTesting, StoryTitle12345532, StoryTitle123455, FeedBackTitle12345
Person Gosho removed to Team Team1 by Pesho
WorkItem BugTitleTesting has been unassigned from Pesho