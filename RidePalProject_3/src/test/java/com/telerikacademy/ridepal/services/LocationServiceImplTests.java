package com.telerikacademy.ridepal.services;


import com.telerikacademy.ridepal.helper.ApiHelper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)

public class LocationServiceImplTests {

    @Mock
    ApiHelper apiHelper;

    @InjectMocks
    LocationServiceImpl locationService;


    @Test
    public void getLocationParameters_ShouldCall_ApiHelper() {

        //Act
        locationService.getLocationParameters("test", "test1");
        //Assert
        Mockito.verify(apiHelper, Mockito.times(1)).getLocationParameters("test","test1");
    }


}
