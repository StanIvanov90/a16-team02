package com.telerikacademy.ridepal.services;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.helper.DBHelper;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)

public class GenresServiceTests {

    @Mock
    GenresRepository genresRepository;
    @Mock
    DBHelper dbHelper;
    @Mock
    JSONObject jsonObject;
    @Mock
    JSONParser jsonParser;
    @Mock
    JSONArray jsonArray;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    RestTemplate restTemplate;

    @InjectMocks
    GenresService genresService;

    @Test
    public void getAllShould_CallRepository() {

        //Act
        genresService.getAll();
        //Assert
        Mockito.verify(genresRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getEnabledGenresShould_CallRepository() {

        //Act
        genresService.getEnabledGenres();
        //Assert
        Mockito.verify(genresRepository, Mockito.times(1)).getAllByEnabledTrue();
    }

    @Test
    public void getByIdShould_CallRepository(){
        //Arrange
        Mockito.when(genresRepository.existsById(anyInt())).thenReturn(true);
        //Act
        genresService.getById(anyInt());
        //Assert
        Mockito.verify(genresRepository, Mockito.times(1)).findById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByIdShould_ThrowEntityNotFound(){
        //Act
        genresService.getById(anyInt());
    }


    @Test
    public void enableGenreShould_CallRepository() {
        //Arrange
        Genre genre = new Genre();
        genre.setName("test");
        Mockito.when(genresRepository.findByName(anyString())).thenReturn(genre);
        Mockito.when(genresRepository.existsById(anyInt())).thenReturn(true);
        genre.setEnabled(true);
        //Act

        genresService.enableGenre(genre.getName());
        //Assert
        Mockito.verify(genresRepository, Mockito.times(1)).save(genre);
    }

    @Test(expected = EntityNotFoundException.class)
    public void enableGenreShould_ThrowEntityNotFound(){
        //Arrange
        Genre genre = new Genre();
        genre.setName("test");
        Mockito.when(genresRepository.findByName(anyString())).thenReturn(genre);
        //Act
        genresService.enableGenre(genre.getName());
    }




//    @Test
//    public void populateGenresShould_CallRepository() throws JsonProcessingException, ParseException {
//        //Arrange
//
//        RestTemplate restTemplate1 = Mockito.mock(RestTemplate.class);
//
//
//        JSONObject newJsonObject = Mockito.mock(JSONObject.class);
//        String test = "test test";
//        Mockito.when(restTemplate.getForObject(test,String.class))
//                .thenReturn(genreResponse());
//        Mockito.when(jsonParser.parse(genreResponse())).thenReturn(newJsonObject);
//        JSONArray jsonArray1 = new JSONArray();
//        Mockito.when(newJsonObject.get("data")).thenReturn(jsonArray1);
//        //Act
//
//
////        Mockito.when(jsonObject1.get("data")).thenReturn(jsonArray);
////        jsonArray.add(jsonObject1);
//
//        genresService.populateGenres();
//
//        //Assert
//        Mockito.verify(dbHelper, Mockito.times(1)).populateGenres();
//    }


}

