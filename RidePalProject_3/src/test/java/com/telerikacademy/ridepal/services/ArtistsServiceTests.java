package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.anyInt;

@RunWith(MockitoJUnitRunner.class)

public class ArtistsServiceTests {
    @Mock
    ArtistsRepository artistsRepository;

    @InjectMocks
    ArtistsService artistsService;

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        artistsService.getAll();

        //Assert
        Mockito.verify(artistsRepository, Mockito.times(1)).getAll();
    }

    @Test
    public void getByIdShould_CallRepository(){
        //Arrange
        Mockito.when(artistsRepository.existsById(anyInt())).thenReturn(true);
        //Act
        artistsService.getById(anyInt());
        //Assert
        Mockito.verify(artistsRepository, Mockito.times(1)).getArtistById(anyInt());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByIdShould_ThrowEntityNotFound(){
        //Act
        artistsService.getById(anyInt());
    }
}
