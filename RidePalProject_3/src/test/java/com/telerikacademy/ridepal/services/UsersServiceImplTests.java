package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.Factory;
import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserDetails;
import com.telerikacademy.ridepal.repositories.UserRepository;
import com.telerikacademy.ridepal.repositories.UserSecurityRepository;
import com.telerikacademy.ridepal.services.UsersServiceImpl;
import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;

@RunWith(MockitoJUnitRunner.class)


public class UsersServiceImplTests {

    @Mock
    UserRepository repository;
    @Mock
    UserSecurityRepository userSecurityRepository;

    @InjectMocks
    UsersServiceImpl mockService;

    @Test
    public void getByIdShould_ReturnUser_WhenUserExists() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();

        Mockito.when(repository.userExists(anyInt())).thenReturn(true);
        Mockito.when(repository.getById(anyInt())).thenReturn(expectedUser);

        //Act
        UserDetails returnedUser = mockService.getById(1);

        //Assert
        Assert.assertSame(expectedUser, returnedUser);

    }

    @Test
    public void getByIdShould_CallRepository() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(repository.userExists(anyInt())).thenReturn(true);
        Mockito.when(repository.getById(anyInt()))
                .thenReturn(expectedUser);
        //Act
        mockService.getById(1);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getById(anyInt());
    }

    @Test
    public void getByIdShould_Throw_WhenUserDoesntExist() {
        //Arrange
        Mockito.when(repository.userExists(anyInt()))
                .thenReturn(false);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.getById(anyInt()));
    }

    @Test
    public void createShould_Throw_WhenUserAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkUsernameExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createUser()));
    }
    @Test
    public void createShould_Throw_WhenUserEmailAlreadyExist() {
        //Arrange
        Mockito.when(repository.checkEmailExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createUser()));
    }
    @Test
    public void createShould_Throw_WhenUserIsDeleteAndEmailExist() {
        //Arrange
        Mockito.when(repository.isDeleted(anyString()))
                .thenReturn(true);
        Mockito.when(repository.checkEmailExists(anyString()))
                .thenReturn(true);

        //Act, Assert
        Assertions.assertThrows(DuplicateEntityException.class,
                () -> mockService.create(Factory.createUser()));
    }

    @Test
    public void createShould_CallRepository() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        //Act
        mockService.create(expectedUser);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).create(expectedUser);
    }

    @Test
    public void createShould_CallUpdateRepository() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(repository.isDeleted(anyString()))
                .thenReturn(true);
        Mockito.when(repository.checkEmailExists(anyString()))
                .thenReturn(false);
        //Act
        mockService.create(expectedUser);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(expectedUser.getId(),expectedUser);
    }

    @Test
    public void DeleteShould_DeleteUser_WhenData_Is_Valid() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        User user  = new User();
        user.setUsername(expectedUser.getName());
        List<Playlist> test = new ArrayList<>();
        Playlist newPlaylist = new Playlist();
        test.add(newPlaylist);


        Mockito.when(repository.getById(anyInt())).thenReturn(expectedUser);
        Mockito.when(repository.userExists(anyInt())).thenReturn(true);
        Mockito.when(userSecurityRepository.getByUsername(anyString()))
                .thenReturn(user);

        //Act
        mockService.delete(1);
        //Assert
        Assert.assertEquals(0, repository.getAll().size());
    }

    @Test
    public void deleteShould_Throw_WhenUserDoesntExist() {
        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.delete(anyInt()));
    }

    @Test
    public void getAll_Should_Return_ListOfUsers() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        List<UserDetails> test = new ArrayList<>();
        test.add(expectedUser);
        Mockito.when(repository.getAll()).thenReturn(test);

        //Act
        List<UserDetails> result = mockService.getAll();
        //Assert
        Assert.assertEquals(1, result.size());
    }

    @Test
    public void getAllShould_CallRepository() {
        //Arrange

        //Act
        mockService.getAll();
        //Assert
        Mockito.verify(repository, Mockito.times(1)).getAll();
    }

    @Test
    public void UpdateShould_CallRepository() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();
        Mockito.when(repository.userExists(anyInt())).thenReturn(true);
        //Act
        mockService.update(1, expectedUser);
        //Assert
        Mockito.verify(repository, Mockito.times(1)).update(1, expectedUser);
    }

    @Test
    public void updateShould_Throw_WhenUserDoesntExist() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();

        Mockito.when(repository.userExists(anyInt()))
                .thenReturn(false);

        //Act, Assert
        Assertions.assertThrows(EntityNotFoundException.class,
                () -> mockService.update(anyInt(), expectedUser));
    }

//    @Test
//    public void updateShould_Throw_WhenUserAlreadyExist() {
//        //Arrange
//        UserDetails expectedUser = Factory.createUser();
//
//        Mockito.when(repository.userExists(anyInt()))
//                .thenReturn(true);
//        Mockito.when(repository.checkUsernameExists(anyString()))
//                .thenReturn(true);
//
//        //Act, Assert
//        Assertions.assertThrows(DuplicateEntityException.class,
//                () -> mockService.update(anyInt(), expectedUser));
//    }
//
//    @Test
//    public void updateShould_Throw_WhenUserEmailAlreadyExist() {
//        //Arrange
//        UserDetails expectedUser = Factory.createUser();
//
//        Mockito.when(repository.userExists(anyInt()))
//                .thenReturn(true);
////        Mockito.when(repository.checkUsernameExists(anyString()))
////                .thenReturn(true);
//        Mockito.when(repository.checkEmailExists(anyString()))
//                .thenReturn(true);
//
//        //Act, Assert
//        Assertions.assertThrows(DuplicateEntityException.class,
//                () -> mockService.update(anyInt(), expectedUser));
//    }

    @Test
    public void getByUserNameShould_ReturnUser_WhenUserExists() {
        //Arrange
        UserDetails expectedUser = Factory.createUser();

        Mockito.when(repository.getByUsername(anyString())).thenReturn(expectedUser);

        //Act
        UserDetails returnedUser = mockService.getByUsername("pesho");

        //Assert
        Assert.assertSame(expectedUser, returnedUser);

    }
}

