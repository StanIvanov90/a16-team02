package com.telerikacademy.ridepal;

import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.models.Track;
import com.telerikacademy.ridepal.models.UserDetails;

public class Factory {

    public static UserDetails createUser() {
        return new UserDetails("ivan", "ivan@abv.bg", "ivan", "ivanov");
    }
    public static Track createTack(){
        Track track = new Track();
        track.setEnabled(true);
        track.setTitle("Title");
        track.setDuration("300");
        track.setRank("200");
        track.setId(1);
        track.setPreview("test");
        return track;
    }
}
