package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UsersService {

    List<UserDetails> getAll();

    UserDetails getById(int id);

    void create(UserDetails userDetails);

    void delete(int id);

    void update(int id, UserDetails userDetails);

    Page<UserDetails> UserPage(Pageable pageable);

    UserDetails getByUsername(String username);
}
