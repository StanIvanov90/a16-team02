package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.helper.DBHelper;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class ArtistsService {
    private static final String ARTIST_WITH_ID_DOES_NOT_EXIST = "Artist with id %d doesn't exist";
    private ArtistsRepository artistsRepository;
    private AlbumsRepository albumsRepository;
    private TracksRepository trackRepository;
    private GenresRepository genresRepository;
    private ObjectMapper objectMapper;
    private RestTemplate restTemplate;
    private DBHelper dbHelper;
    private JSONParser jsonParser;

    public ArtistsService(ArtistsRepository artistsRepository, AlbumsRepository albumsRepository,
                          TracksRepository trackRepository, GenresRepository genresRepository,
                          ObjectMapper objectMapper, RestTemplate restTemplate, JSONParser jsonParser) {
        this.artistsRepository = artistsRepository;
        this.albumsRepository = albumsRepository;
        this.trackRepository = trackRepository;
        this.genresRepository = genresRepository;
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.dbHelper = new DBHelper(artistsRepository,albumsRepository,genresRepository ,trackRepository,objectMapper,restTemplate,jsonParser);
        this.jsonParser = jsonParser;
    }

    public List<Artist> getAll() {
        return artistsRepository.getAll();
    }

    public Artist getById(int id){
        if(!artistsRepository.existsById(id)){
            throw new EntityNotFoundException(String.format(ARTIST_WITH_ID_DOES_NOT_EXIST,id));
        }
        return artistsRepository.getArtistById(id);
    }
    public void populateArtistsByGenre(String name) throws ParseException, JsonProcessingException {
        dbHelper.populateArtistsByGenre(name);
    }
}

