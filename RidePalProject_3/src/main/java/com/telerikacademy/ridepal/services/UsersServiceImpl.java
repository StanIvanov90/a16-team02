package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.repositories.PlaylistsRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.telerikacademy.ridepal.exceptions.DuplicateEntityException;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserDetails;
import com.telerikacademy.ridepal.repositories.UserRepository;
import com.telerikacademy.ridepal.repositories.UserSecurityRepository;
import com.telerikacademy.ridepal.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static com.telerikacademy.ridepal.constants.CommandConstants.*;

@Service
public class UsersServiceImpl implements UsersService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UsersServiceImpl.class);

    private UserRepository userRepository;
    private UserSecurityRepository userSecurityRepository;
    private PlaylistsRepository playlistsRepository;

    @Autowired
    public UsersServiceImpl(UserRepository userRepository, UserSecurityRepository userSecurityRepository, PlaylistsRepository playlistsRepository) {
        this.userRepository = userRepository;
        this.userSecurityRepository = userSecurityRepository;
        this.playlistsRepository=playlistsRepository;
    }

    @Override
    public List<UserDetails> getAll() {
        return userRepository.getAll();
    }

    @Override
    public UserDetails getById(int id) {
        if (!userRepository.userExists(id)) {
            throw new EntityNotFoundException(String.format(USER_NOT_FOUND, id));
        }
        return userRepository.getById(id);
    }

    @Override
    public void create(UserDetails user) {
        if (userRepository.isDeleted(user.getName())) {
            if (userRepository.checkEmailExists(user.getEmail())) {
                throw new DuplicateEntityException(
                        String.format(EMAIL_ALREADY_EXISTS, user.getEmail()));
            }
            user.setId(userRepository.getUserID(user.getName()));
            userRepository.update(user.getId(), user);
        } else if (userRepository.checkUsernameExists(user.getName())) {
            throw new DuplicateEntityException(
                    String.format(USERNAME_ALREADY_EXISTS, user.getName()));
        } else if (userRepository.checkEmailExists(user.getEmail())) {
            throw new DuplicateEntityException(
                    String.format(EMAIL_ALREADY_EXISTS, user.getEmail()));
        } else {
            userRepository.create(user);
            LOGGER.info("User with username: " + user.getName() + "was created!");
        }
    }

    @Override
    public void delete(int id) {
        if (!userRepository.userExists(id)) {
            throw new EntityNotFoundException(String.format(USER_NOT_FOUND, id));
        }
//        List<Playlist> playlists = playlistsRepository.getPlaylistsByUser(id);
//        playlists.forEach(playlist -> playlist.setEnabled(false));
//        playlists.forEach(element -> playlistsRepository.saveAndFlush(element));
        User user = userSecurityRepository.getByUsername(userRepository.getById(id).getName());
        user.setEnabled(false);
        userSecurityRepository.save(user);
        userRepository.delete(id);
        LOGGER.info("User with username: " + user.getUsername() + "was deleted!");
    }

    @Override
    public void update(int id, UserDetails user) {
        if (!userRepository.userExists(id)) {
            throw new EntityNotFoundException(String.format(USER_NOT_FOUND, id));
        }
        userRepository.update(id, user);
        LOGGER.info("User with username: " + user.getName() + "was updated!");
    }

    @Override
    public Page<UserDetails> UserPage(Pageable pageable) {
        List<UserDetails> userDetailsList = getAll();
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<UserDetails> userDetailsSubList;
        if (userDetailsList.size() < startItem) {
            userDetailsSubList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, userDetailsList.size());
            userDetailsSubList = userDetailsList.subList(startItem, toIndex);
        }
        return new PageImpl<UserDetails>(userDetailsSubList, PageRequest.of(currentPage, pageSize), userDetailsList.size());
    }

    @Override
    public UserDetails getByUsername(String username) {
        return userRepository.getByUsername(username);
    }
}


