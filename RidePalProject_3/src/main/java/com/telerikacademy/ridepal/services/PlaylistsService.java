package com.telerikacademy.ridepal.services;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.exceptions.ShortJourneyException;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.PlaylistsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import static com.telerikacademy.ridepal.constants.CommandConstants.*;

@Service
public class PlaylistsService {
    private static final int MAX_LENGTH = 2;
    private static final int SECONDS = 60;
    private static final int MINUTES = 60;
    private static final Logger LOGGER = LoggerFactory.getLogger(PlaylistsService.class);


    private TracksService tracksService;
    private PlaylistsRepository playlistsRepository;
    private DtoMapper dtoMapper;
    private GenresRepository genresRepository;

    @Autowired
    public PlaylistsService(TracksService tracksService,
                            PlaylistsRepository playlistsRepository,
                            DtoMapper dtoMapper,
                            GenresRepository genresRepository) {
        this.tracksService = tracksService;
        this.playlistsRepository = playlistsRepository;
        this.dtoMapper = dtoMapper;
        this.genresRepository = genresRepository;
    }

    public List<Playlist> getAll() {
        return playlistsRepository.getAll();
    }

    public List<Playlist> filterByName(List<Playlist> result, String name) {
        if (name != null && !name.isEmpty()) {
            if (playlistsRepository.findAllByTitleContainingAndEnabledTrue(name).isEmpty()) {
                throw new EntityNotFoundException(String.format(PLAYLIST_NOT_FOUND, name));
            }
            return playlistsRepository.findAllByTitleContainingAndEnabledTrue(name);
        }
        return result;
    }

    public List<Playlist> filterByNameAndUser(List<Playlist> result, String name, UserDetails username) {
        if (name != null && !name.isEmpty()) {
            if (playlistsRepository.findAllByTitleContainingAndEnabledTrueAndAndUserDetails(name, username).isEmpty()) {
                throw new EntityNotFoundException(String.format(PLAYLIST_NOT_FOUND, name));
            }
            return playlistsRepository.findAllByTitleContainingAndEnabledTrueAndAndUserDetails(name, username);
        }
        return result;
    }


    public List<Playlist> filterByDuration(List<Playlist> result, int startDuration, int endDuration) {
        return playlistsRepository.filterByDuration(startDuration * MINUTES * SECONDS, endDuration * MINUTES * SECONDS, result);
    }

    public List<Playlist> filterByGenreAndUser(List<Playlist> result, String genre, UserDetails userDetails) {
        if (genre != null && !genre.isEmpty()) {
            if (playlistsRepository.filterByGenreAndUser( genre, userDetails.getId(), result).isEmpty()) {
                throw new EntityNotFoundException(String.format(PLAYLISTS_WITH_GENRE_NOT_FOUND, genre));
            }
            return playlistsRepository.filterByGenreAndUser(genre, userDetails.getId(), result);
        }
        return result;
    }

    public List<Playlist> filterByDurationAndUser(List<Playlist> result, int startDuration, int endDuration, UserDetails userDetails) {
        return playlistsRepository.filterByDurationAndUserDetails(startDuration * MINUTES * SECONDS, endDuration * MINUTES * SECONDS, userDetails.getId(), result);
    }

    public List<Playlist> filterByGenre(List<Playlist> result, String genre) {
        if (genre != null && !genre.isEmpty()) {
            if (playlistsRepository.filterByGenre(genre).isEmpty()) {
                throw new EntityNotFoundException(String.format(PLAYLISTS_WITH_GENRE_NOT_FOUND, genre));
            }
            return playlistsRepository.filterByGenre(genre);
        }
        return result;
    }


    public Playlist getById(int id) {
        if (!playlistsRepository.existsById(id)) {
            throw new EntityNotFoundException(String.format(PLAYLIST_WITH_ID_DOES_NOT_EXIST, id));
        }
        return playlistsRepository.getById(id);
    }

    public List<Playlist> getPlaylistsByUser(int id) {
        return playlistsRepository.getPlaylistsByUser(id);
    }




    public void updatePlaylist(Playlist playlist) {
        if (!playlistsRepository.existsById(playlist.getId())) {
            throw new EntityNotFoundException(PLAYLIST_WITH_ID_NOT_FOUND);
        }
        playlistsRepository.save(playlist);
        LOGGER.info("Playlist with name " + playlist.getTitle() + "was updated!");
    }

    public void deletePlaylist(int id) {
        if (!playlistsRepository.existsById(id)) {
            throw new EntityNotFoundException(PLAYLIST_WITH_ID_NOT_FOUND);
        }
        Playlist playlist = getById(id);
        playlist.setEnabled(false);
        playlistsRepository.save(playlist);
        LOGGER.info("Playlist with name " + playlist.getTitle() + "was deleted!");
    }

    public Playlist createNewPlaylist(PlaylistDto playlist, int travelDuration,boolean topTracks,boolean useArtists) {
        if(travelDuration < FIVE_MINUTES){
            throw new ShortJourneyException(SHORT_JOURNEY);
        }
        List<String> time = playlist.getTime();
        List<String> genres = playlist.getGenres();

        Playlist newPlaylist = dtoMapper.playlistFromDto(playlist);
        List<Track> newTracks = new ArrayList<Track>();


        for (int i = 0; i < MAX_LENGTH; i++) {
            if(time.get(i).equals("0")){
                continue;
            }
            int genreTime = (int) (travelDuration * (Float.parseFloat(time.get(i).replace("%", "")) / 100.0f));
//            System.out.println("genreTime:" + genreTime);
            newTracks.addAll(tracksService.trackList(genresRepository.findByIdName(Integer.parseInt(genres.get(i))), genreTime,topTracks,useArtists));

        }
        newPlaylist.setPlaylist(newTracks);

        int totalDurationOfSongs = 0;
        int avgRating = 0;

        // System.out.println(newTracks.size());
        for (Track newTrack : newTracks) {
            totalDurationOfSongs = Integer.parseInt(newTrack.getDuration()) + totalDurationOfSongs;
            avgRating = Integer.parseInt(newTrack.getRank()) + avgRating;
        }
        newPlaylist.setDuration(totalDurationOfSongs);
        newPlaylist.setRank(avgRating / newTracks.size());
        LOGGER.info("Playlist with name " + playlist.getTitle() + "was created by" + playlist.getUserDetails());
        return playlistsRepository.save(newPlaylist);


    }


    public Page<Playlist> playlistPage(Pageable pageable, List<Playlist> playlists) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Playlist> playlistSubList;
        if (playlists.size() < startItem) {
            playlistSubList = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, playlists.size());
            playlistSubList = playlists.subList(startItem, toIndex);
        }
        return new PageImpl<Playlist>(playlistSubList, PageRequest.of(currentPage, pageSize), playlists.size());
    }

}


