package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.exceptions.ShortJourneyException;
import com.telerikacademy.ridepal.helper.DBHelper;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Track;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.*;


@Service
public class TracksService {
    private static final String TRACK_WITH_ID_DOES_NOT_EXIST = "Track with id %d doesn't exist.";
    private ArtistsRepository artistsRepository;
    private AlbumsRepository albumsRepository;
    private TracksRepository trackRepository;
    private GenresRepository genresRepository;
    private ObjectMapper objectMapper;
    private RestTemplate restTemplate;
    private DBHelper dbHelper;
    private JSONParser jsonParser;

    @Autowired
    public TracksService(TracksRepository trackRepository, ObjectMapper objectMapper, RestTemplate restTemplate,
                         GenresRepository genresRepository,AlbumsRepository albumsRepository,
                         ArtistsRepository artistsRepository,JSONParser jsonParser) {
        this.trackRepository = trackRepository;
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.genresRepository = genresRepository;
        this.albumsRepository = albumsRepository;
        this.artistsRepository = artistsRepository;
        this.dbHelper = new DBHelper(artistsRepository,albumsRepository,genresRepository ,trackRepository,objectMapper,restTemplate,jsonParser);
    }


    public List<Track> getTracksByPlaylistId(int id){
        return  trackRepository.getTracksByPlaylistId( id);
    }

    public List<Track> getAll(){
        return trackRepository.getAll();
    }

    public Track getById(int id){
        if(!trackRepository.existsById(id)){
            throw new EntityNotFoundException(String.format(TRACK_WITH_ID_DOES_NOT_EXIST,id));
        }
        return trackRepository.getTrackById(id);
    }

    public void populateSongsByGenre(String name) throws ParseException, InterruptedException, JsonProcessingException {
        dbHelper.populateSongsByGenre(name);
    }

    public List<Track> trackList(String name, int duration,boolean topTracks,boolean useArtists) {
        int sum = 0;
        List<Track> tracks = new ArrayList<>();
        if(!topTracks && !useArtists){
            tracks = trackRepository.findtracksDistinctArtist(name);
        } else if (!topTracks && useArtists){
            tracks = trackRepository.findtracks(name);
        } else if (topTracks && !useArtists){
            tracks = trackRepository.findTopByRankDistinct(name);
        } else {
            tracks = trackRepository.findTop(name);
        }
        List<Track> playlist = new ArrayList<>();
        for (Track track : tracks) {

            sum += Integer.parseInt(track.getDuration());
            int LEEWAY_MINUTES = 150;
            if ((sum > duration + LEEWAY_MINUTES)) {
                break;
            }
            playlist.add(track);
        }
        return playlist;
    }

}



