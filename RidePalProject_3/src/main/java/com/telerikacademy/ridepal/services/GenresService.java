package com.telerikacademy.ridepal.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.telerikacademy.ridepal.helper.DBHelper;
import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Genre;
import com.telerikacademy.ridepal.repositories.AlbumsRepository;
import com.telerikacademy.ridepal.repositories.ArtistsRepository;
import com.telerikacademy.ridepal.repositories.GenresRepository;
import com.telerikacademy.ridepal.repositories.TracksRepository;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static com.telerikacademy.ridepal.constants.CommandConstants.*;

@Service
public class GenresService {

    private ArtistsRepository artistsRepository;
    private AlbumsRepository albumsRepository;
    private TracksRepository trackRepository;
    private GenresRepository genresRepository;
    private ObjectMapper objectMapper;
    private RestTemplate restTemplate;
    private DBHelper dbHelper;
    private JSONParser jsonParser;

    @Autowired
    public GenresService(TracksRepository trackRepository, ObjectMapper objectMapper, RestTemplate restTemplate,
                         GenresRepository genresRepository, AlbumsRepository albumsRepository,
                         ArtistsRepository artistsRepository, JSONParser jsonParser) {
        this.trackRepository = trackRepository;
        this.objectMapper = objectMapper;
        this.restTemplate = restTemplate;
        this.genresRepository = genresRepository;
        this.albumsRepository = albumsRepository;
        this.artistsRepository = artistsRepository;
        this.dbHelper = new DBHelper(artistsRepository,
                albumsRepository,
                genresRepository,
                trackRepository,
                objectMapper,
                restTemplate,
                jsonParser);
    }

    public void populateGenres() throws JsonProcessingException, ParseException {
        dbHelper.populateGenres();
    }

    public List<Genre> getAll() {
        return genresRepository.getAll();
    }

    public List<Genre> getEnabledGenres(){
        return genresRepository.getAllByEnabledTrue();
    }

    public Genre getById(int id) {
        if (!genresRepository.existsById(id)) {
            throw new EntityNotFoundException(String.format(GENRE_WITH_ID_DOES_NOT_EXIST, id));
        }
        return genresRepository.findById(id);
    }

    public void enableGenre(String name){
        Genre genre =  genresRepository.findByName(name);
        if(!genresRepository.existsById(genre.getId())){
            throw new EntityNotFoundException(String.format(GENRE_NOT_FOUND,name));
        }
        genre.setEnabled(true);
        genresRepository.save(genre);
    }
}

