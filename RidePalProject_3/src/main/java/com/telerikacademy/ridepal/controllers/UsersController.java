package com.telerikacademy.ridepal.controllers;


import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.services.PlaylistsService;
import com.telerikacademy.ridepal.services.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.security.Principal;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.ridepal.constants.CommandConstants.USER_WAS_DELETED;

@Controller
public class UsersController {
    private UsersService usersService;
    private PlaylistsService playlistsService;
    private DtoMapper dtoMapper;

    @Autowired
    public UsersController(UsersService usersService, PlaylistsService playlistsService, DtoMapper dtoMapper) {
        this.usersService = usersService;
        this.playlistsService = playlistsService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping("/users")
    public String showUsers(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                            @RequestParam(name = "size", defaultValue = "6") int pageSize, Model model, Principal principal,
                            SecurityContextHolderAwareRequestWrapper request) {
        if (principal == null) {
            return "access-denied";
        } else {
            if (request.isUserInRole("ROLE_ADMIN")) {
                UserDetails userDetails = usersService.getByUsername(principal.getName());
                model.addAttribute("user", userDetails);
                Page<UserDetails> userPage = usersService.UserPage(PageRequest.of(currentPage - 1, pageSize));
                model.addAttribute("userPage", userPage);
                model.addAttribute("userDetails", new UserDetails());
                int totalPages = userPage.getTotalPages();
                if (totalPages > 0) {
                    List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                            .boxed()
                            .collect(Collectors.toList());
                    model.addAttribute("pageNumbers", pageNumbers);
                }
                return "users";
            } else {
                return "access-denied";
            }
        }
    }

    @GetMapping("/users/{id}")
    public String showUserPage(Model model, Principal principal,
                               @PathVariable int id, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ROLE_ADMIN") || request.isUserInRole("ROLE_USER")) {
            UserDetails userDetails = usersService.getById(id);
            model.addAttribute("user", userDetails);
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (userDetails.getName().equals(principal.getName()) || isAdmin) {
                return "user";
            }
            return "access-denied";
        }
        return "access-denied";
    }

    @GetMapping("/users/{id}/createdplaylists")
    public String createdByUsersPlatlists(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                                          @RequestParam(name = "size", defaultValue = "12") int pageSize,
                                          @RequestParam(defaultValue = "") String startDuration,
                                          @RequestParam(defaultValue = "") String endDuration,
                                          @RequestParam(defaultValue = "") String genre,
                                          @RequestParam(defaultValue = "") String name,
                                          Model model,
                                          @PathVariable int id,
                                          Principal principal,
                                          SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            UserDetails userProfile = usersService.getById(id);
            model.addAttribute("user", userDetails);
            model.addAttribute("userprofile", userProfile );
            List<Playlist> result = playlistsService.getPlaylistsByUser(id);
            result = playlistsService.filterByNameAndUser(result, name, userProfile);
            result = playlistsService.filterByGenreAndUser(result, genre, userProfile);
            if (!(startDuration.isEmpty() || endDuration.isEmpty())) {
                if (startDuration.matches("^[0-9]*$") && endDuration.matches("^[0-9]*$")) {
                    result = playlistsService.filterByDurationAndUser(result, Integer.parseInt(startDuration), Integer.parseInt(endDuration), userProfile);
                }
            }

            model.addAttribute("startDuration", startDuration);
            model.addAttribute("endDuration", endDuration);
            model.addAttribute("genre", genre);
            model.addAttribute("name", name);
            model.addAttribute("playlist", result);
            Page<Playlist> playlistPage = playlistsService.playlistPage(PageRequest.of(currentPage - 1, pageSize), result);
            model.addAttribute("playlistPage", playlistPage);
            int totalPages = playlistPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "createdplaylists";
        }
        return "access-denied";
    }

    @GetMapping("/users/update/{id}")
    public String getProfile(@PathVariable int id, Model model, Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getById(id);
            UserDto userDto = new UserDto();
            userDto.setFirstName(userDetails.getFirstName());
            userDto.setLastName(userDetails.getLastname());
            userDto.setEmail(userDetails.getEmail());
            model.addAttribute("username", userDetails);
            model.addAttribute("userDto", userDto);
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (userDetails.getName().equals(principal.getName()) || isAdmin) {
                return "updateuser";
            }
            return "access-denied";
        }
        return "access-denied";
    }

    @PostMapping("/users/update/{id}")
    public String editProfile(@PathVariable int id, @ModelAttribute("userDto") UserDto userDto, Principal principal,
                              SecurityContextHolderAwareRequestWrapper request, Model model,
                              @RequestParam("file") MultipartFile file) throws IOException {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getById(id);
            if (file.isEmpty()) {
                userDto.setImage(userDetails.getImage());
            } else {
                userDto.setImage(Base64.getEncoder().encodeToString(file.getBytes()));
            }
            if (userDto.getEmail().isEmpty() || userDto.getEmail() == null) {
                return "redirect:/users/update/{id}";
            }
            usersService.update(userDetails.getId(), dtoMapper.updateUserFromDto(userDetails, userDto));
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (userDetails.getName().equals(principal.getName()) || isAdmin) {
                return "redirect:/users/{id}";
            }
            return "access-denied";
        }
        return "access-denied";
    }
    @RequestMapping("/users/delete/{id}")
    public String deleteUser(@PathVariable int id, RedirectAttributes redirectAttributes,
                             Principal principal, SecurityContextHolderAwareRequestWrapper request){
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getById(id);
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (isAdmin || userDetails.getName().equals(principal.getName())) {
                usersService.delete(id);
                redirectAttributes.addFlashAttribute("message", USER_WAS_DELETED);
                if(isAdmin){
                    return "redirect:/users";
                }
                return "delete-confirmation";
            }
        }
        return "access-denied";
    }
}
