package com.telerikacademy.ridepal.controllers.rest;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.Album;
import com.telerikacademy.ridepal.services.AlbumsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/albums")
public class AlbumsRestController {

    private AlbumsService albumsService;

    @Autowired
    public AlbumsRestController(AlbumsService albumsService) {
        this.albumsService = albumsService;
    }

    @GetMapping
    public List<Album> getAll() {

        return albumsService.getAll();
    }

    @GetMapping("/{id}")
    public Album getById(@PathVariable int id) {
        try {
            return albumsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
