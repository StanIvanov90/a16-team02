package com.telerikacademy.ridepal.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.telerikacademy.ridepal.models.Artist;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.Track;
import com.telerikacademy.ridepal.services.*;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Controller
public class TrackController {
    private TracksService tracksService;
    private GenresService genresService;
    private PlaylistsService playlistsService;
    private ArtistsService artistsService;
    private AlbumsService albumsService;

    @Autowired
    public TrackController(TracksService tracksService, GenresService genresService, PlaylistsService playlistsService,
                           ArtistsService artistsService, AlbumsService albumsService) {
        this.tracksService = tracksService;
        this.genresService = genresService;
        this.playlistsService = playlistsService;
        this.artistsService = artistsService;
        this.albumsService = albumsService;
    }
}



