package com.telerikacademy.ridepal.controllers;

import com.telerikacademy.ridepal.models.UserDetails;
import com.telerikacademy.ridepal.services.UsersService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.security.Principal;

@Controller("/")
public class HomeController {
    private UsersService usersService;

    public HomeController(UsersService usersService) {
        this.usersService = usersService;
    }

    @GetMapping("/")
    public String showHomePage(Model model, Principal principal) {
        if (principal == null) {
            return "index";
        }
        UserDetails userDetails = usersService.getByUsername(principal.getName());
        model.addAttribute("user", userDetails);
        return "index";
    }

    @GetMapping("/startjourney")
    public String startjourney() {
        return "startjourney";
    }

    @GetMapping("/admin")
    public String showAdminPage() {
        return "admin";
    }

    @GetMapping("/access-denied")
    public String showAccessDenied() {
        return "access-denied";
    }
}
