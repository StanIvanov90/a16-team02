package com.telerikacademy.ridepal.controllers.rest;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.DtoMapper;
import com.telerikacademy.ridepal.models.Playlist;
import com.telerikacademy.ridepal.models.PlaylistDto;
import com.telerikacademy.ridepal.services.PlaylistsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("api/playlists")
public class PlaylistsRestController {

    private PlaylistsService playlistsService;
    private DtoMapper dtoMapper;

    @Autowired
    public PlaylistsRestController(PlaylistsService playlistsService, DtoMapper dtoMapper) {
        this.playlistsService = playlistsService;
        this.dtoMapper = dtoMapper;
    }

    @GetMapping
    public List<Playlist> getAll() {
        return playlistsService.getAll();
    }

    @GetMapping("/{id}")
    public Playlist getById(@PathVariable int id) {
        try {
            return playlistsService.getById(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
    @PutMapping("/{id}")
    public Playlist update(@PathVariable int id){
        try{
            Playlist playlistToUpdate = playlistsService.getById(id);
            PlaylistDto playlist = new PlaylistDto();
            playlist.setTitle(playlistToUpdate.getTitle());
            playlist.setDescription(playlistToUpdate.getDescription());
            playlist.setId(playlistToUpdate.getId());
            playlist.setUserDetails(playlistToUpdate.getUserDetails());
            playlistsService.updatePlaylist(dtoMapper.updateplaylistFromDto(playlistToUpdate,playlist));
            return playlistToUpdate;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id) {
        try {
            playlistsService.deletePlaylist(id);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }
}
