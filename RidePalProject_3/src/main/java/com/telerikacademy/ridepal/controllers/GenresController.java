package com.telerikacademy.ridepal.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.telerikacademy.ridepal.services.*;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.telerikacademy.ridepal.constants.CommandConstants.GENRE_SYNCHRONIZED;
import static com.telerikacademy.ridepal.constants.CommandConstants.ALL_GENRES_INFO_DOWNLOADED;


@Controller
public class GenresController {
    private TracksService tracksService;
    private GenresService genresService;
    private ArtistsService artistsService;
    private AlbumsService albumsService;

    @Autowired
    public GenresController(TracksService tracksService, GenresService genresService, ArtistsService artistsService,
                            AlbumsService albumsService) {
        this.tracksService = tracksService;
        this.genresService = genresService;
        this.artistsService = artistsService;
        this.albumsService = albumsService;
    }


    @GetMapping("/genres")
    public String genres(Model model){
        model.addAttribute("genres",genresService.getAll());
        return "genres";
    }

    @GetMapping("genre/post/{name}")
    public String getGenre(@PathVariable String name, RedirectAttributes redirectAttributes) throws ParseException,
            JsonProcessingException, InterruptedException {
        genresService.enableGenre(name);
        artistsService.populateArtistsByGenre(name);
        albumsService.populateArtistsAlbumsByGenre(name);
        tracksService.populateSongsByGenre(name);
        return "genreSuccess";
    }

    @GetMapping("/genre/post")
    public String getAllGenres(RedirectAttributes redirectAttributes) throws JsonProcessingException, ParseException {
        genresService.populateGenres();
        redirectAttributes.addFlashAttribute("message",ALL_GENRES_INFO_DOWNLOADED);
        return "genres";
    }

}
