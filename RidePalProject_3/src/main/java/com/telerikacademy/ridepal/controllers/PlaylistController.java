package com.telerikacademy.ridepal.controllers;


import com.telerikacademy.ridepal.helper.ApiHelper;
import com.telerikacademy.ridepal.models.*;
import com.telerikacademy.ridepal.services.*;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Controller;

import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.security.Principal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.telerikacademy.ridepal.constants.CommandConstants.BING_MAPS_URL;
import static com.telerikacademy.ridepal.constants.CommandConstants.PLAYLIST_WAS_DELETED;


@Controller
public class PlaylistController {
    private static final String NAME_YOUR_PLAYLIST = "Name your playlist.";
    private static final String WRITE_DESCRIPTION = "Write description.";
    private static final String PLAYLIST_NOT_FOUND = "Playlist not found!";

    private TracksService tracksService;
    private RestTemplate restTemplate;
    private UsersService usersService;
    private GenresService genresService;
    private LocationServiceImpl locationService;
    private DtoMapper dtoMapper;
    private ApiHelper apiHelper;
    private PlaylistsService playlistsService;
    private ArtistsService artistsService;

    @Autowired
    public PlaylistController(RestTemplate restTemplate,
                              TracksService tracksService,
                              UsersService usersService,
                              LocationServiceImpl locationService,
                              GenresService genresService,
                              DtoMapper dtoMapper,
                              ApiHelper apiHelper,
                              PlaylistsService playlistsService,
                              ArtistsService artistsService) {
        this.restTemplate = restTemplate;
        this.genresService = genresService;
        this.tracksService = tracksService;
        this.usersService = usersService;
        this.locationService = locationService;
        this.dtoMapper = dtoMapper;
        this.apiHelper = apiHelper;
        this.playlistsService = playlistsService;
        this.artistsService = artistsService;
    }

    @ModelAttribute("genres")
    public List<Genre> populateGenres() {
        return genresService.getEnabledGenres();
    }

    @ModelAttribute("artists")
    public List<Artist> populateStyles() {
        return artistsService.getAll();
    }


    @GetMapping("/newplaylist")
    public String getPlaylist(Model model, Principal principal) {
        UserDetails userDetails = usersService.getByUsername(principal.getName());
        PlaylistDto playlistDto = new PlaylistDto();
        playlistDto.setGenres(Arrays.asList("", ""));
        playlistDto.setTime(Arrays.asList("", ""));
        model.addAttribute("playlist", playlistDto);
        model.addAttribute("user", userDetails);
        return "newplaylist";
    }


    @PostMapping("/newplaylist")
    public String postPlaylist(@Valid @ModelAttribute("playlist") PlaylistDto playlistDto,
                               BindingResult errors,
                               Model model,
                               Principal principal) {

        if (errors.hasErrors()) {
            return "newplaylist";
        }
        try {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            if (!(playlistDto.getStartLocation().isEmpty() || playlistDto.getEndLocation().isEmpty())) {
                Location finalLocation = locationService.getLocationParameters(playlistDto.getStartLocation(), playlistDto.getEndLocation());

                if (playlistDto.getTitle().isEmpty()) {
                    model.addAttribute("error", NAME_YOUR_PLAYLIST);
                    return "newplaylist";
                }
                if (playlistDto.getDescription().isEmpty()) {
                    model.addAttribute("error", WRITE_DESCRIPTION);
                    return "newplaylist";
                }

                playlistDto.setCover(apiHelper.getPictureForPlaylist());
                playlistDto.setUserDetails(usersService.getByUsername(principal.getName()));

                model.addAttribute("playlist", playlistDto);
                model.addAttribute("location", finalLocation);
                model.addAttribute("URL", BING_MAPS_URL);
                System.out.println(playlistDto.getTime());
                Playlist playlist = playlistsService.createNewPlaylist(playlistDto, (int) finalLocation.getDuration(),
                       playlistDto.isTopTracks(),playlistDto.isUseArtists());
                List<Track> tracks = tracksService.getTracksByPlaylistId(playlist.getId());
                model.addAttribute("tracks", tracks);
                model.addAttribute("playlist", playlistDto);
                model.addAttribute("user", userDetails);

                return "showmap";
            }
        } catch (org.springframework.web.client.HttpClientErrorException e) {
            model.addAttribute("error", "Invalid address.");
            return "newplaylist";
        }
        return "newplaylist";
    }


    @GetMapping("/playlists")
    public String showPlaylists(@RequestParam(name = "page", defaultValue = "1") int currentPage,
                                @RequestParam(name = "size", defaultValue = "12") int pageSize,
                                @RequestParam(defaultValue = "") String startDuration,
                                @RequestParam(defaultValue = "") String endDuration,
                                @RequestParam(defaultValue = "") String genre,
                                @RequestParam(defaultValue = "") String name,
                                Model model,
                                Principal principal,
                                SecurityContextHolderAwareRequestWrapper request) {
        if (!(principal == null) ){
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            model.addAttribute("user", userDetails);
        }
            List<Playlist> result = playlistsService.getAll();
            result = playlistsService.filterByName(result, name);
            result = playlistsService.filterByGenre(result, genre);
            if (!(startDuration.isEmpty() || endDuration.isEmpty())) {
                if (startDuration.matches("^[0-9]*$") && endDuration.matches("^[0-9]*$")) {
                    result = playlistsService.filterByDuration(result, Integer.parseInt(startDuration), Integer.parseInt(endDuration));
                }
            }

            model.addAttribute("startDuration", startDuration);
            model.addAttribute("endDuration", endDuration);
            model.addAttribute("genre", genre);
            model.addAttribute("name", name);
            Page<Playlist> playlistPage = playlistsService.playlistPage(PageRequest.of(currentPage - 1, pageSize), result);
            model.addAttribute("playlistPage", playlistPage);
            int totalPages = playlistPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages).boxed().collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "playlists";
    }

    @GetMapping("/playlists/{id}")
    public String showBeerPage(Model model, @PathVariable int id, Principal principal) {
        if (!(principal == null)) {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            model.addAttribute("user", userDetails);
        }

            Playlist playlist = getVerifiedPlaylist(id);
            model.addAttribute("playlist", playlist);
            List<Track> tracks = tracksService.getTracksByPlaylistId(playlist.getId());
            model.addAttribute("tracks", tracks);
            return "playlist";
    }

    @GetMapping("/playlists/update/{id}")
    public String showPlaylistUpdateForm(@PathVariable int id, Model model, Principal principal,
                                         SecurityContextHolderAwareRequestWrapper request) {
        boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            Playlist playlistToUpdate = playlistsService.getById(id);
            PlaylistDto playlist = new PlaylistDto();
            playlist.setTitle(playlistToUpdate.getTitle());
            playlist.setDescription(playlistToUpdate.getDescription());
            List<String> genreIds = new ArrayList<>();
            for (Genre g : playlistToUpdate.getGenres()) {
                genreIds.add(String.valueOf(g.getId()));
            }
            playlist.setGenres(genreIds);
            playlist.setId(playlistToUpdate.getId());
            playlist.setUserDetails(playlistToUpdate.getUserDetails());
            model.addAttribute("playlist", playlist);

            if (isAdmin || playlistToUpdate.getUserDetails().getName().equals(principal.getName())) {
                return "updateplaylist";
            }
        }
        return "access-denied";
    }

    @PostMapping("/playlists/update/{id}")
    public String updatePlaylist(@PathVariable int id, @ModelAttribute PlaylistDto playlistDto) {
        Playlist playlist = playlistsService.getById(id);
        playlistsService.updatePlaylist(dtoMapper.updateplaylistFromDto(playlist, playlistDto));

        return "redirect:/playlists/" + id;
    }

    @RequestMapping("/playlists/delete/{id}")
    public String deletePlaylist(@PathVariable int id, RedirectAttributes redirectAttributes,
                                 Principal principal, SecurityContextHolderAwareRequestWrapper request) {
        if (request.isUserInRole("ADMIN") || request.isUserInRole("USER")) {
            UserDetails userDetails = usersService.getByUsername(principal.getName());
            Playlist playlist = playlistsService.getById(id);
            boolean isAdmin = request.isUserInRole("ROLE_ADMIN");
            if (isAdmin || playlist.getUserDetails().getName().equals(principal.getName())) {
                playlistsService.deletePlaylist(playlist.getId());
                redirectAttributes.addFlashAttribute("message", PLAYLIST_WAS_DELETED);
                return "redirect:/playlists";
            }
        }
        return "access-denied";
    }



    private Playlist getVerifiedPlaylist(int id) {
        try {
            return playlistsService.getById(id);
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, PLAYLIST_NOT_FOUND);
        }
    }
}