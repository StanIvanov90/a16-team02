package com.telerikacademy.ridepal.models;

import com.telerikacademy.ridepal.repositories.GenresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class DtoMapper {
    private GenresRepository genresRepository;

    @Autowired
    public DtoMapper(GenresRepository genresRepository) {
        this.genresRepository = genresRepository;
    }

    public UserDetails userFromDto(UserDto userDto) {
        UserDetails user = new UserDetails(userDto.getName(), userDto.getEmail(), userDto.getFirstName(), userDto.getLastName());
        user.setImage(userDto.getImage());
        return user;
    }


    public Playlist playlistFromDto(PlaylistDto playlistDto) {
        Playlist playlist = new Playlist();
        playlist.setTitle(playlistDto.getTitle());
        playlist.setDescription(playlistDto.getDescription());
        playlist.setUserDetails(playlistDto.getUserDetails());
        playlist.setCover(playlistDto.getCover());
        playlist.setEnabled(true);
        Set<Genre> genres = new HashSet<>();
        if (!(playlistDto.getGenres() == null)) {
            for (String i : playlistDto.getGenres()) {
                if (Integer.parseInt(i) == 0) {
                    continue;
                }
                genres.add(genresRepository.findById(Integer.parseInt(i)));
            }
            playlist.setGenres(genres);
        }

        return playlist;
    }

    public Playlist updateplaylistFromDto(Playlist playlist,PlaylistDto playlistDto) {
        playlist.setId(playlistDto.getId());
        playlist.setTitle(playlistDto.getTitle());
        playlist.setDescription(playlistDto.getDescription());
        playlist.setEnabled(true);
        Set<Genre> genres = new HashSet<>();
        if (!(playlistDto.getGenres() == null)) {
            for (String i : playlistDto.getGenres()) {
                if (Integer.parseInt(i) == 0) {
                    continue;
                }
                genres.add(genresRepository.findById(Integer.parseInt(i)));
            }
            playlist.setGenres(genres);
        }

        return playlist;
    }



    public UserDetails updateUserFromDto(UserDetails userDetails,UserDto userDto){
        userDetails.setFirstName(userDto.getFirstName());
        userDetails.setLastname(userDto.getLastName());
        userDetails.setEmail(userDto.getEmail());
        userDetails.setImage(userDto.getImage());
        return userDetails;
    }
}
