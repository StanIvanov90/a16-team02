package com.telerikacademy.ridepal.models;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class UserDto {

    @NotNull(message = "Name should be not null")
    @Pattern(regexp = "[a-zA-Z0-9_.]*", message = "Name should not contain other symbols than letters and digits")
    @Size(min = 2, max = 25, message = "Name size should be between 2 and 25 symbols")
    private String username;

    @NotNull(message = "Email should be not null")
    @Size(min = 6, max = 25, message = "Email size should be between 2 and 25 symbols")
    //@Pattern(regexp ="^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$", message = "Email size should be between 2 and 25 symbols")
    private String email;

    @NotNull(message = "Password should not be null")
    @Size(min = 8, max = 64, message = "Password should be between 8 and 64 symbols")
    private String password;

    private String image;

    private String passwordConfirmation;

    private String firstName;

    private String lastName;

    private int id;

    public UserDto() {
    }

    public String getName() {
        return username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setName(String name) {
        this.username = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
