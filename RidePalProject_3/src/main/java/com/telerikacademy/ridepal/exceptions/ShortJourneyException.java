package com.telerikacademy.ridepal.exceptions;

public class ShortJourneyException extends RuntimeException {
    public ShortJourneyException(String message) {
        super(message);
    }
}
