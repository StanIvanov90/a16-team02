package com.telerikacademy.ridepal.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private DataSource securityDataSource;
    private AuthenticationSuccessHandler authenticationSuccessHandler;


    @Autowired
    public SecurityConfig(DataSource securityDataSource, AuthenticationSuccessHandler authenticationSuccessHandler
                          ) {
        this.securityDataSource = securityDataSource;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    public SecurityConfig(boolean disableDefaults, DataSource securityDataSource,
                          AuthenticationSuccessHandler authenticationSuccessHandler) {
        super(disableDefaults);
        this.securityDataSource = securityDataSource;
        this.authenticationSuccessHandler = authenticationSuccessHandler;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(securityDataSource);

//        auth.inMemoryAuthentication()
//                .withUser(User.withUsername("pesho").password("{noop}pass1").roles("USER", "ADMIN"))
//                .withUser(User.withUsername("nadya").password("{noop}pass2").roles("USER"))
//                .withUser(User.withUsername("tosho").password("{noop}pass3").roles("USER"));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/users/new", "/users/edit/**", "/users/delete/**", "/users/update/**", "/newplaylist/**")
                .hasAnyRole("USER", "ADMIN")

                /*.permitAll()*/
                .antMatchers("/admin","/admin/")
                .hasAnyRole("ADMIN")
                .antMatchers("/genres","/genres/","/genre/post/**")
                .hasAnyRole("ADMIN")
                .antMatchers("/")
                .permitAll()
//                .antMatchers("/css/**", "/js/**", "/images/**")
//                .permitAll()
//                .antMatchers("/beers/test/**")
//                .permitAll()
//                .antMatchers("/users")
//                .hasRole("ADMIN")
                .and()
                .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/authenticate")
                .permitAll()
                .successHandler(authenticationSuccessHandler)
                .and()
                .logout()
                .permitAll()
                .and()
                .exceptionHandling()
                .accessDeniedPage("/access-denied")
                .and().csrf().disable();
    }

    @Bean
    public UserDetailsManager userDetailsManager() {
        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
        jdbcUserDetailsManager.setDataSource(securityDataSource);
        return jdbcUserDetailsManager;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/vendor/**", "/fonts/**");
    }
}




//
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(prePostEnabled = true)
//public class SecurityConfig extends WebSecurityConfigurerAdapter {
//    private DataSource securityDataSource;
//   // private AuthenticationSuccessHandler authenticationSuccessHandler;
//
//
//    @Autowired
//    public SecurityConfig(DataSource securityDataSource) {
//        this.securityDataSource = securityDataSource;
//
//    }
//
//    public SecurityConfig(boolean disableDefaults, DataSource securityDataSource) {
//        super(disableDefaults);
//        this.securityDataSource = securityDataSource;
//
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.jdbcAuthentication()
//                .dataSource(securityDataSource);
//
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        http.authorizeRequests()
//                .antMatchers("/**")
//                .permitAll()
//                .antMatchers("/admin")
//                .hasRole("ADMIN")
//                .antMatchers("/")
//                .permitAll()
////                .antMatchers("/css/**", "/js/**", "/images/**")
////                .permitAll()
////                .permitAll()
//                .antMatchers("/users")
//                .hasRole("ADMIN")
//                .and()
//                .formLogin()
//                .loginPage("/login")
//                .loginProcessingUrl("/authenticate")
//                .permitAll()
//          //      .successHandler(authenticationSuccessHandler)
//                .and()
//                .logout()
//                .permitAll()
//                .and()
//                .exceptionHandling()
//                .accessDeniedPage("/access-denied")
//                .and().csrf().disable();
//    }
//
//    @Bean
//    public UserDetailsManager userDetailsManager() {
//        JdbcUserDetailsManager jdbcUserDetailsManager = new JdbcUserDetailsManager();
//        jdbcUserDetailsManager.setDataSource(securityDataSource);
//        return jdbcUserDetailsManager;
//    }
//
//    @Bean
//    public PasswordEncoder passwordEncoder() {
//        return new BCryptPasswordEncoder();
//    }
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web
//                .ignoring()
//                .antMatchers("/resources/**", "/static/**", "/css/**", "/js/**", "/images/**", "/vendor/**", "/fonts/**");
//    }
//}
//


