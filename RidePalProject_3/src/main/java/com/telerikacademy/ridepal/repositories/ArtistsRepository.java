package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Artist;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface ArtistsRepository extends JpaRepository<Artist, Serializable> {

    @Query("from Artist ")
    List<Artist> getAll();

    Artist getArtistById(int id);

}
