package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.util.List;

@Repository
public interface GenresRepository extends JpaRepository<Genre, Serializable> {

    @Query("select g from Genre g where g.name = ?1")
    Genre findByName(String name);

    @Query("select g from Genre g where g.id > 0 ")
    List<Genre> getAll();

    List<Genre> getAllByEnabledTrue();

    @Query("select g from Genre g where g.id = ?1")
    Genre findById(int id);

    @Query("select g.name from Genre g where g.id = ?1")
    String findByIdName(int id);
}

