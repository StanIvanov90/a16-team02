package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.models.Album;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.io.Serializable;
import java.util.List;

public interface AlbumsRepository extends JpaRepository<Album, Serializable> {

    @Query(value = "select *\n" +
            "from albums a join genres g on a.genre_id = g.genre_id\n" +
            "where genre_name = ?1",nativeQuery = true)
    List<Album> findAlbumsByGenre(String name);

    @Query("from Album ")
    List<Album> getAll();

    Album getAlbumById(int id);

}
