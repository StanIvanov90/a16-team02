package com.telerikacademy.ridepal.repositories;

import com.telerikacademy.ridepal.exceptions.EntityNotFoundException;
import com.telerikacademy.ridepal.models.User;
import com.telerikacademy.ridepal.models.UserDetails;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.telerikacademy.ridepal.constants.CommandConstants.USER_NOT_FOUND;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean isDeleted(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where name like :name and enabled is 0", UserDetails.class);
            query.setParameter("name", username);
            session.clear();
            return !query.list().isEmpty();
        }

    }

    @Override
    public boolean checkEmailExists(String email) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where email like :email and enabled is not 0", UserDetails.class);
            query.setParameter("email", email);
            return !query.list().isEmpty();
        }
    }

    @Override
    public boolean checkUsernameExists(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where name like :name and enabled is not 0", UserDetails.class);
            query.setParameter("name", username);
            return !query.list().isEmpty();
        }
    }

    @Override
    public int getUserID(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where name like :username and enabled is not 0", UserDetails.class);
            query.setParameter("username", username);
            return query.list().get(0).getId();
        }
    }

    @Override
    public UserDetails getByUsername(String username) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where name = :username", UserDetails.class);
            query.setParameter("username", username);
            List<UserDetails> users = query.list();
            if (users.size() != 1) {
                throw new EntityNotFoundException(String.format("User with username %s does not exist", username));
            }
            return users.get(0);
        }
    }

    @Override
    public List<UserDetails> getAll() {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            List<UserDetails> result = session.createQuery("from UserDetails where enabled is not 0", UserDetails.class).getResultList();
            session.getTransaction().commit();
            return result;
        }
    }

    @Override
    public UserDetails getById(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            UserDetails user = session.get(UserDetails.class, id);
            session.getTransaction().commit();
            if (user == null || !user.getExists()) {
                throw new EntityNotFoundException(String.format(USER_NOT_FOUND, id));
            }
            return user;
        }
    }

    @Override
    public void create(UserDetails user) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            user.setExists(true);
            session.save(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            UserDetails user = session.get(UserDetails.class, id);
            session.beginTransaction();
            user.setExists(false);
            session.getTransaction().commit();
        }
    }


    @Override
    public void update(int id, UserDetails user) {
        user.setExists(true);
        user.setId(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(user);
            session.getTransaction().commit();
        }
    }

    @Override
    public boolean userExists(int id) {
        try (Session session = sessionFactory.openSession()) {
            Query<UserDetails> query = session.createQuery("from UserDetails where id like :id and enabled is not 0", UserDetails.class);
            query.setParameter("id", id);
            return !query.list().isEmpty();
        }
    }
}
